<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\User;
use App\Models\Role;
use App\Models\RoleUser;

class CheckUserRoleStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:roles';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check users role is expired or not';
    
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        date_default_timezone_set('Asia/Almaty');
        $date = date('Y-m-d') ;       

        $users = User::whereHas('roles', function ($query) {
            $query->where('name','!=','admin')->where('name', '!=', 'service_provider');
        })->with('roles')->join('role_user','role_user.user_id', '=', 'users.id')->where('expire_time', '<=', $date)->get();

        foreach ($users as $user) {
            $user->roles()->detach($user->role_id);
        }

        $this->info('Successfully checked ');
    }
}
