<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Models\Packet;
use App\Models\Section;

class PacketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      return view('admin.packets.index', ['packets' => Packet::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $packet = new Packet();
        return view('admin.packets.create', compact('packet'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {     
        if ($this->validateRequest()) {
          if (is_numeric(str_replace(' ', '', $request->cost))) {
            Packet::create($request->all());
          }
          else {            
            $request->validate([
              'cost' => 'integer',
            ]);
          }
          if ($request->submit == 'Сохранить и добавить еще') {
            return redirect()->route('packets.create');
          }
          return redirect()->route('packets.index');
        }
        else {
          return redirect()->route('packets.create')->withErrors($this->validateRequest());
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('admin.packets.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $packet = Packet::find($id);
      return view('admin.packets.update', compact('packet'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $packets = Packet::find($id);
        if ($this->validateRequest()) {
           $packets->name = $request->name;
           $packets->term = $request->term;
          if (is_numeric(str_replace(' ', '', $request->cost))) {
            $packets->cost = $request->cost;
          }
          else {
            $request->validate([
              'cost' => 'integer',
            ]);
          }           
           $packets->description = $request->description;
           $packets->save();
           return redirect()->route('packets.index');
        }
        else {
            return redirect()->route('packets.edit')->withErrors($this->validateRequest());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $packets = Packet::findOrFail($id);
      $packets->delete();
      return redirect()->route('packets.index');
    }

    public function validateRequest()
    {
      $rules = request()->validate([
        'name' => 'required|string|max:50',
        'description' => 'required|string|max:5000',
        'term' => 'required|integer',
        'cost' => 'required|string',
      ]);
      return $rules;
    }
}
