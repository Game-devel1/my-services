<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Auth;
use App\Models\Role;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except('index');
    }

    public function index() {
        // $owner = new Role();
        // $owner->name = 'service_provider';
        // $owner->display_name = 'User service provider'; // optional
        // $owner->description  = 'User is the owner of a given service'; // optional
        // $owner->save();

        // $admin = new Role();
        // $admin->name         = 'admin';
        // $admin->display_name = 'User Administrator'; // optional
        // $admin->description  = 'User is allowed to manage and edit other users'; // optional
        // $admin->save();

        // $vip = new Role();
        // $vip->name         = 'vip';
        // $vip->display_name = 'VIP user'; // optional
        // $vip->description  = 'The user has more advantages than the common user'; // optional
        // $vip->save();

        // $paintAds = new Role();
        // $paintAds->name         = 'paintAds';
        // $paintAds->display_name = 'Painting users cards'; // optional
        // $paintAds->description  = 'The user has more advantages than the common user'; // optional
        // $paintAds->save();
        
        // $repeatAds = new Role();
        // $repeatAds->name         = 'repeatAds';
        // $repeatAds->display_name = 'Users card repeated ad'; // optional
        // $repeatAds->description  = 'The user has more repeatAds than the common user'; // optional
        // $repeatAds->save();      
        
        // $profi = new Role();
        // $profi->name = 'profi';
        // $profi->display_name = 'User profi service provider'; // optional
        // $profi->description  = 'User is the profi of a given service'; // optional
        // $profi->save();

        // $more_resume = new Role();
        // $more_resume->name = 'moreResume';
        // $more_resume->display_name = 'User will make more than 3 resume'; // optional
        // $more_resume->description  = 'User will make more than 3 resume'; // optional
        // $more_resume->save();
    }

    public function addRole() 
    {
        //$user = Auth::user();
        //$admin = Role::where('name', '=', 'admin')->first();
        // role attach alias
        //$user->attachRole($admin); // parameter can be an Role object, array, or id
        // or eloquent's original technique
      //  $user->roles()->attach($admin->id); // id only
    }
}
