<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Resume;
use App\Models\License;

class LicenseController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {    
    return view('admin.licenses.index', ['licenses' => License::orderBy('id', 'DESC')->get()]);
  }
      
  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
      $licenses = License::find($id);      
      if ($licenses->isActive == 1) {
        $licenses->isActive = 0;
      }
      else {
        $licenses->isActive = 1;
      }
      $licenses->save();

      return redirect()->route('licenses.index');
  }  
}
