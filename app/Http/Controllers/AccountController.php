<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;
use App\Models\Section;
use App\Models\SubSection;
use App\Models\Region;
use App\Models\Resume;
use App\Models\Portfolio;
use App\Models\License;

use App\Rules\checkImgSize;
use App\Rules\PhoneNumber;

class AccountController extends Controller
{      

  protected function validator()
  {
    return request()->validate([
      'ava_img' => ['nullable', 'array', 'max:3'],
      'ava_img.*' => ['mimes:jpeg,png,jpg,gif,svg'],        
      'Big_img' => ['nullable', 'string'],
      'last_name' => ['required', 'string'],
      'first_name' => ['required', 'string'],
      'whatsapp' => ['required', 'string', new PhoneNumber()],
      'weekday_start_time' => ['required', 'date_format:H:i'],
      'weekday_finish_time' => ['required', 'date_format:H:i'],
      'weekend_start_time' => ['required', 'date_format:H:i'],
      'weekend_finish_time' => ['required', 'date_format:H:i'],
      'sub_section_id' => ['required', 'integer', 'exists:sub_sections,id'],
      // 'section_id' => ['required', 'integer', 'exists:Sections,id'],
      'region_id' => ['required', 'integer', 'exists:regions,id'],
      'cost' => ['required', 'integer'],        
      'site' => ['nullable', 'url'],
      'description' => ['nullable', 'string'],
      'license' => ['nullable', 'array', 'max:10'],
      'license.*' => ['mimes:jpeg,png,jpg,gif,svg'],
      'portfolio' => ['nullable', 'array', 'max:10'],
      'portfolio.*' => ['mimes:jpeg,png,jpg,gif,svg'],
    ]);
  }
  
  public function resumeSave(Request $request)
  {              
    if (!$this->validator()) {
      return response()->json($this->validator(), 422); 
    }    
    $user = Auth::user();
    $resumesCount = Resume::where('user_id', $user->id)->count();
    $resume = new Resume();
    $user->first_name = $request->first_name;
    $user->last_name = $request->last_name;
    $user->patronymic = $request->patronymic;
    $user->save();
    
    $resume->user_id = $user->id;
    if ($request->file('ava_img')) {
      $images = [];      
      foreach ($request->file('ava_img') as $file) {
        $photo = $file->hashName();
        Storage::disk('public')->put('portfolio/'.$user->id, $file);
        array_push($images, $photo);        
        $img_name = $file->getClientOriginalName();
        if (mb_strtolower($img_name) == mb_strtolower($request->Big_img)) {          
          $resume->Big_img = $photo;          
        }        
      }      
      $resume->img1 = json_encode($images);      
      $resume->Big_img = $resume->Big_img ?? $images[0];
    }
        
    $resume->whatsapp = $request->whatsapp;
    $resume->weekday_start_time = $request->weekday_start_time;
    $resume->weekday_finish_time = $request->weekday_finish_time;
    $resume->weekend_start_time = $request->weekend_start_time;
    $resume->weekend_finish_time = $request->weekend_finish_time;
    $resume->cost = $request->cost;
    $resume->site = $request->site;
    $resume->description = $request->description;
    $sub_section = SubSection::find($request->sub_section_id);
    $resume->section_id = $sub_section->section_id;
    $resume->sub_section_id = $request->sub_section_id;
    $resume->region_id = $request->region_id;
    $resume->viewed = 0;
    if ($resumesCount == 0) {
      $resume->isLicense = 1;
      $resume->isPortfolio = 1;
    }
    $status = $resume->save();

    if ($status) {
      if ($request->file('license') && $resumesCount == 0) {
        $images = [];
        foreach ($request->file('license') as $file) {
          $license = new License();
          $license->img = $file->hashName();
          $license->user_id = $user->id;
          $license->resume_id = $resume->id;
          Storage::disk('public')->put('portfolio/'.$user->id, $file);
          $license->save();
        }
      }

      if ($request->file('portfolio') && $resumesCount == 0) {
        $images = [];
        foreach ($request->file('portfolio') as $file) {
          $portfolio = new Portfolio();
          $portfolio->img = $file->hashName();
          $portfolio->user_id = $user->id;
          $portfolio->resume_id = $resume->id;
          Storage::disk('public')->put('portfolio/'.$user->id, $file);
          $portfolio->save();
        }
      }
    }
    else {
      return response()->json(['message' => "The given data was invalid."], 423);
    }

    return response()->json(['message' => "success"], 200);
  }

  public function updateResume(Request $request, $id)
  {            
    if (!$this->validator()) {
      return response()->json($this->validator(), 422);
    }
    $request->validate([
      'ava_img' => ['nullable', 'array', 'max:3', new checkImgSize($id)],           
    ]);
    $user = Auth::user();
    $resume = Resume::where('id', $id)->where('user_id', $user->id)->first();
    $user->first_name = $request->first_name;
    $user->last_name = $request->last_name;
    $user->patronymic = $request->patronymic;
    $user->save();
    
    if ($request->file('ava_img')) {
      $images = [];
      if ($resume->img1 != null) {
        $images = json_decode($resume->img1);
      }
      foreach ($request->file('ava_img') as $file) {
        $photo = $file->hashName();
        Storage::disk('public')->put('portfolio/'.$user->id, $file);
        array_push($images, $photo);   
        $img_name = $file->getClientOriginalName();
        if (mb_strtolower($img_name) == mb_strtolower($request->Big_img)) {          
          $request->Big_img = $photo;          
        }                     
      }  
      $resume->Big_img = $request->Big_img ?? $resume->Big_img;    
      $resume->img1 = json_encode($images);
    }
    $resume->whatsapp = $request->whatsapp;
    $resume->weekday_start_time = $request->weekday_start_time;
    $resume->weekday_finish_time = $request->weekday_finish_time;
    $resume->weekend_start_time = $request->weekend_start_time;
    $resume->weekend_finish_time = $request->weekend_finish_time;
    $resume->cost = $request->cost;
    $resume->site = $request->site;
    $resume->description = $request->description;
    $sub_section = SubSection::find($request->sub_section_id);
    $resume->section_id = $sub_section->section_id;
    $resume->sub_section_id = $request->sub_section_id;
    $resume->region_id = $request->region_id;   
    if ($request->Big_img != null) {
      $resume->Big_img = $request->Big_img;
    }         
    $status = $resume->save();

    if ($status) {
      if ($request->file('license')) {        
        foreach ($request->file('license') as $file) {
          $license = new License();
          $license->img = $file->hashName();
          $license->user_id = $user->id;
          $license->resume_id = $resume->id;
          Storage::disk('public')->put('portfolio/'.$user->id, $file);
          $license->save();
        }
      }

      if ($request->file('portfolio')) {        
        foreach ($request->file('portfolio') as $file) {
          $portfolio = new Portfolio();
          $portfolio->img = $file->hashName();
          $portfolio->user_id = $user->id;
          $portfolio->resume_id = $resume->id;
          Storage::disk('public')->put('portfolio/'.$user->id, $file);
          $portfolio->save();
        }
      }
    }

    if ($status) {
      return response()->json(['message' => "success"], 200);
    }
    else {
      return response()->json(['message' => "The given data was invalid."], 423);
    }

  }

  public function removeAvaImg(Request $request)
  {
    $resume = Resume::find($request->resume);
    $img_index = $request->img_index;
    if ($resume->img1 != null) {
      $img = json_decode($resume->img1);
      $new_img = [];        
      if (isset($img[$img_index])) {
        Storage::delete('portfolio/'.$resume->user_id.'/'.$img[$img_index]);
        for ($i = 0; $i < count($img); $i++) {
          if ($i != $img_index) {
            array_push($new_img, $img[$i]);
          }
        }
        $resume->img1 = json_encode($new_img);
        $resume->save();
      }      
    }
    return response()->json(['message' => "success"], 200);
  }

  public function removeLicense(Request $request)
  {
    $license = License::find($request->license);      
    if ($license != null) {
      Storage::delete('portfolio/'.$license->user_id.'/'.$license->img);
      $license->delete();
    }
    return response()->json(['message' => "success"], 200);
  }

  public function removePortfolio(Request $request)
  {
    $portfolio = Portfolio::find($request->portfolio);      
    if ($portfolio != null) {
      Storage::delete('portfolio/'.$portfolio->user_id.'/'.$portfolio->img);
      $portfolio->delete();
    }
    return response()->json(['message' => "success"], 200);
  }
  
  protected function isSaveResume()
  {
    $user = Auth::user();
    $resumes = Resume::where('user_id', $user->id)->count();
    if ($resumes > 3) {
      return response()->json(['message' => "error"], 200);
    }
    return response()->json(['message' => "success"], 200);
  }  
}
