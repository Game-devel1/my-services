<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Resume;
use App\Models\Feedback;

class FeedbackController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {    
    return view('admin.feedbacks.index', ['feedbacks' => Feedback::orderBy('id', 'DESC')->get()]);
  }
      
  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
      $feedback = Feedback::find($id);      
      if ($feedback->isActive == 1) {
        $feedback->isActive = 0;
      }
      else {
        $feedback->isActive = 1;
      }
      $feedback->save();

      return redirect()->route('feedbacks.index');
  }
  
  public function validateRequest()
  {
    $rules = request()->validate([
      'name' => 'required|max:30',
    ]);
    return $rules;
  }
}
