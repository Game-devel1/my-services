<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\VerifiesEmails;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\Events\Verified;
use App\Models\User;
use Session;

class VerificationController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Email Verification Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling email verification for any
    | user that recently registered with the application. Emails may also
    | be re-sent if the user didn't receive the original email message.
    |
    */

    use VerifiesEmails;

    /**
     * Where to redirect users after verification.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth')->only('verify');
        $this->middleware('signed')->only('verify');
        $this->middleware('throttle:6,1')->only('verify', 'resend');
    }

    /**
     * Mark the authenticated user's email address as verified.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function verify(Request $request)
    {                
        $user = User::find($request->route('id'));        
        if ($request->route('id') != $user->getKey()) {
            throw new AuthorizationException;
        }        
        // if ($request->route('id') != $request->user()->getKey()) {
        //     throw new AuthorizationException;
        // }
            // var_dump('aqq');die;
        if ($user->hasVerifiedEmail()) {            
            return redirect($this->redirectPath());
        }        
        if ($user->markEmailAsVerified()) {
            event(new Verified($user));
        }

        Session::flash('message', 'Ваша почта подтверждена');
        Session::flash('alert-class', 'alert-success');
        return redirect($this->redirectPath())->with('verified', true);
    }
}
