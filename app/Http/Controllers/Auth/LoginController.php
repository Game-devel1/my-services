<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin';


   

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if ($validator->passes()) {
            if ($this->guard()->attempt($this->credentials($request), $request->filled('remember')))
            {                
                if ($this->guard()->user()->email_verified_at != null){
                    return redirect()->back();
                }
                else {
                    $this->guard()->logout();
                    return response()->json(['errors' => ['email' => ['Вы не подтвердили почту']]], 423);        
                }
            }
            
            return $this->sendFailedLoginResponse($request);
        }

        return response()->json(['error'=>$validator->errors()->all()], 423);
    }

    protected function guard()
    {
        return Auth::guard();
    }
        
}
