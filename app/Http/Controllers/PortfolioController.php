<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Resume;
use App\Models\Portfolio;

class PortfolioController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {        
    return view('admin.portfolios.index', ['portfolios' => Portfolio::orderBy('id', 'DESC')->get()]);
  }
      
  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $portfolios = Portfolio::find($id);      
    if ($portfolios->isActive == 1) {
      $portfolios->isActive = 0;
    }
    else {
      $portfolios->isActive = 1;
    }
    $portfolios->save();

    return redirect()->route('portfolios.index');
  }  
}
