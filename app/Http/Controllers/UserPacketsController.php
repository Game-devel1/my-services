<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Models\User;
use App\Models\Role;
use App\Models\RoleUser;
use App\Models\Packet;
use App\Services\Mailer;

class UserPacketsController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $users = User::whereHas('roles', function ($query) {
      $query->where('name', '!=', 'admin');
    })->get();
    $packets = Packet::all();
    return view('admin.users.index', ['users' => $users, 'packets' => $packets]);
  }
  
  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $user = User::find($id);
    return view('admin.users.update', compact('user'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $user = User::find($id);
    if ($this->validateRequest()) {      
      if ($request->isBan == 2) {
        $user->status = 2;
        $user->save();        
        $data['subject'] = '!!! BAN !!! Вас забанили на сайте '. config('app.name');
        $data['content'] = $request->comment;
        $data['mail'] = $user->email;
        $mailer = new Mailer();
        $mailer->sendEmailReminder($data);
      }
      return redirect()->route('users.index');
    }
    else {
      return redirect()->route('users.edit')->withErrors($this->validateRequest());
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  // public function destroy($id)
  // {
  //   $users = User::findOrFail($id);
  //   $users->delete();
  //   return redirect()->route('users.index');
  // }

  public function validateRequest()
  {
    $rules = request()->validate([
      'isBan' => 'required|integer',
      'comment' => 'nullable|string'
    ]);
    return $rules;
  }
}
