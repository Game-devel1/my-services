<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Models\Region;
use App\Models\Section;

class RegionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      return view('admin.regions.index', ['regions' => Region::orderBy('name', 'ASC')->get()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $regions = new Region();
        return view('admin.regions.create', compact('regions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {     
        if ($this->validateRequest()) {
            Region::create($request->all());
            if ($request->submit == 'Сохранить и добавить еще') {
              return redirect()->route('regions.create');
            }
            return redirect()->route('regions.index');
        }
        else {
            return redirect()->route('regions.create')->withErrors($this->validateRequest());
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('admin.regions.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $regions = Region::find($id);
      return view('admin.regions.update', compact('regions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $regions = Region::find($id);
        if ($this->validateRequest()) {
           $regions->name = $request->name;
           $regions->save();
           return redirect()->route('regions.index');
        }
        else {
            return redirect()->route('regions.edit')->withErrors($this->validateRequest());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $regions = Region::findOrFail($id);
      $regions->delete();
      return redirect()->route('regions.index');
    }

    public function validateRequest()
    {
      $rules = request()->validate([
        'name' => 'required|max:30',
      ]);
      return $rules;
    }
}
