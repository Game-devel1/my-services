<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Models\User;
use App\Models\Role;
use App\Services\Mailer;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
      $users = User::whereHas('roles', function ($query) {
        $query->where('name', '!=', 'admin');
      })->orderBy('id', 'DESC')->get();
    return view('admin.users.index', ['users' => $users]);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $user = User::find($id);
      $isAdmin = $user->hasRole('admin');

    return view('admin.users.update', compact('user', 'isAdmin'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $user = User::find($id);

    if ($this->validateRequest()) {
      $user->passport_status = $request->passport_status;

    $roleId = Role::where('name', '=', 'admin')->first();
    if ($request->is_admin == 'on') {
      $user->attachRole($roleId);
    } else {
      $user->detachRole($roleId);
    }

      $user->save();
      if ($request->isBan == 2) {
        $user->status = 2;
        $user->save();
        $data['subject'] = '!!! BAN !!! Вас забанили на сайте '. config('app.name');
        $data['content'] = $request->comment;
        $data['mail'] = $user->email;
        $mailer = new Mailer();
        $mailer->sendEmailReminder($data);
      }
      else if ($request->isBan == 1 && $user->status == 2) {
        $user->status = 1;
        $user->save();
        $data['subject'] = 'Вас разбанили на сайте '. config('app.name');
        $data['content'] = 'Привет, '.$user->first_name . '<br> Вас разбанили и теперь вы можете зайти на портал';
        $data['mail'] = $user->email;
        $mailer = new Mailer();
        $mailer->sendEmailReminder($data);
      }
      return redirect()->route('users.index');
    }
    else {
      return redirect()->route('users.edit')->withErrors($this->validateRequest());
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  // public function destroy($id)
  // {
  //   $users = User::findOrFail($id);
  //   $users->delete();
  //   return redirect()->route('users.index');
  // }

  public function validateRequest()
  {
    $rules = request()->validate([
      'isBan' => 'required|integer',
      'passport_status' => 'required|integer',
      'comment' => 'nullable|string'
    ]);
    return $rules;
  }

  public function create()
  {
      $user = new User();
      return view('admin.users.create', compact('user'));
  }

  public function store(Request $request)
  {
      if ($this->validateStoreRequest()) {
          $data = $request->all();

          $success = User::create([
              'first_name' => $data['first_name'],
              'last_name' => $data['last_name'],
              'patronymic' => $data['patronymic'],
              'passport_data' => 0,
              'passport_img' => 'null',
              'email_verified_at' => now(),
              'email' => $data['email'],
              'password' => Hash::make($data['password']),
              'status' => '1',
          ]);

          if (isset($data['is_admin']) && $data['is_admin'] == 'on') {
              $roleName = 'admin';
          } else {
              $roleName = 'service_provider';
          }

          if ($success) {
              $user = $success;
              $roleId = Role::where('name', '=', $roleName)->first();
              // role attach alias
              $user->attachRole($roleId); // parameter can be an Role object, array, or id

              $message['subject'] = 'Регистрация на сайте ' . config('app.name');
              $message['content'] = 'Вы были зарегистрированы на сайте '. config('app.name') .
                                    "\n\nВаш пароль: " . $data['password'];
              $message['mail'] = $user->email;
              $mailer = new Mailer();
              //$mailer->sendEmailReminder($message);
          }

          return redirect()->route('users.index');
      }

      return redirect()->route('userss.create')->withErrors($this->validateStoreRequest());
  }

    public function validateStoreRequest()
    {
        $rules = request()->validate([
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'patronymic' => ['nullable', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8'],
        ]);
        return $rules;
    }
}
