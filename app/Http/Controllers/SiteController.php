<?php

namespace App\Http\Controllers;

use App\Models\Packet;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use App\Models\Section;
use App\Models\SubSection;
use App\Models\Region;
use App\Models\Resume;
use App\Models\Portfolio;
use App\Models\License;
use App\Models\Feedback;
use App\Models\AdsBanner;

use Session;

class SiteController extends Controller
{

    public function __construct()
    {
        //$this->middleware('guest')->except('my_portfolio');
    }

    /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Contracts\Support\Renderable
   */
  public function index()
  {
    $sections = Section::all();
    $subsections = SubSection::with('section')->get();
    $regions = Region::orderBy('name', 'ASC')->get();

    $vip_user_ids = User::select('id')->whereHas('roles', function ($query) {
      $query->where('name','vip')->orWhere('name','profi');
    })->get();
    $topUserCards = Resume::whereIn('user_id', $vip_user_ids)->with('user')->with('section')->with('sub_section')->withCount('feedbacks')->inRandomOrder()->limit(3)->get();

    $user_ids = User::whereHas('roles', function ($query) {
        $query->where('name','!=','admin');
    })->whereNotIn('id', $vip_user_ids)->pluck('id');
    $plainUsersCards = Resume::whereIn('user_id', $user_ids)->with('user')->with('section')->with('sub_section')->withCount('feedbacks')->take(3)->orderBy('created_at','DESC')->get();
    $plainUsersCount = 0;
    if (count($user_ids) > 0) {
      $plainUsersCount = Resume::whereIn('user_id', $user_ids)->count();
    }

    $ads_right = AdsBanner::where('isActive', 1)->where('page_name','Главная')->where('name','Правый Блок')->first();
    $ads_bottom = AdsBanner::where('isActive', 1)->where('page_name','Главная')->where('name','Нижний блок')->first();

    return view('site/index', [
        'sections' => $sections,
        'subsections' => $subsections,
        'regions' => $regions,
        'topUserCards' => $topUserCards,
        'plainUsersCards' => $plainUsersCards,
        'plainUsersCount' => $plainUsersCount,
        'ads_right' => $ads_right,
        'ads_bottom' => $ads_bottom,
    ]);
  }

    public function load($limit)
    {
      $vip_user_ids = User::select('id')->whereHas('roles', function ($query) {
        $query->where('name','vip')->orWhere('name','profi');
      })->get();
      $user_ids = User::whereHas('roles', function ($query) {
        $query->where('name','!=','admin');
      })->whereNotIn('id', $vip_user_ids)->pluck('id');
      $plainUsersCards = Resume::whereIn('user_id', $user_ids)->with('user')->with('section')->with('sub_section')->withCount('feedbacks')->take($limit)->orderBy('created_at','DESC')->get();

      return (String) view('site/plainUsers', [
          'plainUsersCards' => $plainUsersCards,
      ]);
    }

  public function my_portfolio()
  {
    $user = Auth::user();
    $resumes = Resume::where('user_id', $user->id)->with('licenses')->with('region')->with('section')->with('sub_section')->withCount('feedbacks')->with('user')->with('portfolios')->get();
    $portfolios = Portfolio::where('user_id', $user->id)->get();
    $licenses = License::where('user_id', $user->id)->get();
    $packets = Packet::all();

    $sections = Section::all();
    $subsections = SubSection::with('section')->get();
    $regions = Region::orderBy('name', 'ASC')->get();
    return view('my_portfolio', compact(['packets', 'user', 'resumes', 'portfolios', 'licenses', 'sections', 'subsections', 'regions']));
  }

  public function about($id)
  {
    $resume = Resume::where('id', $id)
      ->with('region')
      ->with('section')
      ->with('sub_section')
      ->with('user')
      ->with('district')
      ->withCount('feedbacks')
      ->firstOrFail();
    if (!Session::has('viewed')) {
      if (Auth::guest() || Auth::user()->id != $resume->user_id) {
        $resume->viewed = $resume->viewed + 1;
        $resume->save();
        Session::put('viewed', 'viewed');
      }
    }

    $portfolios = Portfolio::where('resume_id', $id)->where('isActive', 1)->get();
    $licenses = License::where('resume_id', $id)->where('isActive', 1)->get();

    $dopUsluga = Resume::where('user_id', $resume->user_id)->get();
    $feedbacks = Feedback::where('resume_id', $resume->id)->where('isActive', 1)->get();

    $vip_user_ids = User::select('id')->whereHas('roles', function ($query) {
        $query->where('name','vip')->orWhere('name','profi');
    })->get();
    $recommendationUsers = Resume::whereIn('user_id', $vip_user_ids)->where('id', '!=', $id)->where('sub_section_id', $resume->sub_section_id)->with('user')->with('section')->with('sub_section')->get();

    $regions = Region::orderBy('name', 'ASC')->get();
    $sections = Section::all();
    $subsections = SubSection::with('section')->get();
    return view('about/index', [
        // 'user' => $user,
        'resume' => $resume,
        'portfolios' => $portfolios,
        'licenses' => $licenses,
        'recommendationUsers' => $recommendationUsers,
        'regions' => $regions,
        'sections' => $sections,
        'subsections' => $subsections,
        'dopUsluga' => $dopUsluga,
        'feedbacks' => $feedbacks,
        'ableToComment' => $this->ableToComment($resume->id, Auth::id(), request()->ip())
    ]);
  }

    public function feedback(Request $request)
    {
        if ($this->ableToComment($request->resume_id, Auth::id(), $request->ip())) {

            $this->validate($request, [
                'resume_id' => 'required|integer|exists:resume,id',
                'user_id' => 'required|integer|exists:users,id',
                'fio' => 'required|string|max:60000|min:1',
                'review' => 'required|string|max:60000|min:1',
            ]);

            $feedback = Feedback::create($request->all());

            if (auth()->check()) {
                $feedback->sender_id = Auth::id();
            } else {
                $feedback->ip = $request->ip();
            }

            $feedback->save();
        }

        return redirect()->back();
    }

    protected function ableToComment($resumeId, $userId, $userIP)
    {
        $feedbackQuery = Feedback::query();
        $feedbackQuery->where('resume_id', $resumeId);

        if (auth()->check()) {
            $feedbackQuery->where('sender_id', $userId);
        } else {
            $feedbackQuery->where('ip', $userIP);
        }
        $feedback = $feedbackQuery->take(1)->get()->toArray();

        return empty($feedback);
    }
}
