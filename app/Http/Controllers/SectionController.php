<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use App\Http\Middleware\IsAdmin;
use App\Models\Section;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class SectionController extends Controller
{    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.sections.index', ['sections' => Section::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $section = new Section();
        return view('admin.sections.create', compact('section'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {     
        if ($this->validateStoreRequest()) {
            $photo = $request->file('image')->hashName();
            Storage::disk('public')->put('', $request->file('image'));
            $request['img'] = $photo;
            Section::create($request->all());
            if ($request->submit == 'Сохранить и добавить еще') {
              return redirect()->route('sections.create');
            }
            return redirect()->route('sections.index');
        }
        else {
            return redirect()->route('sections.create')->withErrors($this->validateStoreRequest());
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('admin.sections.index', ['sections' => Section::findOrFail($id)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $section = Section::find($id);
        return view('admin.sections.update', compact('section'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $section = Section::find($id);
        if ($this->validateUpdateRequest()) {
           $section->name = $request->name;
           if ($request->file('image')) {
                $photo = $request->file('image')->hashName();
                Storage::delete('public/' . $section->img);
                Storage::disk('public')->put('', $request->file('image'));      
                $section->img = $photo;  
           }
           $section->save();
           return redirect()->route('sections.index');
        }
        else {
            return redirect()->route('sections.edit')->withErrors($this->validateUpdateRequest());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $section = Section::findOrFail($id);
        if ($section->img != null) {
            Storage::delete('public/' . $section->img);
        }
        $section->delete();
        return redirect()->route('sections.index');
    }

    public function validateStoreRequest()
    {
        $rules = request()->validate([
            'name' => 'required|max:30',   
            'image' => 'required|image|max:5000'         
        ]);
        return $rules;
    }

    public function validateUpdateRequest()
    {
        $rules = request()->validate([
            'name' => 'required|max:30',   
            'image' => 'nullable|image|max:5000'
        ]);
        return $rules;
    }
}
