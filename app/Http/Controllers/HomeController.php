<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use App\Models\Resume;
use App\Models\Section;
use App\Models\SubSection;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {   
        $usersCount = User::whereHas('roles', function ($query) {
            $query->where('name', '!=', 'admin');
        })->count();

        $usersCountWithEmail = User::whereHas('roles', function ($query) {
            $query->where('name', '!=', 'admin');
        })->whereNotNull('email_verified_at')->count();

        $resumeCount = Resume::count();
        $sections = Section::all();
        foreach ($sections as $section) {
            $section->resumeCount = Resume::where('section_id', $section->id)->count();                        
        }                
        return view('admin/home',[
            'usersCount' => $usersCount,
            'usersCountWithEmail' => $usersCountWithEmail,
            'sections' => $sections,
            'resumeCount' => $resumeCount
        ]);
    }
}
