<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Models\SubSection;
use App\Models\Section;

class SubSectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.sub-sections.index', ['subSections' => SubSection::with('section')->get()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $subSection = new SubSection();
        $sections = Section::all();
        return view('admin.sub-sections.create', compact(['subSection', 'sections']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {     
        if ($this->validateRequest()) {
            SubSection::create($request->all());
            if ($request->submit == 'Сохранить и добавить еще') {
              return redirect()->route('sub-sections.create');
            }
            return redirect()->route('sub-sections.index');
        }
        else {
            return redirect()->route('sub-sections.create')->withErrors($this->validateRequest());
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('admin.sub-sections.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $subSection = SubSection::find($id);
        $sections = Section::all();
        return view('admin.sub-sections.update', compact(['subSection', 'sections']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $section = SubSection::find($id);
        if ($this->validateRequest()) {
           $section->name = $request->name;
           $section->section_id = $request->section_id;
           $section->save();
           return redirect()->route('sub-sections.index');
        }
        else {
            return redirect()->route('sub-sections.edit')->withErrors($this->validateRequest());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $section = SubSection::findOrFail($id);
        $section->delete();
        return redirect()->route('sub-sections.index');
    }

    public function validateRequest()
    {
        $rules = request()->validate([
            'name' => 'required|max:30',   
            'section_id' => 'required|integer'         
        ]);
        return $rules;
    }
}
