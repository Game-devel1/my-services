<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Models\AdsBanner;
use App\Models\Region;

class AdsController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    return view('admin.ads-banner.index', ['ads' => AdsBanner::all()]);
  }
          
  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $ads = AdsBanner::find($id);
    return view('admin.ads-banner.update', compact('ads'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
      $ads = AdsBanner::find($id);
      if ($this->validateRequest()) {
        if ($request->img) {
          $photo = $request->file('img')->hashName();
          Storage::disk('public')->put('', $request->file('img'));            
          $ads->img = $photo;
        }              
        $ads->url = $request->url;
        $ads->save();
          return redirect()->route('ads-banner.index');
      }
      else {
          return redirect()->route('ads-banner.edit')->withErrors($this->validateRequest());
      }
  }
  
  public function validateRequest()
  {
    $rules = request()->validate([
      'img' => 'nullable|image|max:5000',
      'url' => 'required|url|max:5000',
    ]);
    return $rules;
  }
}
