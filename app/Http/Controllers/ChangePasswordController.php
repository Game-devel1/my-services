<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class ChangePasswordController extends Controller
{
   
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function change(Request $request)
    {
        $request->validate([
            'current_password' => ['required', 'string', 'min:8'],
            'new_password' => ['required', 'string', 'min:8'],
            'new_confirm_password' => ['same:new_password'],
        ]);

        if (!\Hash::check($request->current_password, Auth::user()->password)) {
            return response()->json(['message' => "The given data was invalid.", 'errors' => ['current_password' => ["Ваш текущий пароль неверен."]]], 423);
        }       

        User::find(auth()->user()->id)->update(['password' => Hash::make($request->new_password)]);
        dd('Password change successfully.');
    }
}
