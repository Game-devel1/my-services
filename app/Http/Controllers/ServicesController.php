<?php

namespace App\Http\Controllers;

use App\Models\District;
use DB;
use App\Models\Region;
use App\Models\Resume;
use App\Models\Section;
use App\Models\SubSection;
use App\Models\User;
use App\Models\AdsBanner;
use Illuminate\Http\Request;

class ServicesController extends Controller
{

    public function __construct()
    {
        //$this->middleware('guest')->except('my_portfolio');
    }


    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function services($id = null, Request $request)
    {

        $vip_user_ids = User::whereHas('roles', function ($query) {
            $query->where('name','vip')->orWhere('name', 'profi');
        })->pluck('id');

        $user_ids = User::whereHas('roles', function ($query) {
            $query->where('name','!=','admin');
        })->whereNotIn('id', $vip_user_ids)->pluck('id');

        $topUserCards = Resume::query()->withCount('feedbacks')->leftJoin('sub_sections', 'sub_sections.id', '=', 'resume.sub_section_id')->leftJoin('users', 'users.id', '=', 'resume.user_id');
        $plainUserCards = Resume::query()->withCount('feedbacks')->leftJoin('sub_sections', 'sub_sections.id', '=', 'resume.sub_section_id')->leftJoin('users', 'users.id', '=', 'resume.user_id');
        if ($request->filled('section_id')) {
            $topUserCards->where('resume.section_id', $request->section_id);
            $plainUserCards->where('resume.section_id', $request->section_id);
        }
        if ($request->filled('region_id')) {
            $topUserCards->where('region_id', $request->region_id);
            $plainUserCards->where('region_id', $request->region_id);
        }
        if ($request->filled('search')) {

            $searchValues = preg_split('/\s+/', $request->search, -1, PREG_SPLIT_NO_EMPTY);
            foreach ($searchValues as $value) {
                $topUserCards->where('users.first_name', 'like', "%{$value}%")
                ->where('users.last_name', 'like', "%{$value}%")
                ->where('users.patronymic', 'like', "%{$value}%")
                ->orWhere('resume.description', 'like', "%{$request->search}%")
                ->where('sub_sections.name', 'like', "%{$request->search}%");
            }

            foreach ($searchValues as $value) {
                $plainUserCards->where('users.first_name', 'like', "%{$value}%")
                ->where('users.last_name', 'like', "%{$value}%")
                ->where('users.patronymic', 'like', "%{$value}%")
                ->orWhere('resume.description', 'like', "%{$request->search}%")
                ->where('sub_sections.name', 'like', "%{$request->search}%");
            }
        }
        if ($id != null) {
            $topUserCards->where('sub_section_id', $id);
            $plainUserCards->where('sub_section_id', $id);
        }
        $topUserCards = $topUserCards->whereIn('resume.user_id', $vip_user_ids)->limit(3)->inRandomOrder()->get();
        $plainUserCards = $plainUserCards->whereIn('resume.user_id', $user_ids)->orderBy('created_at', 'DESC')->get();

        $plainUserCount = count($plainUserCards);
        $sections = Section::all();
        $subsections = SubSection::with('section')->get();
        $regions = Region::orderBy('name', 'ASC')->get();
        $ads_bottom = AdsBanner::where('isActive', 1)->where('page_name','Раздел услуг')->where('name','Нижний блок')->first();
        $searchParams = $request->query();
        return view('services/index', [
            'sections' => $sections,
            'subsections' => $subsections,
            'regions' => $regions,
            'topUserCards' => $topUserCards,
            'plainUserCards' => $plainUserCount > 2 ? [$plainUserCards[0], $plainUserCards[1]] : $plainUserCards,
            'plainUsersCount' => $plainUserCount,
            'ads_bottom' => $ads_bottom,
            'searchParams' => $searchParams,
            'searchString' => $request->search
        ]);
    }

    public function load($limit, $view, Request $request)
    {
        $vip_user_ids = User::whereHas('roles', function ($query) {
            $query->where('name','vip')->orWhere('name', 'profi');
        })->pluck('id');
        $user_ids = User::whereHas('roles', function ($query) {
            $query->where('name','!=','admin');
        })->whereNotIn('id', $vip_user_ids)->pluck('id');

        $plainUserCards = Resume::query()->withCount('feedbacks')->leftJoin('sub_sections', 'sub_sections.id', '=', 'resume.sub_section_id');

        if ($request->filled('section_id')) {
            $plainUserCards->where('resume.section_id', $request->section_id);
        }
        if ($request->filled('region_id')) {
            $plainUserCards->where('resume.region_id', $request->region_id);
        }
        if ($request->filled('search')) {
            $plainUserCards
            ->orWhere('resume.description', 'like', "%{$request->search}%")
            ->orWhere('sub_sections.name', 'like', "%{$request->search}%");

            $searchValues = preg_split('/\s+/', $request->search, -1, PREG_SPLIT_NO_EMPTY);
            $plainUserCards->leftJoin('users', function($q) use ($searchValues) {
                $q->on('users.id', '=', 'resume.user_id');
                foreach ($searchValues as $value) {
                    $q->orWhere('users.first_name', 'like', "%{$value}%");
                    $q->orWhere('users.last_name', 'like', "%{$value}%");
                    $q->orWhere('users.patronymic', 'like', "%{$value}%");
                }
            });
        }
        $plainUserCards = $plainUserCards->whereIn('resume.user_id', $user_ids)->orderBy('created_at', 'DESC')->take($limit)->get();
        return (String) view('services/'.$view, [
            'plainUserCards' => $plainUserCards
        ]);
    }

    public function search($search = null)
    {
        // $resumes = Resume::where('description', 'LIKE', "%$search%")
        //             ->select(DB::raw('CONCAT(users.first_name, " ", users.last_name, " ", users.patronymic) as fio'), 'sections.name as section_name', 'sub_sections.name as sub_section_name', 'id', 'section_id', 'sub_section_id')
        //             ->join('sub_sections', 'sub_sections.id', '=', 'resume.sub_section_id')
        //             ->orWhere('sub_sections.name', 'LIKE', "%$search%")
        //             ->join('sections', 'sections.id', '=', 'resume.section_id')
        //             ->orWhere('sections.name', 'LIKE', "%$search%")
        //             ->join('users', 'users.id', '=', 'resume.user_id')
        //             ->orWhere('users.first_name', 'LIKE', "%$search%")
        //             ->orWhere('users.last_name', 'LIKE', "%$search%")
        //             ->orWhere('users.patronymic', 'LIKE', "%$search%")
        //             ->get();

        $sub_sections = SubSection::where('name', 'LIKE', "%{$search}%")->get();
        $resumes = Resume::where('description', 'LIKE', "%{$search}%")->get();
        $searchValues = preg_split('/\s+/', $search, -1, PREG_SPLIT_NO_EMPTY);
        $users = User::query()->select(DB::raw('CONCAT(first_name, " ", last_name, " ", patronymic) as fio'), 'id');
            foreach ($searchValues as $value) {
                $users->orWhere('first_name', 'like', "%{$value}%");
                $users->orWhere('last_name', 'like', "%{$value}%");
                $users->orWhere('patronymic', 'like', "%{$value}%");
            }
            $users = $users->get();
        return response()->json(['sub_sections' => $sub_sections, 'resumes' => $resumes, 'users' => $users], 200);
    }

    public function filter(Request $request) {
        $resumes = Resume::query();
        if ($request->district != 'null') {
            $resumes->where('district_id', $request->district)->orWhereNull('district_id');
        }
        if ($request->section != 'null') {
            $resumes->where('section_id', $request->section);
        }
        if ($request->sub_section != 'null') {
            $resumes->where('sub_section_id', $request->sub_section);
        }
        if ($request->region != 'null') {
            $resumes->where('region_id', $request->region);
        }
        if ($request->isPortfolio != 'null')
        {
            $resumes->where('isPortfolio', 1);
        }
        if ($request->cost == 'low')
        {
            $resumes->orderBy('cost', 'DESC');
        }
        if ($request->cost == 'high')
        {
            $resumes->orderBy('cost', 'ASC');
        }
        if ($request->date == 'new')
        {
            $resumes->orderBy('created_at', 'ASC');
        }
        if ($request->date == 'old')
        {
            $resumes->orderBy('created_at', 'DESC');
        }

        $filtered_resumes = $resumes->with('user.roles')->with('section')->with('sub_section')->withCount('feedbacks')->get();

        return $filtered_resumes;
    }

    public function getSubSection($section) {
        $sub_section = SubSection::where('section_id', $section)->get();

        return $sub_section;
    }

    public function getDistricts($region) {
        $districts = District::where('region_id', $region)->get();

        return $districts;
    }
}
