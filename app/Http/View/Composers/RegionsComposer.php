<?php

namespace App\Http\View\Composers;

use App\Repositories\UserRepository;
use Illuminate\View\View;

use App\Models\Region;
use App\Models\Section;
use App\Models\SubSection;

class RegionsComposer
{
    /**
     * Create a new Regions composer.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $regions = Region::all();
        $sections = Section::all();
        $sub_sections = SubSection::all();
        $view->with('regions', $regions);
    }
}