<?php 

namespace App\Models;

use Zizaco\Entrust\EntrustRole;

class RoleUser extends EntrustRole
{  

  /**
  * Связанная с моделью таблица.
  *
  * @var string
  */
  protected $table = 'role_user';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'user_id', 'role_id', 'expire_time'
  ];

  
  function user() {
    return $this->hasMany("App\Models\User", "id", "user_id");
  }
}