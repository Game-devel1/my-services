<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Resume extends Model
{
    /**
    * Связанная с моделью таблица.
    *
    * @var string
    */
    protected $table = 'resume';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id','section_id','sub_section_id','region_id','cost', 'description', 'Big_img', 'img1', 'img2', 'img3','site', 'weekday_start_time','weekday_finish_time','weekend_start_time','weekend_finish_time','whatsapp'
    ];

    function licenses() {
        return $this->hasMany("App\Models\License", "resume_id", "id");
    }

    function portfolios() {
        return $this->hasMany("App\Models\Portfolio", "resume_id", "id");
    }

    function user() {
        return $this->hasOne("App\Models\User", "id", "user_id");
    }

    function section() {
        return $this->hasOne("App\Models\Section", "id", "section_id");
    }

    function sub_section() {
        return $this->hasOne("App\Models\SubSection", "id", "sub_section_id");
    }

    function region() {
        return $this->hasOne("App\Models\Region", "id", "region_id");
    }

	 function feedbacks() {
        return $this->hasMany("App\Models\Feedback", "resume_id", "id");
    }

    function district() {
        return $this->hasOne("App\Models\District", "id", "district_id");
    }
}
