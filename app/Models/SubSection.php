<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubSection extends Model
{
    /**
    * Связанная с моделью таблица.
    *
    * @var string
    */
    protected $table = 'sub_sections';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'section_id',
    ];

    function section() {
        return $this->hasOne("App\Models\Section", "id", "section_id");
    }
}
