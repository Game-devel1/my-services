<?php 

namespace App\Models;

use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole
{
  /**
  * Связанная с моделью таблица.
  *
  * @var string
  */
  protected $table = 'roles';
}