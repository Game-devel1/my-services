<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Portfolio extends Model
{  
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'img','user_id', 'resume_id', 'isActive'
    ];

    function user() {
        return $this->hasOne("App\Models\User", "id", "user_id");
    }
}
