<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    /**
    * Связанная с моделью таблица.
    *
    * @var string
    */
    protected $table = 'feedbacks';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fio','review', 'resume_id', 'user_id', 'ip', 'sender_id'
    ];

    function user() {
        return $this->hasOne("App\Models\User", "id", "user_id");
    }

}
