<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdsBanner extends Model
{
    /**
    * Связанная с моделью таблица.
    *
    * @var string
    */
    protected $table = 'ads_banners';
}
