<?php

namespace App\Services;
use Mail;

class Mailer{

    public function sendEmailReminder($data)
    {
        Mail::send([], $data, function ($message) use ($data) {
            if (isset($data['mail'])) {
                $message->to([$data['mail']]);
            }
            else
                $message->to([env('MAIL_USERNAME')]);
            $message->subject($data['subject']);
            $message->setBody($data['content']);
            $message->from(env('MAIL_FROM_ADDRESS'));        
            if (isset($data['file'])) {
                $message->attach($data['file']->getRealPath(), array(
                    'as' => 'file.' . $data['file']->getClientOriginalExtension(),
                    'mime' => $data['file']->getMimeType())
                );
            }
            
        });

        return "Success";
    }
}

    
?>