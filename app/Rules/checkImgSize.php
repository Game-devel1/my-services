<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Models\Resume;

class checkImgSize implements Rule
{
    public $id;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $resume = Resume::find($this->id);
        $img_count = 0;
        if ($resume->img1 !=  null) {
            $img_count = count(json_decode($resume->img1));
        }
        if ((count($value) + $img_count) > 3) {
            return false;
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'максимальное количество картинок 3';
    }
}
