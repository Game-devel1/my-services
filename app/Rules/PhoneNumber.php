<?php
namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Models\Resume;

class PhoneNumber implements Rule
{
  
  /**
   * Determine if the validation rule passes.
   *
   * @param  string  $attribute
   * @param  mixed  $value
   * @return bool
   */
  public function passes($attribute, $value)
  {

    return strlen(preg_replace('/[^0-9]/', '', $value)) == 11;
  }

  /**
   * Get the validation error message.
   *
   * @return string
   */
  public function message()
  {
      return 'Поле :attribute имеет ошибочный формат.';
  }
}
?>