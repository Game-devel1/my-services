$(document).ready(function () {    
    var _token = $('meta[name="csrf-token"]').attr('content');
    var header_height = $('.header').height() + 50;
    $('#loginForm, #regForm, #password-reset, #changePassForm').on('submit', function(e){
        e.preventDefault();
        var modal = $(this).parents('.modal');
        var loader = $(modal).find('.modal-loader');
        $(loader).show();
        $(modal).find('.error_word').html('');
        var formData = new FormData($(this)[0]);        
        $.ajax({
            url: $(this).attr('action'),
            method: $(this).attr('method'),
            cache: false,
            processData: false,
            contentType: false,
            data: formData,
            dataType: 'json',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            before: function () { $(loader).show(); },
            success: function(){             
                modalResponse(modal)
            },
            error:function(error)
            {
                if(typeof error.responseJSON != "undefined") {
                    let errors = error.responseJSON.errors;
                    if(errors){
                        for(let key in errors)
                        {
                            let errorDiv = $(modal).find(`.error_word[data-error="${key}"]`);
                            if(errorDiv.length )
                            {
                                errorDiv.text(errors[key][0]);
                            }
                        }
                    }
                }else {
                    modalResponse(modal)
                }
                setTimeout(function () {
                    $(loader).hide();
                }, 800)
            }
        });
    });

     function modalResponse(modal) {
        if ( $(modal).is("#signUpModal") ) {
            $("#signUpModal").modal('hide');
            $('#reg_btn').trigger('click');
            setTimeout(function () {
                location.reload();
            }, 4000)
        }else if($(modal).is("#passwordModal")) {
            $('#passwordModal').modal('hide');
            $('#password_btn').trigger('click');
            setTimeout(function () {
                location.reload();
            }, 4000)
        }else {
            location.reload();
        }
    }

    $('.show_subsection').click(function (e) {
        e.preventDefault();
        var id = $(this).data('id');
        $('.post_section').removeClass('active');
        $('.post_section').removeAttr('style');
        $('#services-inactive').removeAttr('style');
        $('.services__clicked_block').css('display', 'none');
        var box = $('.post_section[data-id="'+ id +'"]');        
        $(box).addClass('active');
        $('html, body').animate({scrollTop: $(box).offset().top - header_height}, 300);
        $('.top_specialistsAbsolute').css('top','1520px');
        $('.newListingsAbsolute').css('top','2460px');
    })

    $('.services__clicked_block .right .close').click(function (e) {
        e.preventDefault();
        $('.post_section').removeClass('active');
        $('.top_specialistsAbsolute').css('top','1056px');
        $('.newListingsAbsolute').css('top','1964px');
    })
    
    var moreBtn = $('#load-more');
    var maxLimit = $(moreBtn).data('max');
    if(maxLimit < 3){
        $(moreBtn).hide();
    }
    $(moreBtn).on('click', function (e) {
        e.preventDefault();
        var limit = parseInt($(this).data('limit'));
        var nextLimit = limit + 3;
        var max = $(this).data('max');
        if(limit > max){
            limit = max;
        }
        $.ajax({
            url: '/load/' + limit,
            method: 'GET',
            headers: {
                'X-CSRF-TOKEN': _token
            },
            success: function (data) {
                $(moreBtn).data('limit', nextLimit).attr('data-limit', nextLimit);
                $('#post-plain-users').html(data);
                if(limit == max){
                    $(moreBtn).hide();
                    return false;
                }
            }
        })
    });


    var serviceLoadBtn = $('.load_service');
    var maxService = $(serviceLoadBtn).data('max');
    if(maxService <= 2){
        $(serviceLoadBtn).hide();
    }
    $(serviceLoadBtn).on('click', function (e) {
        e.preventDefault();
        var limit = parseInt($(this).data('limit'));
        var nextLimit = limit + 2;
        var max = $(this).data('max');
        var sort = $(this).data('sort');
        if(limit > max){
            limit = max;
        }
        var post_box = $('.userPlainContainer');
        var render = 'usersPlain';
        if($(this).hasClass('mob_load')){
            render = 'usersPlainMobile';
            post_box = $('.post-load-mobile');
        }
        $.ajax({
            url:  '/service-load/' + limit + '/'+render,
            method: 'GET',
            data: sort,
            headers: {
                'X-CSRF-TOKEN': _token
            },
            success: function (data) {
                $(serviceLoadBtn).data('limit', nextLimit).attr('data-limit', nextLimit);
                $(post_box).html(data);
                if(limit == max){
                    $(serviceLoadBtn).hide();
                    return false;
                }
            }
        })
    });


    $('.items_title').on('click', function () {
        var parent = $(this).parents('.items');
        var dropDown = $(parent).find('.sub-items');
        $(parent).toggleClass('active');
        $(this).toggleClass('active');
        if($(parent).hasClass('active')){
            $(dropDown).slideDown();
        }else {
            $(dropDown).slideUp();
        }
    });
   
    $('#search_name, #searchHeader').on('keyup', function () {
        var value = $(this).val();
        var input  = $(this)
        $.ajax({
            url: '/search/'+value,
            method: 'get',
            headers: {
                'X-CSRF-TOKEN': _token
            },
            success: function (data) {
                generateSearchList(data)                
            },
            error: function(error) {
                console.log(error)
            }
        })
    })
    
    function generateSearchList(data) {
        var content = $('.prompt_words'),
        list = `
                <div class = 'prompt_words__info' id="prompt_words__sections">                    
                    <ul>`;
        content.empty();                
        for (var i = 0; i < data.sub_sections.length; i++) {
            list = list + `
                <li data-name="sub_section" data-id="${data.sub_sections[i].id}">
                    ${data.sub_sections[i].name}
                </li>                        
            `
        }
        for (var i = 0; i < data.resumes.length; i++) {
            list = list + `
                <li data-name="resume" data-id="${data.resumes[i].id}">
                    ${data.resumes[i].description}
                </li>                        
            `
        }
        for (var i = 0; i < data.users.length; i++) {
            if (data.users[i].fio != null) {
                list = list + `
                    <li data-name="user" data-id="${data.users[i].id}">
                        ${data.users[i].fio}
                    </li>                        
                `
            }            
        }                
        list = list + `
                </ul>
            </div>
        `

        content.append(list)
    }
        
});



$('.searchElem').keyup(function(){
    valueInput = $(this).val();
    if (valueInput.length > 0){
        $('.prompt_words').addClass('active');        
    }
    else{
        $('.prompt_words').removeClass('active');
    }
})

$('body').on('click', '.prompt_words ul li', function(){
    let valueLi = $.trim($(this).html());    
    $('.searchElem').val(valueLi);
    $('.prompt_words').removeClass('active');
})


jQuery(function($){
	$(document).mouseup(function (e){ // событие клика по веб-документу
		var div = $(".searchPromptParent"); // тут указываем ID элемента
		if (!div.is(e.target) // если клик был не по нашему блоку
		    && div.has(e.target).length === 0) { // и не по его дочерним элементам
                $('.prompt_words').removeClass('active');
		}
	});
});