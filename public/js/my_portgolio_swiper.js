var ava_img_list = new FormData(),
    portfolio_list = new FormData(),
    license_list = new FormData();
$(document).ready(function() {    
  var _token = $('meta[name="csrf-token"]').attr('content');
  function checkResume() {
    var isSaveResume = false;
    $.ajax({
      url: '/account/check-resume',
      method: 'post',
      dataType: 'json',
      headers: {
        'X-CSRF-TOKEN': _token
      },
      success: function (data) {
        if (data['message'] == 'success') {
          isSaveResume = true;
        }
      },
      error: function (error) {
        console.log(error)
      }
    })

    return isSaveResume;
  }

  $('#createResume, #createResumeMob').on('submit', function (e) {
    e.preventDefault();
    if (checkResume) {
      var formData = new FormData($(this)[0]);
      formData.delete('ava_img[]');
      formData.delete('portfolio[]');
      formData.delete('license[]');
      for (var pair of ava_img_list.entries()) {
        formData.append('ava_img[]', pair[1]);
      }
      for (var pair of portfolio_list.entries()) {
        formData.append('portfolio[]', pair[1])
      }
      for (var pair of license_list.entries()) {
        formData.append('license[]', pair[1])
      }
      var form = $(this);
      $.ajax({
        url: $(this).attr('action'),
        method: $(this).attr('method'),
        processData: false,
        contentType: false,
        data: formData,
        dataType: 'json',
        beforeSend: function () {
          $('.resume_submit').prop("disabled", true);
          $(form).find(".error_word").html("");
        },
        success: function (data) {          
          location.reload();
        },
        error: function (error) {
          if (typeof error.responseJSON != "undefined") {
            $('.resume_submit').prop("disabled", false);
            let errors = error.responseJSON.errors;
            if (errors) {
              for (let key in errors) {
                let errorDiv = $(form).find(`.error_word[data-error="${key}"]`);
                if (errorDiv.length) {
                  errorDiv.text(errors[key][0]);
                }
              }
            }
          }
          else {
            location.reload();
          }
        }
      })
    }

  });

  $(".sub_section__item").click(function () {
    var currentele = $(this).html();
    console.log(currentele);
    $('.default_section_ul li').html(currentele);
    $(this).parents(".list_of_specialists").removeClass("active");
  })
  $(".city_change").click(function () {
    var currentele = $(this).html();
    console.log(currentele);
    $('.default_select_city li').html(currentele);
    $(this).parents(".list_of_specialists").removeClass("active");
  })

  var portfolio_count = $('#portfolioCount').data('id');
  var license_count = $('#licenseCount').data('id');
  var _token = $('meta[name="csrf-token"]').attr('content');
  for (var i = 1; i <= portfolio_count; i++) {     
    if ($('#more_swiper_portfolio' + i).length != 0) {
      new Swiper('#more_swiper_portfolio'+i, {
        navigation: {
          nextEl: '#more_right_partfolio' + i,
          prevEl: '#more_left_partfolio' + i,
        },
        spaceBetween: 10,
        slidesPerView: 6,
        // loop: true,
      });
    }
    else {
      new Swiper('#swiper_portfolio'+i, {
        navigation: {
          nextEl: '#right_partfolio' + i,
          prevEl: '#left_partfolio' + i,
        },
        spaceBetween: 10,
        slidesPerView: 3,
        // loop: true,
      });
    } 
  }

  for (var i = 1; i <= license_count; i++) {

    if ($('#more_swiper_license' + i).length != 0) {
      new Swiper('#more_swiper_license'+i, {
        navigation: {
          nextEl: '#more_right_license' + i,
          prevEl: '#more_left_license' + i,
        },
        spaceBetween: 10,
        slidesPerView: 6,
        // loop: true,
      });
    }
    else {
      new Swiper('#swiper_license'+i, {
        navigation: {
          nextEl: '#right_license' + i,
          prevEl: '#left_license' + i,
        },
        spaceBetween: 10,
        slidesPerView: 3,
        // loop: true,
      });
    }    
  }

  $('.sub_section__item').on('click', function() {    
    $('#sub_section_id').val($(this).data('id'))
    $('#sub_section_id_mob').val($(this).data('id'))
  })

  $('.city_change').on('click', function () {
    $('#region_id').val($(this).data('id'))
    $('#region_id_mob').val($(this).data('id'))
  })

  $(".change_resume").click(function () {
    var resume = $(this).data('id');
    var resume_form = $(".change_page");    
    if (resume.img1 !== null && resume.img1.length > 0) {
      for (var i = 0; i < resume.img1.length; i++) {
        resume_form.find('.images').prepend(`          
            <div class="image_button">
              <form style="width: 25%;" action = "/removeAva" method="post">
                <input type="hidden" name="_token" value="${_token}">
                <input type="hidden" name="img_index" value="${i}">
                <input type="hidden" name="resume" value="${resume.id}">
                <button type="submit" class='removeAvaForm' style="color: #c8d400; font-size: 1.5rem; cursor: pointer;">x</button>
              </form>
              <div class='img_btn_img'>
                <img style="width: 84%" src='/storage/portfolio/${resume.user_id}/${resume.img1[i]}' />
              </div>
              <input type="radio" name="Big_img" value="${resume.img1[i]}" style="display: none;" id="Big_img${i}" ${resume.Big_img == resume.img1[i] ? 'checked' : ''} >
              <label class="checkboxLabel" for="Big_img${i}">Сделать основным</label>
            </div>          
      `)        
      }
    }

    if (resume.isPortfolio) {      
      let content = `
        <div class="partgolio_change">
          <div class="partgolio_change__title"> Портфолио: Размер фотографии 270х190 </div>
          <div class="partgolio_change__images_partfolio flex"> `;
        if (resume.portfolios != 'null' && resume.portfolios.length > 0) { 
          for (var i = 0; i < resume.portfolios.length; i++) {
            content += `     
              <form action="/removePortfolio" class="portfolioRm" method="post" style="width: 25%;">
                <input type="hidden" name="_token" value="${_token}">
                <input type="hidden" name="portfolio" value="${resume.portfolios[i].id}">
                <button type="submit" class="portfolioForm" style="color: #c8d400; font-size: 1.5rem; cursor: pointer;">x</button>
                <img style="width: 84%; object-fit: cover;" src="/storage/portfolio/${resume.user_id}/${resume.portfolios[i].img}" />
              </form>
            `
          }          
        }          
        content += `       
            <div style="width: 25%; margin-top: 9px;">     
              <button type="submit" class="" style="color: #c8d400; font-size: 1.5rem; cursor: pointer;"></button>
              <div style="width: 84%;" class="download_image_block">                
                <input type="file" name="portfolio[]" id="portfolio" class="inputfile" data-multiple-caption='{count} файлов выбрано' size="10" multiple="multiple" />
                <label for="portfolio">
                  <p>Загрузить фотографию </p>
                  <i class="icon-download_arrow"></i>
                </label>
              </div>
            </div>
            <span class = "error_word" data-error="portfolio"></span>
          </div>
        </div>`
      resume_form.find('#portfolio_resume').append(content)             
    }
    if (resume.isLicense) {                      
      let content = `
        <div class="partgolio_change">
          <div class="partgolio_change__title"> Лицензии и сертификаты : Размер фотографии 270х190 </div>
          <div class="partgolio_change__images_partfolio flex">`;
      if (resume.licenses != 'null' && resume.licenses.length > 0) {
        for (var i = 0; i < resume.licenses.length; i++) {
          content += `
            <form action="/removeLicense" class="licenseRm" method="post" style="width: 25%;">
              <input type="hidden" name="_token" value="${_token}">
              <input type="hidden" name="license" value="${resume.licenses[i].id}">
              <button type="submit" class="licenseForm" style="color: #c8d400; font-size: 1.5rem; cursor: pointer;">x</button>
              <img style="width: 84%; object-fit: cover;" src="/storage/portfolio/${resume.user_id}/${resume.licenses[i].img}" />
            </form>
            `
        }
      }

      content += `
          <div style="width: 25%; margin-top: 9px;">
            <button type="submit" class="" style="color: #c8d400; font-size: 1.5rem; cursor: pointer;"></button>
            <div style = "width: 84%" class="download_image_block">              
              <input type="file" name="license[]" id="license" class="inputfile" data-multiple-caption='{count} файлов выбрано' size="10" multiple="multiple" />
              <label for="license">
                <p>Загрузить фотографию </p>
                <i class="icon-download_arrow"></i>
              </label>
            </div>
          </div>
          <span class="error_word" data-error="license"></span>
        </div>
      </div>
      `     
      resume_form.find('#portfolio_resume').append(content)
      resume_form.find('#portfolio_resume').append(`
        <div class = "flex" style = "align-items:baseline">
          <i class = "icon-Star"></i>
          <p>Лицензии и портфолио появляются на сайте после подтверждения модератором</p>
        </div>
      `)
    }
    $('#createResume').attr('action', '/updateResume/'+resume.id);

    resume_form.find('#whatsapp').val(resume.whatsapp)
    
    resume_form.find('#weekday_start_time').val(resume.weekday_start_time === null ? '' : resume.weekday_start_time.substr(0, 5))
    resume_form.find('#weekday_finish_time').val(resume.weekday_finish_time === null ? '' : resume.weekday_finish_time.substr(0, 5))
    resume_form.find('#weekend_start_time').val(resume.weekend_start_time === null ? '' : resume.weekend_start_time.substr(0, 5))
    resume_form.find('#weekend_finish_time').val(resume.weekend_finish_time === null ? '' : resume.weekend_finish_time.substr(0, 5))

    resume_form.find('#site').val(resume.site)
    resume_form.find('#cost').val(resume.cost)
    resume_form.find('#description').val(resume.description)
    resume_form.find('#sub_section_id').val(resume.sub_section_id)
    resume_form.find('.default_section_ul li').text(resume.sub_section.name)
    resume_form.find('#region_id').val(resume.region_id)
    resume_form.find('#default_city').text(resume.region.name)

    $(".change_page").css("display", "block"),
    $(".my_cvs").css("display", "none"),
    $(".additional_services").css("display", "none")
  })

  $('.change_resume_mob').click(function() {
    var resume = $(this).data('id');
    var resume_form = $('.change_page_mob');
    $('#resumeFormTitle').text('Редактировать услугу')
    $('#createResumeMob').attr('action', '/updateResume/' + resume.id);
 
    resume_form.find('#whatsapp_mob').val(resume.whatsapp)

    resume_form.find('#weekday_start_time_mob').val(resume.weekday_start_time === null ? '' : resume.weekday_start_time.substr(0, 5))
    resume_form.find('#weekday_finish_time_mob').val(resume.weekday_finish_time === null ? '' : resume.weekday_finish_time.substr(0, 5))
    resume_form.find('#weekend_start_time_mob').val(resume.weekend_start_time === null ? '' : resume.weekend_start_time.substr(0, 5))
    resume_form.find('#weekend_finish_time_mob').val(resume.weekend_finish_time === null ? '' : resume.weekend_finish_time.substr(0, 5))

    resume_form.find('#site_mob').val(resume.site)
    resume_form.find('#cost_mob').val(resume.cost)
    resume_form.find('#description_mob').val(resume.description)
    resume_form.find('#sub_section_id_mob').val(resume.sub_section_id)
    resume_form.find('#default_section_ul_mob li').text(resume.sub_section.name)
    resume_form.find('#region_id_mob').val(resume.region_id)
    resume_form.find('#default_city_mob').text(resume.region.name)

    $('.change_page_mob').css('display', 'block')
    $('.my_cvs_mob').css('display', 'none');
    $('.table_mob_my_port').css('display', 'none');
  })

  function previewImages() {

    var $preview = $('.download_image_block:first');
    if (this.files) $.each(this.files, readAndPreview);

    function readAndPreview(i, file) {
      if (!/\.(jpe?g|png|gif|svg)$/i.test(file.name)) {
        return alert(file.name + " Это не картинка!");
      } // else...            
      var reader = new FileReader();

      $(reader).on("load", function () {
        let img_cont = ``,
          ava_img_count = $('.image_button').length;
        ava_img_count++;
        ava_img_list.append(ava_img_count, file);
        img_cont += `
                  <div class="image_button">
                    <form style="width: 25%;">
                      <button type="submit" class="removeAvaForm" style="color: #c8d400; font-size: 1.5rem; cursor: pointer;">x</button>
                    </form>
                    <div class="img_btn_img">
                      <img style="width: 84%" src="${this.result}">
                    </div>
                    <input type="radio" name="Big_img" value="${file.name}" style="display: none;" id="${ava_img_count}">
                    <label class="checkboxLabel" for="${ava_img_count}">Сделать основным</label>
                  </div>`;        
        $preview.before(img_cont)        
      });

      reader.readAsDataURL(file);
    }
  }
  function previewImagesDown() {

    var $preview = $('.download_image_block:eq(1)');
      // img_cont = $('.img_preview:first').clone();
    if (this.files) $.each(this.files, readAndPreview);

    function readAndPreview(i, file) {

      if (!/\.(jpe?g|png|gif|svg)$/i.test(file.name)) {
        return alert(file.name + " Это не картинка!");
      } // else...

      var reader = new FileReader();

      $(reader).on("load", function () {
        var portfolioRm = $(".portfolioRm").length,
        img_cont = ``
        portfolioRm++;
        portfolio_list.append(portfolioRm, file)
        img_cont = `
              <form action="/removePortfolio" class="portfolioRm" method="post" style="width: 25%;">
                <button type="submit" class="portfolioForm" data-id="${portfolioRm}" style="color: #c8d400; font-size: 1.5rem; cursor: pointer;">x</button>
                <img style="width: 84%; object-fit: cover;" src="${this.result}">
              </form>
        `
        $preview.before($(img_cont))
      });

      reader.readAsDataURL(file);
    }

  }
  function previewImagesDownSecond() {

    var $preview = $('.download_image_block:eq(2)');
      // img_cont = $('.img_preview:first').clone();
    if (this.files) $.each(this.files, readAndPreview);

    function readAndPreview(i, file) {

      if (!/\.(jpe?g|png|gif|svg)$/i.test(file.name)) {
        return alert(file.name + " Это не картинка!");
      } // else...

      var reader = new FileReader();

      $(reader).on("load", function () {
        var licenseRm = $('.licenseRm').length, 
        img_cont = ``
        licenseRm++;
        license_list.append(licenseRm, file)
        img_cont = `
              <form action="/removeLicense" class="licenseRm" method="post" style="width: 25%;">
                <button type="submit" class="licenseForm" data-id="${licenseRm}" style="color: #c8d400; font-size: 1.5rem; cursor: pointer;">x</button>
                <img style="width: 84%; object-fit: cover;" src="${this.result}">
              </form>
        `
        $preview.before($(img_cont))
      });

      reader.readAsDataURL(file);

    }

  }

  $('#ava_img').on("change", previewImages);
  $('#portfolio').on("change", previewImagesDown);
  $('#license').on("change", previewImagesDownSecond);    
})

$('body').on('click', '.licenseForm, .portfolioForm, .removeAvaForm', function (e) {
  e.preventDefault();  
  var form = $(this).closest('form');  
  var button = $(this)  
  if ($(form).find('input[type=hidden]').length == 0) {   
    if ($(button).hasClass("removeAvaForm")) {      
      ava_img_list.delete($(form).parent().find('input[type=radio]').attr('id'))
      $(form).parent().remove();
    }
    else if ($(button).hasClass("licenseForm")) {
      license_list.delete($(button).data('id'))
      $(form).remove()
    }
    else if ($(button).hasClass("portfolioForm")) {
      portfolio_list.delete($(button).data('id'))
      $(form).remove()
    }
    return false;
  }
  var formData = new FormData($(form)[0]);
  $.ajax({
    url: $(form).attr('action'),
    method: $(form).attr('method'),
    processData: false,
    contentType: false,
    data: formData,
    dataType: 'json',
    success: function (data) {
      if ($(button).hasClass("removeAvaForm")) {        
        $(form).parent().remove()
      }
      else {      
        $(form).remove()
      }      
    }
  })
});


jQuery(function($){
	$(document).mouseup(function (e){ // событие клика по веб-документу
		var div = $(".select_wrap"); // тут указываем ID элемента
		if (!div.is(e.target) // если клик был не по нашему блоку
        && div.has(e.target).length === 0) { // и не по его дочерним элементам
          $('.select_wrap').removeClass('active');
    }
    let citySelect = $('#citySelect');
    
    let countCityClick = 0;

    let countSpecialistsClick = 0;
    $('#citySelect').click(function(){

      countCityClick++;
    })
    $('#specializationSelect').click(function(){
      countSpecialistsClick++;
    })
    let specializationSelect = $('#specializationSelect');
    if (!citySelect.is(e.target) // если клик был не по нашему блоку
        && citySelect.has(e.target).length === 0) { // и не по его дочерним элементам
          $('.list_of_specialists').removeClass('active');
          console.log('hiiiiiiiiiiiiii');
    }
    if (!specializationSelect.is(e.target) // если клик был не по нашему блоку
        && specializationSelect.has(e.target).length === 0) { // и не по его дочерним элементам
          $('.list_of_specialists').removeClass('active');
    }
    else{
      if(countCityClick % 2 === 0 || countSpecialistsClick % 2 === 0){
        console.log('uuuu');
        $('.list_of_specialists').removeClass('active');
      }
    }
    
	});
});


var mobPhotoService = new Swiper('.mob-swiper_services', {
  navigation: {
    nextEl: '.right_photo',
    prevEl: '.left_photo',
  },
  spaceBetween:10,
  slidesPerView: 2,
  loop:true,
});

// $(document).ready(function(){
//   let clickCount = 0;
//   $('#specializationSelect').click(function(){
//     clickCount +=1;
//     // console.log(clickCount);
//     if(clickCount % 2 === 0){
//       $('.section_change').css('display','none');
//       console.log(clickCount % 2);
//       // clickCount++;
//     }
//     else{
//       $('.section_change').css('display','block');
//       console.log('else');
//     }
//   })
  
//   // console.log(clickCount);
//   // if(clickCount % 2 === 0){
//   //   $('.section_change').css('display','none');
//   //   console.log(clickCount % 2);
//   //   // clickCount++;
//   // }
//   // else{
//   //   $('.section_change').css('display','block');
//   //   console.log('else');
//   // }
// })