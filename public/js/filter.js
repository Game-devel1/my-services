
var section_id = null;
var sub_section_id = null;
var region_id = null;
var district_id = null;
var isPortfolio = null;
var date = null;
var cost = null;
var all_sub_section = [];
var _token = $('meta[name="csrf-token"]').attr('content');
function getAllResumes() {
  $.ajax({
    url: '/getAllResume?section=' + section_id + '&sub_section=' + sub_section_id + '&region=' + region_id + '&isPortfolio=' + isPortfolio + '&date=' + date + '&cost=' + cost + '&district=' + district_id,
    method: 'get',
    headers: {
      'X-CSRF-TOKEN': _token
    },
    success: function (data) {
      generate(data)
    }
  })
}

function generate(data) {
  var role = null;
  var container = $('.filter__right');
  var container_mob = $('.mob_allServices')

  $(container).find('.right_inner').empty()
  $(container).find('.userPlainContainer').empty()

  $(container_mob).find('.top-specialists_mob').remove()
  $(container_mob).find('.mob_listings').empty()
  if (data.length > 0) {
    if (window.screen.width > 992) {
      for (var i = 0; i < data.length; i++) {
        role = getRole(data[i].user.roles)
        if (role != 'service_provider') {
          $(container).find('.right_inner').prepend(renderTopUser(data[i], role))
        }else {
          $(container).find('.userPlainContainer').prepend(renderPlainUser(data[i]))
        }
      }
    }
    else {
      for (var i = 0; i < data.length; i++) {
        role = getRole(data[i].user.roles)
        if (role != 'service_provider') {
          $(container_mob).find('.unfiltered_bock').prepend(renderTopUserMob(data[i], role))
        } else {
          $(container_mob).find('.mob_listings').prepend(renderPlainUserMob(data[i]))
        }
      }
    }
  }
}

function getRole(roles) {
  for (var i = 0; i < roles.length; i++) {
    if (roles[i].name == 'vip') {
      return 'vip';
    }
    else if (roles[i].name == 'profi') {
      return 'profi';
    }
  }
  return 'service_provider';
}

function renderTopUser(item, role) {
  var content = `
        <div class="top-specialists__information ${role == 'vip' ? 'vip_border' : 'profi_border'}">
          <div class="first ${role == 'vip' ? 'vip' : 'profi'}">`
      if (role == 'vip') {
        content += `
              <div class="first__top-title flex">
                  <p class="upper">Vip</p>
                  <div class="diamonds"></div>
              </div>`
      }
      else {
        content += `
              <div class="first__top-title flex">
                  <p class="upper">Profi</p>
                  <div class="brief"></div>
              </div>
        `
      }
      content += `
            <div class="first__avatar" style="background-image: url('${item.Big_img != null ? '/storage/portfolio/' + item.user_id+'/' + item.Big_img : ''}');">
                <div class="review_col">
                    <p>отзывы <span>${item.feedbacks_count}</span></p>
                </div>
            </div>
        </div>
        <div class="second flex">
          <div class="dots">
            <span class="dots__dot"></span>
            <span class="dots__dot"></span>
            <span class="dots__dot"></span>
            <span class="dots__dot"></span>
            <span class="dots__dot"></span>
            <span class="dots__dot"></span>
            <span class="dots__dot"></span>
            <span class="dots__dot"></span>
            <span class="dots__dot"></span>
            <span class="dots__dot"></span>
          </div>
          <div class="second__information">
            <p class="second__position">${item.sub_section.name}</p>
            <p class="second__name">${item.user.first_name} ${item.user.last_name} ${ item.user.patronimyc != null ? item.user.patronimyc : '' }</p>
            <div class="ability">
              ${item.description != null ? item.description : ''}
            </div>
            <div class="buttons flex justify-content-around">
                <form action="/about/${item.id}" target = "_blank">
                    <button class="btn btn_small">Подробнее</button>
                </form>
                <form action="https://api.whatsapp.com/send?phone=${item.whatsapp}" target="_blank">
                    <button class="btn btn_small">WhatsApp</button>
                </form>
            </div>
          </div>
        </div>
      </div>
  `
  return content;
}

function renderTopUserMob(item, role)
{

  var content = `
    <div class="top-specialists_mob ">
      <div class="block">
        <div class="left flex">`
        if (role == 'vip') {
          content += `
            <div class="vip_diamond">
              <img src="/images/samples/vip.png" />
              <p>VIP</p>
            </div>
          `
        }
        else {
          content += `
            <div class="vip_diamond">
              <div class="brief"></div>
              <p>Profi</p>
            </div>
          `
        }
      content += `
          <div class="image_avatar">`
        if (item.Big_img != null) {
          content += `
            <img src="/storage/portfolio/${item.user_id}/${item.Big_img}" />
          `
        }
      content += `
          </div>
        </div>
        <div class="right">
          <div class="title"> ${item.sub_section.name} </div>
          <div class="name"> ${item.user.first_name} ${item.user.last_name} ${item.user.patronimyc != null ? item.user.patronimyc : ''} </div>
          <div class="details">
            ${item.description != null ? item.description : ''}
          </div>
          <div class="buttons flex">
            <form action="/about/${item.id}" target = "_blank">
                <button class="btn btn_small">Подробнее</button>
            </form>
            <form action="https://api.whatsapp.com/send?phone=${item.whatsapp}" target="_blank">
                <button class="btn btn_small">WhatsApp</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  `

  return content;
}

function renderPlainUser(item) {
  var content = `
    <div class="listings_blocks flex" style='margin-left: 0;margin-top: 40px;'>
        <div class="listings_blocks__left">
            <div class="avatar" style="background-image: url('${item.Big_img != null ? '/storage/portfolio/' + item.user_id + '/' + item.Big_img : ''}')"></div>
        </div>
        <div class="listings_blocks__rigth">
            <div class="flex justify-content-between">
                <div class="emlpoyee_info">
                    <p class="employee_position">${item.section.name}</p>
                    <p class="employee_name">${item.user.first_name} ${item.user.last_name} ${item.user.patronimyc != null ? item.user.patronimyc : ''} <span>${item.sub_section.name}</span></p>
                </div>
                <div class="listings_blocks_buttons">
                <form action="https://api.whatsapp.com/send?phone=${item.whatsapp}" target = "_blank">
                  <button class="btn btn_writeWhatssap">Написать в WhatsApp</button>
                </form>
                <form action="/about/${item.id}" target="_blank">
                  <button class="btn btn_details">Подробнее</button>
                </form>
                </div>
            </div>
            <div class="reviews_right">
                <div class="review_flexed flex ">
                    <i class="icon-review_svg"></i>
                    <p>отзывы: ${item.feedbacks_count}</p>
                </div>
            </div>
        </div>
    </div>
  `

  return content;
}

function renderPlainUserMob(item) {
  var content = `
    <div class="listining_block_mob">
        <div class="left flex">
            <div class="avatar-image-mob">
                <img src="${item.Big_img != null ? '/storage/portfolio/' + item.user_id + '/' + item.Big_img : ''}" />
            </div>
            <div class="left-second">
                <div class="top-title"> ${item.section.name} </div>
                <div class="name"> ${item.user.first_name} ${item.user.last_name} ${item.user.patronimyc != null ? item.user.patronimyc : ''} </div>
                <div class="info">${item.sub_section.name}</div>
            </div>
        </div>
        <div class="right flex">
            <div class="reviews flex">
                <i class="icon-review_svg"></i>
                <p>отзывы: ${item.feedbacks_count}</p>
            </div>
            <div class="buttons flex">
                <button onclick="location.href='/about/${item.id}'" class="btn btn_btnSmallMob flex">
                    <i class="icon-mob_arrow_near_button"></i> Подробнее </button>
                <button onclick="location.href='https://api.whatsapp.com/send?phone=${item.whatsapp}'" class="btn btn_btnSmallMob flex">
                    <i class="icon-mob_arrow_near_button"></i> WhatsApp </button>
            </div>
        </div>
    </div>
  `
}

function getSubSections() {
  $.ajax({
    url: '/getSubSection/'+section_id,
    method: 'get',
    headers: {
      'X-CSRF-TOKEN': _token
    },
    success: function (data) {
      generateSubSection(data)
    }
  })
}

function generateSubSection(data) {
  var content = $('#filter_sub_section_list');
  content.empty()
  if (data.length > 0) {
    for (var i = 0; i < data.length; i++) {
      content.append(`
        <li class = "option subsection_li sub_section_filter" data-id="${data[i].id}" data-section="${data[i].section_id}">${data[i].name}</li>
      `)
    }
  }
}

function getDistricts(regionId) {
    $.ajax({
        url: '/getDistricts/' + regionId,
        method: 'get',
        headers: {
            'X-CSRF-TOKEN': _token
        },
        success: function (data) {
            renderDistrictlist(data);
        }
    })
}

function renderDistrictlist(data) {
    var districtSelect = $('#district-select');
    var districtSelectMob = $('#district-select_mob');
    districtSelect.empty();
    districtSelectMob.empty();
    if (data.length > 0) {
        for (var i in data) {
            var li = `<li class="option district_li district_filter" href="#" data-id="${data[i].id}">${data[i].name}</li>`;
            districtSelect.append(li);
            districtSelectMob.append(li);
        }
    }

    $('#district_title').text('район');

    $(".district_li").click(function() {
        var e=$(this).html();
        $("#district_title").html(e);
        $(this).parents(".select_wrap").removeClass("active");
        $(".default-option li").css("color","#00657b");
        $('#ditrict_title').text(($(this).text()));
    });
}

$('.city_section_mob_filter_li').click(function () {

});

$(document).ready(function() {

  $('body').on('click', '.section_filter, .sub_section_filter, .region_filter, .portfolio_filter, .price_filter, .data_filter, .district_filter', function() {
    var value = $(this).data('id')
    if ($(this).hasClass('section_filter')) {
      section_id = value;
      getSubSections()
    }
    else if ($(this).hasClass('sub_section_filter')) {
      sub_section_id = value;
    }
    else if ($(this).hasClass('region_filter')) {
      region_id = value;
        district_id = null;
        getDistricts(value);
    }
    else if ($(this).hasClass('portfolio_filter')) {
      isPortfolio = value;
    }
    else if ($(this).hasClass('price_filter')) {
      cost = value;
    }
    else if ($(this).hasClass('data_filter')) {
      date = value;
    }
    else if ($(this).hasClass('district_filter')) {
        district_id = value;
    }
    getAllResumes()
  })

  // $("#datalist1").select2({
  //   tags: true
  // });
})
