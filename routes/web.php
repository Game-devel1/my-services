<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', 'SiteController@index')->name('home');

Route::get('/load/{limit}', 'SiteController@load')->name('load');

Route::get('/about/{id}', 'SiteController@about')->name('about');

Route::get('/my_portfolio', 'SiteController@my_portfolio')->name('my_portfolio')->middleware('user');

Route::get('/services/{id?}', 'ServicesController@services')->name('services');

Route::get('/service-load/{limit}/{view}', 'ServicesController@load')->name('load');

Route::get('/search/{search?}', 'ServicesController@search')->name('search');

Route::get('/getAllResume', 'ServicesController@filter')->name('filter');

Route::get('/getSubSection/{section}', 'ServicesController@getSubSection')->name('getSubSection');

Route::get('/getDistricts/{region}', 'ServicesController@getDistricts')->name('getDistricts');

Route::post('/feedback', 'SiteController@feedback')->name('feedback');
// Route::get('/search', function () {
//     return view('search');
// })->name('search');

// Route::get('/services_btn-mob', function () {
//     return view('services_btn-mob');
// })->name('services_btn-mob');

// Route::get('/filter-mob', function () {
//     return view('filter-mob');
// })->name('filter-mob');


Auth::routes(['verify' => true]);
Route::post('/change-password', 'ChangePasswordController@change')->name('change.password')->middleware('user');
Route::post('/account/create-resume', 'AccountController@resumeSave')->name('create.resume')->middleware('user');
Route::post('/account/check-resume', 'AccountController@isSaveResume')->name('check.resume')->middleware('user');
Route::post('/removeAva', 'AccountController@removeAvaImg')->name('removeAvaImg')->middleware('user');
Route::post('/removeLicense', 'AccountController@removeLicense')->name('removeLicense')->middleware('user');
Route::post('/removePortfolio', 'AccountController@removePortfolio')->name('removePortfolio')->middleware('user');
Route::post('/updateResume/{id}', 'AccountController@updateResume')->name('updateResume')->middleware('user');

Route::get('/admin', 'HomeController@index')->name('admin')->middleware('admin');
Route::group(['prefix' => 'admin', 'middleware' => 'admin'], function () {
    Route::resource('sections', 'SectionController');
    Route::resource('sub-sections', 'SubSectionController');
    Route::resource('regions', 'RegionController');
    Route::resource('packets', 'PacketController');
    Route::resource('feedbacks', 'FeedbackController');
    Route::resource('licenses', 'LicenseController');
    Route::resource('portfolios', 'PortfolioController');
    Route::resource('ads-banner', 'AdsController');
    Route::get('users', 'UserController@index')->name('users.index');
    Route::get('users/{user}/edit', 'UserController@edit')->name('user.edit');
    Route::put('users/{user}', 'UserController@update')->name('user.update');
    Route::get('users/create', 'UserController@create')->name('users.create');
    Route::post('users/store', 'UserController@store')->name('users.store');
});

// Настройки ролей

// Route::get('/role', 'Controller@index')->name('role');

// Route::get('/addRole', 'Controller@addRole')->name('addRole');


