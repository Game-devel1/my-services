@extends('layouts.main')
@section('pageTitle','Мой профиль')
@section('content')
@section('template','my_portfolio')
@section('namespace','my_portfolio')
  <!-- Страница Партфолио. -->
  <div class="fon-wrapper">
    <div class="fon"></div>
  </div>
  <div class="images_back">
    <div class="stamp">
      <img src="images/samples/stamp.png" />
    </div>
    <div class="leaf3">
      <img src="images/samples/leaf3.png" />
    </div>
    <div class="leaf4">
      <img src="images/samples/leaf4.png" />
    </div>
    <div class="leaf9">
      <img src="images/samples/leaf9.png" />
    </div>
    <div class="cup">
      <img src="images/samples/cup.png" />
    </div>
  </div>
  <div class="container-fluid">
      <div class="modal" id="changePassword" role="dialog" aria-labelledby="changePassword" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role='document' id="changePasswordModal">
          @include('layouts._modal_change_pass')
        </div>
      </div>
      <div class="topTitle webTitle">
          <span>МОИ УСЛУГИ</span>
          <p class="topTitle__p">портал объединяющий заказчиков <br> и исполнителей услуг</p>
      </div>
      <div class="topTitle mob">
          <span>МОИ УСЛУГИ</span>
          <p class="topTitle__p">портал объединяющий заказчиков и исполнителей услуг</p>
      </div>
    <div class="modal" id="changePassword" role="dialog" aria-labelledby="changePassword" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role='document' id="changePasswordModal">
        @include('layouts._modal_change_pass')
      </div>
    </div>
    <div class="web_my_partfolio">
      <form id="searchForm" action="{{route('services')}}" method="get" class = "searchPromptParent">
        <div class="menu_card_services flex">
          <div class="menu_card_services__search-block">
            <input type="text" class="searchElem" placeholder="Поиск" name="search" list = 'search_list' id="search_name" autocomplete="off">
            <div class="prompt_words">
              <div class = 'prompt_words__info' id="prompt_words__sections">
                  <!-- <div class="title">Разделы</div> -->
                  <ul>
                      <li>
                          Дом
                      </li>
                      <li>
                        Интернет услуги
                      </li>
                      <li>
                        Образование
                      </li>
                      <li>
                        Красота и здоровье
                      </li>
                      <li>
                        Менеджмент
                      </li>
                      <li>
                        Автомобиль
                      </li>
                  </ul>
              </div>
            </div>
          </div>
          <button class="btn btn_large btn_shadow">Искать</button>
        </div>
      </form>
      <div class="portfolio flex">
        <div class="portfolio__left">
          <div class="left-title"> МОЙ ПРОФИЛЬ </div>
          <div class="cv_buttons">
            <div class="cv-first flex justify-content-between" id="my_cv">
              <p>Мои услуги</p>
              <i class="icon-dropdown-arrow"></i>
            </div>
            <div class="cv-first flex justify-content-between" id="additional_services">
              <p>Дополнительные услуги</p>
              <i class="icon-dropdown-arrow"></i>
            </div>
          </div>
          <div class="add_cv_buttons flex">
            @if ($resumes->isEmpty())
              <button class="btn btn_btnWithIcon btn-add flex" id="show_changed_btn">
                <i class="icon-plusik"></i> Добавить услугу </button>
            @endif
            <button class="btn btn_btnWithIcon btn-add flex justify-content-between" data-toggle="modal" data-target="#changePassword" data-backdrop="false">
              <i class="icon-lock"></i> Изменить пароль </button>
          </div>
        </div>
        <div class="portfolio__right ">
          @if (!$resumes->isEmpty())
            <div class="my_cvs">
              @php
                $count = 1;
                $licenseCount = 1;
                $portfolioCount = 1;
                $month = ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'];
              @endphp
              @foreach ($resumes as $resume)
                @php
                  $date = date_parse($resume->created_at);
                  $dateFormat = $date['day'] . ' ' . $month[$date['month']-1] . ' ' . $date['year'];
                  if ($resume->img1 != null) {
                    $resume->img1 = json_decode($resume->img1);
                  }
                @endphp
                @if ($count > 1)
                  <div class="portfolio__right_under">
                @endif
                <div class="top-title flex">
                  <div class="top-date">{{$dateFormat}} г.</div>
                  <div class="cv_number">Услуга №{{$count}}</div>
                </div>
                <div class="about-worker flex justify-content-between">
                  <div class="worker-images">
                    <div class="top-block ">
                      @if ($user->hasRole('profi'))
                      <div class="status">
                        <img src="/images/samples/profi.png" />
                        <p>PROFI</p>
                      </div>
                      @elseif ($user->hasRole('vip'))
                      <div class="status">
                        <img src="/images/samples/diamond_black.png" />
                        <p>VIP</p>
                      </div>
                      @endif
                      <a href="{{$resume->Big_img != null ? asset('storage/portfolio/').'/'.$user->id.'/'.$resume->Big_img : '' }}" data-fancybox="gallery-10">
                        <div class="image_avatar" style="background-image: url('{{$resume->Big_img != null ? asset('storage/portfolio/').'/'.$user->id.'/'.$resume->Big_img : '' }}')">
                        </div>
                      </a>
                    </div>
                      <div class="images_avatar_bottom flex justify-content-between">
                        @php
                          $images1 = [];
                          if ($resume->Big_img != null) {
                            foreach ($resume->img1 as $img) {
                              if ($img != $resume->Big_img) {
                                array_push($images1, $img);
                              }
                            }
                          }
                        @endphp
                        @if (isset($images1[0]))
                        <a href="{{ asset('storage/portfolio/').'/'.$user->id.'/'.$images1[0] }}" data-fancybox="gallery-10"><img src="{{ asset('storage/portfolio/').'/'.$user->id.'/'.$images1[0] }}" /></a>
                        @endif
                        @if (isset($images1[1]))
                          <a href="{{ asset('storage/portfolio/').'/'.$user->id.'/'.$images1[1] }}" data-fancybox="gallery-10"><img src="{{ asset('storage/portfolio/').'/'.$user->id.'/'.$images1[1] }}" /></a>
                        @endif
                      </div>
                  </div>
                  <div class="worker-info">
                    <div class="first_half">
                      <div class="name_button flex justify-content-between">
                        <p>{{$user->first_name .' '.$user->last_name}}<br> {{$user->patronymic}}</p>
                        <div>
                          <button class = "btn flex btn_btnChangeIcon change_resume" data-id="{{$resume}}">
                            <i class="icon-change"></i>
                            Редактировать услугу
                          </button>
                        </div>
                      </div>
                      <div class="questions flex">
                        <div class="first_block flex ">
                          <div class="fhalf">
                            <span class="question">Специализация:</span>
                            <span class="question">Стоимость:</span>
                            <span class="question">WhatsApp:</span>
                            <span class="question">Документ:</span>
                          </div>
                          <div class="shalf">
                            <span class="answear"> {{isset($resume->section) ? $resume->section->name : ''}} - {{isset($resume->sub_section) ? $resume->sub_section->name : ''}}</span>
                            <span class="answear subToThousand">{{number_format($resume->cost, 0, '', ' ')}} тг</span>
                            <a href="tel:8777777777" class="answear">{{$resume->whatsapp}}</a>
                            <span class="answear">{{$user->passport_status == 1 ? 'Подтверждено' : 'Не подтверждено'}}</span>
                          </div>
                        </div>
                        <div class="second_block flex">
                          <div class="fhalf">
                            <span class="question">Город:</span>
                            <span class="question">Район:</span>
                            <span class="question">Почта:</span>
                            <span class="question">Часы работы:</span>
                          </div>
                          <div class="shalf">
                            <span class="answear">{{isset($resume->region) ? $resume->region->name : '' }}</span>
                            <span class="answear">{{isset($resume->region) ? $resume->region->name : '' }}</span>
                            <span class="answear">{{$user->email}}</span>
                            <span class="answear">Будни: {{mb_substr($resume->weekday_start_time, 0, 5)}} - {{mb_substr($resume->weekday_finish_time, 0, 5)}} <br> Выходные: {{ mb_substr($resume->weekend_start_time, 0, 5)}} - {{mb_substr($resume->weekend_finish_time, 0, 5)}}</span>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="details_abot_worker">
                      <div class="title">Описание:</div>
                      <div class="info"> {{$resume->description}} </div>
                    </div>
                  </div>
                </div>
                <div class="swiper_avatar flex justify-content-between">
                  @if (!$resume->portfolios->isEmpty())
                    <div style="{{ $resume->licenses->isEmpty() ? 'width: 100%;' : '' }}" class="portfolio_license__block">
                      <div class="title_arrows flex">
                        <div class="title">Портфолио:</div>
                        <div class="arrow flex">
                          <div class="left" id="{{ $resume->portfolios->isEmpty() ? 'more_left_partfolio'.$portfolioCount : 'left_partfolio'.$portfolioCount }}">
                            <span class="figure_left"></span>
                          </div>
                          <div class="right" id="{{ $resume->portfolios->isEmpty() ? 'more_right_partfolio'.$portfolioCount : 'right_partfolio'.$portfolioCount }}">
                            <span class="figure_right"></span>
                          </div>
                        </div>
                      </div>
                      <div class="images swiper-container {{ !$resume->portfolios->isEmpty() ? 'flex' : '' }}" id="{{ $resume->portfolios->isEmpty() ? 'more_swiper_portfolio'.$portfolioCount : 'swiper_portfolio'.$portfolioCount }}">
                        <div class="swiper-wrapper">
                          @foreach ($resume->portfolios as $portfolio)
                            <div class="swiper-slide portfolioSlide">
                              <a href="{{ asset('storage/portfolio/').'/'. $user->id.'/'.$portfolio->img }}" data-fancybox="gallery-11"><img src="{{ asset('storage/portfolio/').'/'.$user->id.'/'.$portfolio->img }}" /></a>
                            </div>
                          @endforeach
                        </div>
                      </div>
                    </div>
                    @php
                      $portfolioCount++;
                    @endphp
                  @endif
                  @if (!$resume->licenses->isEmpty())
                    <div style="{{ $resume->portfolios->isEmpty() ? 'width: 100%;' : '' }}" class="portfolio_license__block">
                      <div class="title_arrows flex">
                        <div class="title">Лицензии и сертификаты:</div>
                        <div class="arrow flex">
                          <div class="left" id="{{ $resume->licenses->isEmpty() ? 'more_left_license'.$licenseCount : 'left_license'.$licenseCount }}">
                            <span class="figure_left"></span>
                          </div>
                          <div class="right" id="{{ $resume->licenses->isEmpty() ? 'more_right_license'.$licenseCount : 'right_license'.$licenseCount }}">
                            <span class="figure_right"></span>
                          </div>
                        </div>
                      </div>
                      <div class="images swiper-container {{ !$resume->licenses->isEmpty() ? 'flex' : '' }}" id="{{ $resume->licenses->isEmpty() ? 'more_swiper_license'.$licenseCount : 'swiper_license'.$licenseCount }}">
                        <div class="swiper-wrapper">
                          @foreach ($resume->licenses as $license)
                            <div class="swiper-slide portfolioSlide">
                              <a href="{{ asset('storage/portfolio/').'/'. $user->id.'/'.$license->img }}" data-fancybox="gallery-12"><img src="{{ asset('storage/portfolio/').'/'. $user->id.'/'.$license->img }}" /></a>
                            </div>
                          @endforeach
                        </div>
                      </div>
                    </div>
                    @php
                      $licenseCount++;
                    @endphp
                  @endif
                </div>
                @if ($count > 1)
                  </div>
                @endif
                  <!-- </div> -->
                @php
                  $count++;
                @endphp
              @endforeach
              <div id="portfolioCount" data-id="{{$portfolioCount}}"></div>
              <div id="licenseCount" data-id="{{$licenseCount}}"></div>
            </div>
          @endif
            <div class="additional_services ">
              @if (!$packets->isEmpty())
                <table class="redTable">
                  <thead>
                  <tr>
                  <th class = 'number_table'>№</th>
                  <th class = 'name_table'>Наименование</th>
                  <th class = "period_table">Период</th>
                  <th class = "period_table">Сумма</th>
                  <th>Описание</th>
                  <th class = 'btn_table_w'>Купить</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php $n = 0 ?>
                  @foreach($packets as $packet)
                    <?php $n++ ?>
                    <tr>
                      <td>{{ $n }}</td>
                      <td>{{ $packet->name }}</td>
                      <td>{{ $packet->term }} месяц</td>
                      <td>{{ $packet->cost }}тг</td>
                      <td>{{ $packet->description }}</td>
                      <td>
                        <button class="btn btn_table" data-id="{{ $packet->id }}">Купить услугу</button>
                      </td>
                    </tr>
                  @endforeach
                  </tbody>
                </table>
              @else
                <p>Ничего не найдено</p>
              @endif

            </div>
            @include('changeResumeForm')
          </div>
      </div>
    </div>
    <div class="mob-my_partfolio">
      @if (!$resumes->isEmpty())
        <div class="my_cvs_mob">
          @php 
            $count = 1;
          @endphp
          <div class="swiper-container workers_big_block">
              <div class="flex justify-content-between align-items-center">
                    <div class="prev-big_block" id = 'prev-block_my_port'>
                        <!-- <img src="../images/samples/left_button_mob.png"/> -->
                        <span></span>
                    </div>
                    
                    <div class="cvs_number">Услуга №1</div>
                    <div class="next-big_block" id = 'next-block_my_port'>
                        <!-- <img src="../images/samples/rigth_button_mob.png"/> -->
                        <span></span>
                    </div>
                </div>
            <div class="swiper-wrapper">
              @foreach ($resumes as $resume)
                @php
                  $date = date_parse($resume->created_at);
                  $dateFormat = $date['day'] . ' ' . $month[$date['month']-1] . ' ' . $date['year'];
                @endphp
                <div class="workers swiper-slide">
                  <div class="workers__first">
                    <div>
                      <button class="btn btn_btnSmallMob center-button change_resume_mob" data-id="{{$resume}}">
                          <i class="icon-change"></i> Редактировать услугу
                      </button>
                    </div>
                    <!-- <div class="first_half flex justify-content-between">
                      @if ($user->hasRole('vip'))
                        <div class="vip_or_profi flex">
                          <p>Vip</p>
                          <img src="../images/samples/vip.png" />
                        </div>
                      @elseif ($user->hasRole('profi'))
                        <div class="vip_or_profi flex">
                          <p>Profi</p>
                          <img src="../images/samples/profi.png" />
                        </div>
                      @endif
                      <div class="salary"> {{number_format($resume->cost, 0, '', ' ')}} тг </div>
                    </div> -->
                    <!-- <div class="img_worker_avatar flex" style="background-image: url('{{$resume->Big_img != null ? asset('storage/portfolio/').'/'.$user->id.'/'.$resume->Big_img : '' }}');"></div> -->
                  </div>
                  <div class="workers_second">
                    <div class="name">{{$user->first_name .' '.$user->last_name .' '.$user->patronymic}}</div>
                  </div>
                  <div class="worker_thierd">
                    <div class="questions_answears flex">
                      <div class="question">Специализация:</div>
                      <div class="answear">{{isset($resume->section) ? $resume->section->name : ''}} - {{isset($resume->sub_section) ? $resume->sub_section->name : ''}}</div>
                    </div>
                    <div class="questions_answears flex">
                      <div class="question">Часы работы:</div>
                      <div class="answear">Будни: {{mb_substr($resume->weekday_start_time, 0, 5)}} - {{mb_substr($resume->weekday_finish_time, 0, 5)}}<br> Выходные: {{mb_substr($resume->weekend_start_time, 0, 5)}} - {{mb_substr($resume->weekend_finish_time, 0, 5)}}</div>
                    </div>
                    <div class="questions_answears flex">
                      <div class="question">Город:</div>
                      <div class="answear">{{isset($resume->region) ? $resume->region->name : ''}}</div>
                    </div>
                    <div class="questions_answears flex">
                      <div class="question">Район:</div>
                      <div class="answear">{{isset($resume->region) ? $resume->region->name : ''}}</div>
                    </div>
                    <div class="questions_answears flex">
                      <div class="question">Сайт:</div>
                      <div class="answear">{{$resume->site}}</div>
                    </div>
                    <div class="questions_answears flex">
                      <div class="question">Документ:</div>
                      <div class="answear">{{$user->passport_status == 1 ? 'Подтверждено' : 'Не подтверждено'}}</div>
                    </div>
                    <div class="questions_answears flex">
                      <div class="question">Статус:</div>
                      <div class="answear">VIP</div>
                    </div>
                    <div class="questions_answears flex">
                      <div class="question">Стоимость:</div>
                      <span class="answear subToThousand">{{number_format($resume->cost, 0, '', ' ')}} тг</span>
                    </div>
                  </div>
                  <!-- <div class="workers_fourth">
                    <button class="btn btn_btnSmallMob center-button change_resume_mob" data-id="{{$resume}}">
                      <i class="icon-change"></i> Редактировать профиль </button>
                  </div> -->
                  <div class="workers_fifth">
                    <div class="top_title"> Описание: </div>
                    <div class="info"> {{$resume->description}} </div>
                  </div>
                  <div class="workers_seventh">
                  <div class="title">Фото услуги:</div>
                      <div class="swiper-container mob-swiper_services">

                          <div class="swiper-wrapper">
                                <div class="swiper-slide images-swiper-mob">
                                  <img src="{{$resume->Big_img != null ? asset('storage/portfolio/').'/'.$user->id.'/'.$resume->Big_img : '' }}"/>
                              </div>
                              <div class="swiper-slide images-swiper-mob">
                                @if (isset($images1[0]))
                                  <img src="{{ asset('storage/portfolio/').'/'.$user->id.'/'.$images1[0] }}"/>
                                @endif
                              </div>
                              <div class="swiper-slide images-swiper-mob">
                                
                                @if (isset($images1[1]))
                                  <img src="{{ asset('storage/portfolio/').'/'.$user->id.'/'.$images1[1] }}"/>
                                  @endif
                              </div>
                          </div>
                          <div class="arrows flex">
                              <div class="left left_photo">
                                  <span class="figure_left"></span>
                              </div>
                              <div class="right right_photo">
                                  <span class="figure_right"></span>
                              </div>
                          </div>
                      </div>
                  </div>
                  @if (!$resume->portfolios->isEmpty())
                    <div class="workers_seventh">
                      <div class="title">Портфолио:</div>
                      <div class="swiper-container mob-swiper_partfolio">
                        <div class="swiper-wrapper">
                          @foreach ($resume->portfolios as $portfolio)
                            <div class="swiper-slide images-swiper-mob">
                              <img src="{{asset('storage/portfolio/').'/'.$user->id .'/'. $portfolio->img}}" />
                            </div>
                          @endforeach
                        </div>
                        <div class="arrows flex">
                          <div class="left left_partfolio">
                            <span class="figure_left"></span>
                          </div>
                          <div class="right right_partfolio">
                            <span class="figure_right"></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  @endif
                  @if (!$resume->licenses->isEmpty())
                    <div class="workers_seventh">
                      <div class="title">Лицензии:</div>
                      <div class="swiper-container mob-swiper_license">
                        <div class="swiper-wrapper">
                          @foreach ($resume->licenses as $license)
                            <div class="swiper-slide images-swiper-mob-license">
                              <img src="{{asset('storage/portfolio/').'/'.$user->id .'/'. $license->img}}" />
                            </div>
                          @endforeach
                        </div>
                        <div class="arrows flex">
                          <div class="left left_license">
                            <span class="figure_left"></span>
                          </div>
                          <div class="right right_license">
                            <span class="figure_right"></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  @endif
                  <div class="workers_second">
                  <div class="top-date">На сайте с {{$dateFormat}} года</div>
                    <div class="reviews_shows flex">
                      <div class="reviews flex"> Отзывы:<span>{{ $resume->feedbacks_count }}</span>
                      </div>
                      <div class="reviews flex"> Просмотр:<span>{{$resume->viewed == null ? ' 0' : ' ' . $resume->viewed}}</span>
                      </div>
                    </div>
                  </div>
                </div>
              @endforeach

            </div>
          </div>
        </div>
      @endif
      <div class="table_mob_my_port">
        @if (!$packets->isEmpty())
          @foreach($packets as $packet)
            <div class="table">
              <table class='first_half'>
                <tbody>
                  <tr>
                    <td class='thead_td'>Наименование</td>
                    <td class='tboby_td'>{{$packet->name}}</td>
                  </tr>
                  <tr>
                    <td class='thead_td'>Период</td>
                    <td class='tboby_td'>{{$packet->term}}</td>
                  </tr>
                  <tr>
                    <td class='thead_td'>Сумма</td>
                    <td class='tboby_td'>{{$packet->cost}}тг</td>
                  </tr>
                  <tr>
                    <td class = 'thead_td' >Описание</td>
                    <td class = 'tboby_td'>{{$packet->description}}</td>
                  </tr>
                </tbody>
              </table>
              <table class="end_table">
                <tbody>
                  <tr>
                    <td class='t_btn'><button class="btn btn_small">Купить услугу</button></td>
                  </tr>
                </tbody>
              </table>
            </div>
          @endforeach
        @else
              <p>Ничего не найдено</p>
        @endif
      </div>
      <div class="change_page_mob">
        <div id = "resumeFormTitle" class="title"> Добавить услугу </div>
        <div class="change_img_border" style="display: none;">
          <div class="arrows_mob flex">
            <div class="left " id="mob_left_myPart">
              <span class="figure_left"></span>
            </div>
            <div class="right" id='mob_right_myPart'>
              <span class="figure_right"></span>
            </div>
          </div>
          <div class="images_mob_change swiper-container flex " id="swiper_mob_change_photo">
            <div class="swiper-wrapper">
              <div class="swiper-slide">
                <a href="/images/samples/avatar.jpg" data-fancybox="gallery-5"><img src="/images/samples/avatar.jpg" /></a>
                <p class="do_main_img">Сделать основным</p>
              </div>
              <div class="swiper-slide">
                <a href="/images/samples/avatar.jpg" data-fancybox="gallery-5"><img src="/images/samples/avatar.jpg" /></a>
                <p class="do_main_img">Сделать основным</p>
              </div>
            </div>
          </div>
        </div>
        <div class="title_and_arrow flex">
          <div class="title">*- поле для обязательного заполнения</div>
        </div>
        <form id="createResumeMob" action="{{route('create.resume')}}" method="post" class=" mob_inputs_change">
          {!! csrf_field() !!}
          <label for="last_name_mob">Фамилия*</label>
          <input type="text" id="last_name_mob" name="last_name" value="{{$user->last_name}}">
          <span class = "error_word" data-error="last_name"></span>
          <label for="first_name_mob">Имя*</label>
          <input type="text" id="first_name_mob" name="first_name" value="{{$user->first_name}}">
          <span class = "error_word" data-error="first_name"></span>
          <label for="patronymic_mob">Отчество</label>
          <input type="text" id="patronymic_mob" name="patronymic" value="{{$user->patronymic}}">
          <span class = "error_word" data-error="patronymic"></span>
          <div class="first list_of_specialists">
              <input type="hidden" name="sub_section_id" id="sub_section_id_mob">
              <label for="specialization" class = 'partfoli_change_label specialization_title'>Специализация*</label>
              <ul class = "default_select" id = 'default_section_ul_mob'>
                  <li>Выбрать</li>
              </ul>
              <ul class = 'section_change'>
                @foreach ($sections as $section)
                  <li class = "section_change__item">{{$section->name}}
                    <ul class = "sub_section">
                      @foreach ($subsections as $sub_section)
                        @if ($sub_section->section_id == $section->id)
                          <li data-id="{{$sub_section->id}}" class = "sub_section__item">{{$sub_section->name}}</li>
                        @endif
                      @endforeach
                    </ul>
                  </li>
                @endforeach
              </ul>
          </div>
          <span class = "error_word" data-error="sub_section_id"></span>
          <div class="first">
              <label for="site_mob" class = 'partfoli_change_label'>Сайт</label>
              <input type="text" id = 'site_mob' name = "site">
              <span class = "error_word" data-error="site"></span>
          </div>
          <div class="input_area__flexed_input">
            <div class="half">
              <div class="first list_of_specialists" style = "padding-bottom:21px">
                <input type="hidden" name="region_id" id="region_id_mob">
                <label for="specialization" class = 'partfoli_change_label specialization_title'>Город*</label>
                <ul class = "default_select">
                    <li id = "default_city_mob">Выбрать</li>
                </ul>
                <ul class="section_change cyties_scroll">
                  @foreach ($regions as $region)
                    <li data-id="{{$region->id}}" class = "city_change section_change__item">{{$region->name}}</li>
                  @endforeach                                                                       
                </ul>
              </div>
              
              <span class = "error_word" data-error="region_id"></span>
            </div>
          </div>
          <div class="input_area__flexed_input">
            <div class="half">
              <div class="first list_of_specialists" style = "padding-bottom:21px">
                <input type="hidden" name="region_id" id="region_id_mob">
                <label for="specialization" class = 'partfoli_change_label specialization_title'>Район*</label>
                <ul class = "default_select">
                    <li id = "default_regionPart_mob">Выбрать</li> 
                </ul> 
                <ul class="section_change cyties_scroll">
                  @foreach ($regions as $region)  
                    <li data-id="{{$region->id}}" class = "regionPart_change section_change__item">{{$region->name}}</li>
                  @endforeach                                                                       
                </ul>
              </div>
              <span class = "error_word" data-error="region_id"></span>
            </div>
          </div>
          <label for="cost_mob">Стоимость*</label>
          <input type="text" id="cost_mob" name="cost" maxlength="11">
          <span class = "error_word" data-error="cost"></span>

          <label for="whatsapp_mob">Контакты (WhatsApp)*</label>
          <input type="text" id="whatsapp_mob" name="whatsapp">
          <span class = "error_word" data-error="whatsapp"></span>

          <label for="weekday_start_time_mob">Часы работы (Будни)*</label>
          <input type="time" id="weekday_start_time_mob" name="weekday_start_time">
          <input type="time" id="weekday_finish_time_mob" name="weekday_finish_time">
          <span class = "error_word" data-error="site"></span>

          <label for="weekend_start_time_mob">Часы работы (Выходные)*</label>
          <input type="time" id="weekend_start_time_mob" name="weekend_start_time">
          <input type="time" id="weekend_finish_time_mob" name="weekend_finish_time">
          <span class = "error_word" data-error="weekend_start_time"></span>

          <label for="description_mob">Описание</label>
          <textarea name="description" id="description_mob" maxlength="256"></textarea>
          <span class = "error_word" data-error="description"></span>

          <div class="flex justify-content-center button_saveMob">
            <button type="submit" class='btn btn_saveChangesMob resume_submit'>
              <i class="icon-checked_button"></i> Сохранить редактирование </button>
          </div>
        </form>
      </div>
      <div class="add_cvs_change_pass_mob  flex justify-content-between">
        @if ($resumes->isEmpty())
          <button class="btn btn_mobIconPart flex" id='add_cv'>
            <i class="icon-plusik"></i> Добавить услугу </button>
        @endif
          <div class="btns_my_port">
            <button class='btn btn_small' id='mob_cvs_btn'>Мои услуги</button>
            <button class='btn btn_small' id='mob_table_btn'>Дополнительные услуги</button>
          </div>
      </div>
    </div>
  </div>
  @push('scripts')
      <script src="{{ asset('js/my_portgolio_swiper.js') }}"></script>
  @endpush
@endsection
