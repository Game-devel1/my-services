@extends('layouts.main')
@section('pageTitle','Карточка услуг')
@section('content')
@section('template','about')
@section('namespace','about')
  <div class="fon-wrapper">
    <div class="fon"></div>
  </div>
  <div class="images_back">
    <div class="stamp">
      <img src="{{ asset('images/samples/stamp.png') }}" />
    </div>
    <div class="leaf3">
      <img src="{{ asset('images/samples/Leaf3.png') }}" />
    </div>
    <div class="leaf4">
      <img src="{{ asset('images/samples/Leaf4.png') }}" />
    </div>
    <div class="leaf9">
      <img src="{{ asset('images/samples/Leaf9.png') }}" />
    </div>
    <div class="cup">
      <img src="{{ asset('images/samples/cup.png') }}" />
    </div>
  </div>
  <div class="container">
    <div class="topTitle mob">
        <span>МОИ УСЛУГИ</span>
        <p class="topTitle__p">портал объединяющий заказчиков и исполнителей услуг</p>
    </div>
    <div class="web-about">
      <div class="topTitle webTitle">
          <span>МОИ УСЛУГИ</span>
          <p class="topTitle__p"> портал объединяющий заказчиков <br> и исполнителей услуг</p>
      </div>
      <div class="topTitle mob">
          <p class="topTitle__p"><span>МОИ УСЛУГИ</span> <br>  услуги всего Казахстана</p>
      </div>
      <form id="searchForm" action="{{route('services')}}" method="get" class = "searchPromptParent">
        <div class="menu_card_services flex">
          <div class="menu_card_services__search-block">
            <input type="text" class="searchElem" placeholder="Поиск" name="search" list = 'search_list' id="search_name" autocomplete="off">
            <!-- <datalist id ="search_list"> -->
              <!-- <option data-value="" value="ничего не найдено"></option> -->
            <!-- </datalist> -->
            <div class="prompt_words">
              <div class = 'prompt_words__info' id="prompt_words__sections">
                  <!-- <div class="title">Разделы</div> -->
                  <ul>
                      <li>
                          Дом
                      </li>
                      <li>
                        Интернет услуги
                      </li>
                      <li>
                        Образование
                      </li>
                      <li>
                        Красота и здоровье
                      </li>
                      <li>
                        Менеджмент
                      </li>
                      <li>
                        Автомобиль
                      </li>
                  </ul>
              </div>
            </div>
          </div>
          <button class="btn btn_large btn_shadow">Искать</button>
        </div>
      </form>
      <div class="services_card">
      @if (!empty($resume))
        <div class="services_card__inner">
        <div class="title flex">
          <a href = "/" class="road_link">Главная страница - </a>
          <a href = "/services?search=" class="road_link"> Раздел услуг - </a>
          <a href = "/about/{{ $resume->id }}" class="road_link"> Карточка услуг </a>
        </div>
            @php
              $count = 1;
              $licenseCount = 1;
              $portfolioCount = 1;
              $month = ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'];
            @endphp
            @php
              $date = date_parse($resume->created_at);
              $dateFormat = $date['day'] . ' ' . $month[$date['month']-1] . ' ' . $date['year'];
            @endphp
            <div class="profi_services_card flex">
              <div class="profi_services_card_img flex">
                <div class="first flex <?=$resume->user->hasRole('vip') ? 'vip' : ''?>">
                  @if ($resume->user->hasRole('vip') )
                    <div class="first__top-title flex">
                      <p class="upper">
                          Vip
                      </p>
                      <div class="diamonds"></div>
                    </div>
                  @elseif ($resume->user->hasRole('profi'))
                    <div class="first__top-title flex">
                      <p class="upper">
                        Profi
                      </p>
                      <div class="brief"></div>
                    </div>
                  @endif
                  <div class="first__avatar">
                    @php
                      $avatar = $resume->Big_img;
                      $images = json_decode($resume->img1);
                      if($avatar == null){
                        if($images != null){
                          $avatar = $images[0];
                        }
                      }
                    @endphp
                    @if (isset($avatar))
                      <a href="{{ asset('storage/portfolio/').'/'.$resume->user_id.'/'.$avatar }}" data-fancybox="gallery-10"><img src="{{ asset('storage/portfolio/').'/'.$resume->user_id.'/'.$avatar }}" /></a>
                    @else
                      <img src="{{ asset('public/images/no-img.png') }}" />
                    @endif
                  </div>
                </div>
                <div class="second_card_services">
                  @if($images != null)
                    @php $im = 0; @endphp
                    @foreach($images as $image)
                      @php
                        $im++;
                        if($im != 1){
                        $url = asset('storage/portfolio/').'/'.$resume->user_id.'/'.$image;
                          echo '<a href="'. $url .'" data-fancybox="gallery-10"><img src="'. $url .'" /></a>';
                        }
                      @endphp
                    @endforeach
                  @endif

                </div>
              </div>
              <div class="profi_services_card_information">
                <div class="profi_services_card_information__first flex">
                  <div class="fhalf">
                    <p>{{$resume->user->first_name .' '.$resume->user->last_name}}<br> {{$resume->user->patronymic}}</p>
                    <p class="profession">Специализация: <span>{{isset($resume->section) ? $resume->section->name : ''}} - {{isset($resume->sub_section) ? $resume->sub_section->name : ''}}</span></p>
                  </div>
                  <div class="shalf subToThousand"> {{number_format($resume->cost, 0, '', ' ')}} тг </div>
                </div> 
                <div class="profi_services_card_information__second flex">
                  <div class="fhalf ">
                    <!-- <p>Часы работы:<span>Будни: 9:00 - 18:00 <br> Выходные: 14:00 - 21:00</span></p> -->
                    <div class="fhalf__question flex">
                      <span class="question">Часы работы:</span>
                      <span class="answear">Будни: {{mb_substr($resume->weekday_start_time, 0, 5)}} - {{mb_substr($resume->weekday_finish_time, 0, 5)}} <br> Выходные: {{ mb_substr($resume->weekend_start_time, 0, 5)}} - {{mb_substr($resume->weekend_finish_time, 0, 5)}}</span>
                    </div>
                    <div class="fhalf__question flex">
                      <span class="question">Город:</span>
                      <span class="answear">{{isset($resume->region) ? $resume->region->name : '' }}</span>
                    </div>
                      <?php if (isset($resume->district)): ?>
                          <div class="fhalf__question flex">
                              <span class="question">Район:</span>
                              <span class="answear">{{$resume->district->name}}</span>
                          </div>
                      <?php endif; ?>
                    <!-- <div class="fhalf__question flex">
                      <span class="question">Сайт:</span>
                      <a href="{{isset($resume->site) ? $resume->site : '' }}" class="answear">{{isset($resume->site) ? $resume->site : '' }}</a>
                    </div> -->
                  </div>
                  <div class="shalf">
                    <div class="shalf__question flex">
                      <span class="question">Документ:</span>
                      <span class="answear">{{$resume->user->passport_status == 1 ? 'Подтверждено' : 'Не подтверждено'}}</span>
                    </div>
                    <div class="shalf__question flex">
                      <span class="question">Просмотр:</span>
                      <span class="answear">{{ $resume->viewed }}</span>
                    </div>
                    <div class="shalf__question flex">
                      <span class="question">Отзывы:</span>
                      <a href="#" class="answear"> {{ $resume->feedbacks_count }}</a>
                    </div>
                    <div class="shalf__question flex">
                      <span class="question">Сайт:</span>
                      <a href="{{isset($resume->site) ? $resume->site : '' }}" class="answear">{{isset($resume->site) ? $resume->site : '' }}</a>
                    </div>
                  </div>
                </div>
                <div class="profi_services_card_information__theird flex">
                  <p>На сайте с {{ $dateFormat }} года</p>
                  @if ((Auth::check() && Auth::user()->id != $resume->user_id || !Auth::check()) && $ableToComment)
                    <button class="btn btn_small" data-toggle="modal" data-target="#review_modal" data-backdrop="false">Написать отзыв</button>
                  @endif
                  @if( $resume->whatsapp != null)
                    <form action="https://wa.me/{{str_replace([' ', '(', ')', '-'], '', $resume->whatsapp)}}" target="_blank">
                      <button class="btn btn_small">
                        Написать в WhatsApp
                      </button>
                    </form>
                  @endif
                  </div>
              </div>
            </div>
            <div class="general_information flex">
              <div class="general_information__first">
                <div class="title">Описание:</div>
                <div class="info"> {{ $resume->description }}</div>
              </div>
              @if ($dopUsluga != null)
                <div class="general_information__first">
                  <div class="title">Дополнительная специализация:</div>
                  <div class="info">
                    @foreach ($dopUsluga as $dop)
                        {{ $dop->sub_section->name }} /
                    @endforeach
                    <!-- Разработка сайта / Дизайн логотипов / 3D дизайн  -->
                  </div>
                </div>
              @endif
            </div>
              @include('about.portfolio_lisence')
              @include('about.reviews')
        </div>
      @endif
      </div>
      <div id="portfolioCount" data-id="1"></div>
      <div id="licenseCount" data-id="1"></div>
      @include('about.recommendations')
    </div>
    <div class="mob-about">
        <div onclick="window.history.back();" class="prev-about">
          <p>назад</p>
        </div>
        @if (!empty($resume))
              @php
                $licenseCount = 1;
                $portfolioCount = 1;
                  $date = date_parse($resume->created_at);
                  $dateFormat = $date['day'] . ' ' . $month[$date['month']-1] . ' ' . $date['year'];
              @endphp
              <div class="workers">
                  <div class="workers__first">
                    <div class="first_half flex justify-content-between">
                      <div class="vip_or_profi flex">
                        <p> @if ($resume->user->hasRole('vip'))
                            Vip
                          @elseif ($resume->user->hasRole('profi'))
                            Profi
                          @endif</p>
                        <img src="../images/samples/vip.png" />
                      </div>
                      <div class="salary"> {{ number_format($resume->cost, 0, '', ' ')}} тг </div>
                    </div>
                    @php
                      $avatar = $resume->Big_img;
                      $images = json_decode($resume->img1);
                      if($avatar == null){
                        if($images != null){
                          $avatar = $images[0];
                        }
                      }
                    @endphp
                    <div class="swiper-container workers_big_block">
                      <div class="prev-big_block" id='prev-block'>
                        <img src="../images/samples/left_button_mob.png" />
                      </div>
                      <div class="next-big_block" id='next-block'>
                        <img src="../images/samples/rigth_button_mob.png" />
                      </div>
                      <div class="swiper-wrapper">
                        <div class="swiper-slide">
                          <div class="img_worker_avatar flex" style="background-image: url('{{ asset('storage/portfolio/').'/'.$resume->user_id.'/'.$avatar }}');"></div>
                        </div>
                        <!-- <div class="swiper_slide">
                            @if (isset($avatar))
                            <div class="img_worker_avatar"><a href="{{ asset('storage/portfolio/').'/'.$resume->user_id.'/'.$avatar }}" data-fancybox="gallery-10"><img src="{{ asset('storage/portfolio/').'/'.$resume->user_id.'/'.$avatar }}" /></a></div>
                            @else
                            <div class="img_worker_avatar"><img src="{{ asset('public/images/no-img.png') }}" /></div>
                            @endif
                        </div> -->
                        @if($images != null)
                        @php $im = 0; @endphp
                        @foreach($images as $image)
                          @php
                            $im++;
                            if($im != 1){
                            $url = asset('storage/portfolio/').'/'.$resume->user_id.'/'.$image;
                              echo '<div class = "swiper-slide">
                                <div class = "img_worker_avatar">
                                    <a href="'. $url .'" data-fancybox="gallery-10"><img src="'. $url .'" /></a>
                                </div>
                              
                                </div>';
                                
                            }
                          @endphp
                        @endforeach
                      @endif
                      </div>
                    </div>

                  </div>
                  <div class="workers_second">
                    <div class="top-date">На сайте с {{ $dateFormat }} года</div>
                    <div class="name">{{$resume->user->first_name .' '.$resume->user->last_name}}<br> {{$resume->user->patronymic}}</div>
                  </div>
                  <div class="worker_thierd">
                    <div class="questions_answears flex">
                      <div class="question">Специализация:</div>
                      <div class="answear">{{isset($resume->section) ? $resume->section->name : ''}} - {{isset($resume->sub_section) ? $resume->sub_section->name : ''}}</div>
                    </div>
                    <div class="questions_answears flex">
                      <div class="question">Часы работы:</div>
                      <div class="answear">Будни: {{mb_substr($resume->weekday_start_time, 0, 5)}} - {{mb_substr($resume->weekday_finish_time, 0, 5)}} <br> Выходные: {{ mb_substr($resume->weekend_start_time, 0, 5)}} - {{mb_substr($resume->weekend_finish_time, 0, 5)}}</div>

                    </div>
                    <div class="questions_answears flex">
                      <div class="question">Город:</div>
                      <div class="answear">{{isset($resume->region) ? $resume->region->name : '' }}</div>

                    </div>
                    <div class="questions_answears flex">
                      <div class="question">Район:</div>
                      <div class="answear">{{isset($resume->region) ? $resume->region->name : '' }}</div>

                    </div>
                    <div class="questions_answears flex">
                      <div class="question">Сайт:</div>
                      <div class="answear"><a href="{{isset($resume->site) ? $resume->site : '' }}" class="answear">{{isset($resume->site) ? $resume->site : '' }}</a></div>
                    </div>
                    <div class="questions_answears flex">
                      <div class="question">Документ:</div>
                      <div class="answear">{{$resume->user->passport_status == 1 ? 'Подтверждено' : 'Не подтверждено'}}</div>
                    </div>
                    <div class="reviews_shows flex">
                      <div class="reviews flex"> Отзывы: <span> {{ $resume->feedbacks_count }}</span>
                      </div>
                      <div class="reviews flex"> Просмотр:<span>{{ $resume->viewed }}</span>
                      </div>
                    </div>
                  </div>
                  @if ((Auth::check() && Auth::user()->id != $resume->user_id || !Auth::check()) && $ableToComment)
                    <div class="workers_fourth">
                      <button class = "btn btn_writeReviewMob center-button" data-toggle="modal" data-target="#review_modal" data-backdrop="false">
                          <i class = "icon-mob_arrow_near_button"></i>
                          Написать отзыв
                      </button>
                    </div>
                  @endif
                  <div class="workers_fourth">
                    <form action="https://wa.me/{{str_replace([' ', '(', ')', '-'], '', $resume->whatsapp)}}" target="_blank">
                      <button class="btn btn_btnSmallMob center-button">
                        <i class="icon-mob_arrow_near_button"></i>
                                Написать в WhatsApp
                      </button>
                    </form>
                  </div>
                  <div class="workers_fifth">
                    <div class="top_title"> Описание: </div>
                    <div class="info">{{ $resume->description }} </div>
                  </div>
                  @if ($dopUsluga != null)
                    <div class="workers_sixth">
                      <div class="title">Дополнительная специализация:</div>
                      <div class="position">
                        @foreach ($dopUsluga as $dop)
                            {{ $dop->sub_section->name }} /
                        @endforeach
                        <!-- Разработка сайта / Дизайн логотипов / 3D дизайн -->
                      </div>
                    </div>
                  @endif
                  @include('about.portfolio_lisence_mobile')
                  @include('about.reviews_mobile')
            </div>
      @endif
      </div>
      @include('about.recommendations_mobile')
    </div>
    @include('about._modal_review')
  </div>
    @push('scripts')
      <script src="{{ asset('js/my_portgolio_swiper.js') }}"></script>
  @endpush
@endsection
