<div class="workers_eighth">
    <div class="title">Отзывы</div>
    <div class="swiper-container mob-swiper_review">
        <div class="swiper-wrapper">
            @if ($feedbacks != null)
                @for ($i = 0; $i < count($feedbacks); $i++)
                    <div class="swiper-slide images-swiper-mob-reviews">
                        @php                    
                            $date = date_parse($feedbacks[$i]->created_at);
                            $dateFormatFeedback = $date['day'] . ' ' . $month[$date['month']-1] . ' ' . $date['year'];
                        @endphp
                        <div class="review_swiper">
                            <div class="name"> {{ $feedbacks[$i]->fio }} </div>
                            <div class="date">{{ $dateFormatFeedback }}</div>
                            <div class="review_text"> {{ $feedbacks[$i]->review }} </div>
                        </div>
                        @if (isset($feedbacks[$i+1]))
                            @php                    
                                $date = date_parse($feedbacks[$i+1]->created_at);
                                $dateFormatFeedback = $date['day'] . ' ' . $month[$date['month']-1] . ' ' . $date['year'];
                            @endphp
                            <div class="review_swiper">
                                <div class="name"> {{ $feedbacks[$i+1]->fio }} </div>
                                <div class="date">{{ $dateFormatFeedback }}</div>
                                <div class="review_text"> {{ $feedbacks[$i+1]->review }} </div>
                            </div>
                        @endif                        
                    </div>
                @endfor
            @endif            
        </div>
        <div class="arrows flex">
            <div class="left left_reviews">
                <span class="figure_left"></span>
            </div>
            <div class="right right_reviews">
                <span class="figure_right"></span>
            </div>
        </div>
    </div>
</div>