<div class="reviews_on_services_card">
    <div class="reviews_on_services_card__title">Отзывы</div>
    <div class="reviews_on_services_card__blocks flex">
        @if ($feedbacks != null)
            @foreach ($feedbacks as $feedback)
                @php                    
                    $date = date_parse($feedback->created_at);
                    $dateFormatFeedback = $date['day'] . ' ' . $month[$date['month']-1] . ' ' . $date['year'];
                @endphp
                <div class="review">
                    <div class="name">{{ $feedback->fio }}</div>
                    <div class="data">{{ $dateFormatFeedback }}</div>
                    <div class="text"> {{ $feedback->review }} </div>
                </div>
            @endforeach
        @endif        
    </div>
</div>