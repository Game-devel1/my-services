<div class="modal" id="review_modal" role="dialog" aria-labelledby="review_modal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role = 'document' id ="review-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header flex">
                <div class="title">отзыв</div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class = "icon-close_modal-ico close" aria-hidden="true"></i>
                </button>
                
            </div>
            <div>
                <div class="modal-body reviewModal">
                    <form action="{{ route('feedback') }}" method="post">
                        {!! csrf_field() !!}
                        <label for="review-name">ФИО:</label> 
                        <input class = 'bigInputType' type="text" id = 'review-name' name = 'fio' required>
                        <span class = "error_word" data-error="fio"></span>
                        <label for="review-text">Отзыв (макс. 256 символов):</label>
                        <textarea name="review" id="review-text" cols="30" rows="10"  maxlength="255" required></textarea>
                        <span class = "error_word" data-error="review"></span>
                        <input type="hidden" name="resume_id" value="{{$resume->id}}">
                        <input type="hidden" name="user_id" value="{{$resume->user_id}}">
                        <button type="submit" class = 'btn btn_btnEnterModal flex'>Отправить</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>