<div class="workers_seventh">
    @if (!$portfolios->isEmpty())
        <div class="title">Портфолио:</div>
        <div class="swiper-container mob-swiper_partfolio">
            <div class="swiper-wrapper">
                @foreach ($portfolios as $portfolio)
                    <div class="swiper-slide images-swiper-mob">
                        <a href="{{ asset('storage/portfolio/').'/'. $resume->user_id.'/'.$portfolio->img }}" data-fancybox="gallery-11"><img style="width: 100%;" src="{{ asset('storage/portfolio/').'/'.$resume->user_id.'/'.$portfolio->img }}" /></a>
                    </div>
                @endforeach
            </div>
            <div class="arrows flex">
                <div class="left left_partfolio">
                    <span class="figure_left"></span>
                </div>
                <div class="right right_partfolio">
                    <span class="figure_right"></span>
                </div>
            </div>
        </div>
    @endif
</div>
<div class="workers_seventh">
    @if (!$licenses->isEmpty())
        <div class="title">Лицензии:</div>
        <div class="swiper-container mob-swiper_license">
            <div class="swiper-wrapper">
                @foreach ($licenses as $license)
                    <div class="swiper-slide images-swiper-mob-license">
                        <a href="{{ asset('storage/portfolio/').'/'. $resume->user_id.'/'.$license->img }}" data-fancybox="gallery-12"><img style="width: 100%;" src="{{ asset('storage/portfolio/').'/'. $resume->user_id.'/'.$license->img }}" /></a>
                    </div>
                @endforeach
            </div>
            <div class="arrows flex">
                <div class="left left_license">
                    <span class="figure_left"></span>
                </div>
                <div class="right right_license">
                    <span class="figure_right"></span>
                </div>
            </div>
        </div>
    @endif
</div>

