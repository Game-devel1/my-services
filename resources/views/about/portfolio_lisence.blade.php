<div class="portfolio_license flex">
    @if (!$portfolios->isEmpty())
        <div class="portfolio_license__block">
            <div class="title_arrows flex">
                <div class="title">Портфолио:</div>
                <div class="arrow flex">
                    <div class="left" id="{{ $portfolios->isEmpty() ? 'more_left_partfolio'.$portfolioCount : 'left_partfolio'.$portfolioCount }}">
                        <span class="figure_left"></span>
                    </div>
                    <div class="right" id="{{ $portfolios->isEmpty() ? 'more_right_partfolio'.$portfolioCount : 'right_partfolio'.$portfolioCount }}">
                        <span class="figure_right"></span>
                    </div>
                </div>
            </div>
            <div class="images swiper-container {{ !$portfolios->isEmpty() ? 'flex' : '' }}" id="{{ $portfolios->isEmpty() ? 'more_swiper_portfolio'.$portfolioCount : 'swiper_portfolio'.$portfolioCount }}">
                <div class="swiper-wrapper">
                    @foreach ($portfolios as $portfolio)
                        <div class="swiper-slide">
                            <a href="{{ asset('storage/portfolio/').'/'. $resume->user_id.'/'.$portfolio->img }}" data-fancybox="gallery-11"><img style="width: 100%;" src="{{ asset('storage/portfolio/').'/'.$resume->user_id.'/'.$portfolio->img }}" /></a>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        @php
            $portfolioCount++;
        @endphp
    @endif
    @if (!$licenses->isEmpty())
        <div class="portfolio_license__block">
                <div class="title_arrows flex">
                    <div class="title">Лицензии и сертификаты:</div>
                    <div class="arrow flex">
                        <div class="left" id="{{ $licenses->isEmpty() ? 'more_left_license'.$licenseCount : 'left_license'.$licenseCount }}">
                            <span class="figure_left"></span>
                        </div>
                        <div class="right" id="{{ $licenses->isEmpty() ? 'more_right_license'.$licenseCount : 'right_license'.$licenseCount }}">
                            <span class="figure_right"></span>
                        </div>
                    </div>
                </div>
                <div class="images swiper-container {{ !$licenses->isEmpty() ? 'flex' : '' }}" id="{{ $licenses->isEmpty() ? 'more_swiper_license'.$licenseCount : 'swiper_license'.$licenseCount }}">
                    <div class="swiper-wrapper">
                        @foreach ($licenses as $license)
                            <div class="swiper-slide">
                                <a href="{{ asset('storage/portfolio/').'/'. $resume->user_id.'/'.$license->img }}" data-fancybox="gallery-12"><img style="width: 100%;" src="{{ asset('storage/portfolio/').'/'. $resume->user_id.'/'.$license->img }}" /></a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            @php
                $licenseCount++;
            @endphp
    @endif

</div>