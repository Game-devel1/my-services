@extends('admin/layouts.main')
@section('pageTitle', 'Пакеты')
@section('content')
@if(Session::has('message'))
<p class="alert alert-info">{{ Session::get('message') }}</p>
@endif
<form enctype="multipart/form-data" action="{{ Route('packets.update', $packet) }}" method="post">
  {{ method_field('PUT') }}
  @csrf
  @include('admin/packets._form')
</form>
@endsection