
  <div class="form-group row">
    <label for="name" class="col-md-4 col-form-label text-md-left">Название</label>
    <div class="col-md-12">
      <input id="name" type="name" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') ?? $packet->name }}" autocomplete="name" autofocus>
      @error('name')
        <span class="invalid-feedback" role="alert">
          <strong>{{ $message }}</strong>
        </span>
      @enderror
    </div>       
  </div>

  <div class="form-group row">
    <label for="description" class="col-md-4 col-form-label text-md-left">Описание</label>
    <div class="col-md-12">
      <textarea class="form-control @error('description') is-invalid @enderror" name="description" id="description" cols="30" rows="10">{{ old('cost') ?? $packet->cost }}</textarea>      
      @error('description')
        <span class="invalid-feedback" role="alert">
          <strong>{{ $message }}</strong>
        </span>
      @enderror
    </div> 
  </div>
  
  <div class="form-group row">
    <label for="term" class="col-md-4 col-form-label text-md-left">Срок (кол-во месяца)</label>
    <div class="col-md-12">
      <input id="term" type="term" class="form-control @error('term') is-invalid @enderror" name="term" value="{{ old('term') ?? $packet->term }}" autocomplete="term" autofocus>
      @error('term')
        <span class="invalid-feedback" role="alert">
          <strong>{{ $message }}</strong>
        </span>
      @enderror
    </div>
  </div>

  <div class="form-group row">
    <label for="cost" class="col-md-4 col-form-label text-md-left">Цена</label>
    <div class="col-md-12">
      <input id="cost" type="text" class="form-control @error('cost') is-invalid @enderror" name="cost" value="{{ old('cost') ?? $packet->cost }}" autocomplete="cost" autofocus>
      @error('cost')
        <span class="invalid-feedback" role="alert">
          <strong>{{ $message }}</strong>
        </span>
      @enderror
    </div> 
  </div>
  
  
  <div class="form-group text-right">        
    <a class="btn btn-danger" href="{{ Route('packets.index') }}">Отменить</a>
    <!-- <input type="submit" name="submit" class="btn btn-success" value="Сохранить и добавить еще"> -->
    <input type="submit" name="submit" class="btn btn-success" value="Сохранить">    
  </div>
<!-- <script src="{{ asset('js/tinymce/tinymce.min.js') }}"></script>
<script src="{{ asset('js/tinymce.init.js') }}"></script> -->