@extends('admin/layouts.main')
@section('pageTitle', 'Пакеты')
@section('content')
    <p>
         <a class="btn btn-success" href="{{ Route('packets.create') }}">Создать</a>
    </p>
    @if(Session::has('message'))
        <p class="alert alert-info">{{ Session::get('message') }}</p>
    @endif
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Пакеты</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                <tr>
                    <th>Наименование</th>
                    <th>Срок</th>
                    <th>Стоимость</th>
                    <th>Описание</th>
                    <th>Редактировать</th>
                    <th>Удалить</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th>Наименование</th>
                    <th>Срок</th>
                    <th>Стоимость</th>
                    <th>Описание</th>   
                    <th>Редактировать</th>
                    <th>Удалить</th>
                </tr>
                </tfoot>
                <tbody>
                @foreach ($packets as $packet)
                    <tr>
                        <td style="width: 30%;"> {{ $packet->name }} </td>
                        <td style="width: 10%;"> {{ $packet->term }} </td>
                        <td style="width: 20%;"> {{ $packet->cost }} </td>
                        <td style="width: 30%;"> {{ $packet->description }} </td>
                        <td style="width: 5%;" class="justify-content-center">
                            <a href="{{ Route ('packets.edit', ['packet' => $packet->id]) }}"> <span class="glyphicon glyphicon-pencil"></span> Редактировать </a>
                        </td>
                        <td style="width: 5%;" class="justify-content-center">
                            <a href="javascript:void(0);" onclick="$(this).find('form').submit();" >
                                <form action="{{ Route ('packets.destroy', ['packet' => $packet->id]) }}" method="post">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                </form>
                                Удалить
                            </a>
                        </td>
                    </tr>
                @endforeach            
                </tbody>
            </table>
            </div>
        </div>
    </div>
@endsection
