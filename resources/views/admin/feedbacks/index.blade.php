@extends('admin/layouts.main')
@section('pageTitle', 'Отзывы')
@section('content')
    @if(Session::has('message'))
        <p class="alert alert-info">{{ Session::get('message') }}</p>
    @endif
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Отзывы</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Имя комментатора</th>
                        <th>Дата</th>
                        <th>Отзыв</th>
                        <th>ФИО услугодателя</th>
                        <th>Резюме</th>
                        <th>Статус</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Имя комментатора</th>
                        <th>Дата</th>
                        <th>Отзыв</th>
                        <th>ФИО услугодателя</th>
                        <th>Резюме</th>
                        <th>Статус</th>
                    </tr>
                </tfoot>
                <tbody>
                @foreach ($feedbacks as $feedback)
                    <tr>
                        <td style="width: 20% !important;"> {{ $feedback->fio }} </td>
                        <td style="width: 20% !important;"> {{ $feedback->created_at }} </td>
                        <td style="width: 30% !important;"> {{ $feedback->review }} </td>
                        <td style="width: 20% !important;"> {{ $feedback->user->first_name .' '. $feedback->user->last_name  .' '. $feedback->user->patronymic  }} </td>
                        <td style="width: 5% !important;" class="justify-content-center">
                            <a href="{{ Route ('about', $feedback->resume_id) }}"> Резюме </a>
                        </td>
                        <td style="width: 5%;" class="justify-content-center">
                            <a href="javascript:void(0);" class="changeStatus">
                                <form action="{{ Route ('feedbacks.update', ['feedback' => $feedback->id]) }}" method="post">
                                    {{ method_field('PUT') }}
                                    {{ csrf_field() }}
                                </form>
                                <span> {{ $feedback->isActive == 1 ? 'Активный' : 'Не активный' }} </span>
                            </a>
                        </td>
                    </tr>
                @endforeach            
                </tbody>
            </table>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script>
    $('body').on('click', '.changeStatus', function(e) {    
        e.preventDefault() 
        let text = $.trim($(this).find('span').text());
        var formData = $(this).find('form').serialize();        
        $.ajax({
            url: $(this).find('form').attr('action'),
            method: 'PUT',          
            data: formData,
            dataType: 'json',   
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }                        
        })     
        if (text === "Активный") {
            $(this).find('span').text('Не активный');
        }
        else {
            $(this).find('span').text('Активный');
        }        
    })
</script>
@endpush