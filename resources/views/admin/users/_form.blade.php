@extends('admin/layouts.main')
@section('pageTitle', 'Пользователи')
@section('content')
    @if(Session::has('message'))
        <p class="alert alert-info">{{ Session::get('message') }}</p>
    @endif
    <form enctype="multipart/form-data" action="{{ Route('users.store', $user) }}" method="post">
        {{ method_field('POST') }}
        @csrf
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="first_name" class="control-label">Имя</label>
                    <input type="text" name="first_name" id="first_name" class="form-control">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="last_name" class="control-label">Фамилия</label>
                    <input type="text" name="last_name" id="last_name" class="form-control">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="patronymic" class="control-label">Отчество</label>
                    <input type="text" name="patronymic" id="patronymic" class="form-control">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="email" class="control-label">Email</label>
                    <input type="email" name="email" id="email" class="form-control">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="password" class="control-label">Пароль</label>
                    <input type="password" name="password" id="password" class="form-control">
                </div>
            </div>

            <div class="col-lg-12">
                <div class="form-group">
                    <input type="checkbox" name="is_admin" id="is_admin">
                    <label for="is_admin" class="control-label">Администратор</label>
                </div>
            </div>

            <div class="col-lg-6">

                <div class="form-group text-right">
                    <input type="submit" name="submit" class="btn btn-success" value="Сохранить">
                </div>
            </div>

            <div class="col-lg-12">

            </div>
        </div>
    </form>

    <style>
        .switch {
            display: inline-block;
            height: 34px;
            position: relative;
            width: 60px;
        }

        .switch input {
            display:none;
        }

        .slider {
            background-color: #ccc;
            bottom: 0;
            cursor: pointer;
            left: 0;
            position: absolute;
            right: 0;
            top: 0;
            transition: .4s;
        }

        .slider:before {
            background-color: #fff;
            bottom: 4px;
            content: "";
            height: 26px;
            left: 4px;
            position: absolute;
            transition: .4s;
            width: 26px;
        }

        input:checked + .slider {
            background-color: #66bb6a;
        }

        input:checked + .slider:before {
            transform: translateX(26px);
        }

        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
        }

    </style>

@endsection
