@extends('admin/layouts.main')
@section('pageTitle', 'Пользователи')
@section('content')

    @if(Session::has('message'))
        <p class="alert alert-info">{{ Session::get('message') }}</p>
    @endif
    <p>
        <a class="btn btn-success" href="{{ Route('users.create') }}">Создать</a>
    </p>
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Пользователи</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                <tr>
                    <th>ФИО</th>
                    <th>Почта</th>
                    <th>Статус документа</th>
                    <th>Статус</th>
                    <th>Редактировать</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th>ФИО</th>
                    <th>Почта</th>
                    <th>Статус документа</th>
                    <th>Статус</th>
                    <th>Редактировать</th>
                </tr>
                </tfoot>
                <tbody>
                @foreach ($users as $user)
                    <tr>
                        <td style="width: 40%;">{{ $user->first_name .' '. $user->last_name .' '. $user->patronymic }}</td>
                        <td style="width: 20%;"> {{ $user->email }} </td>
                        <td style="width: 20%;"> {{$user->passport_status == 1 ? 'Подтверждено' : 'Не подтверждено'}} </td>
                        <td style="width: 15%;"> @php if ($user->hasRole('vip')) { echo 'Vip'; } else if ($user->hasRole('profi')) { echo 'Profi'; } else { echo 'None'; } @endphp </td>
                        <td style="width: 5%;" class="justify-content-center">
                            <a href="{{ Route ('user.edit', ['section' => $user->id]) }}"> <span class="glyphicon glyphicon-pencil"></span> Редактировать </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            </div>
        </div>
    </div>
@endsection
