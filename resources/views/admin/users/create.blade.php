@extends('admin/layouts.main')
@section('pageTitle', 'Разделы')
@section('content')
    @if(Session::has('message'))
        <p class="alert alert-info">{{ Session::get('message') }}</p>
    @endif
    @csrf
    @include('admin/users._form')
@endsection
