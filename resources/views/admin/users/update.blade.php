@extends('admin/layouts.main')
@section('pageTitle', 'Пользователи')
@section('content')
@if(Session::has('message'))
<p class="alert alert-info">{{ Session::get('message') }}</p>
@endif
<form enctype="multipart/form-data" action="{{ Route('user.update', $user) }}" method="post">
  {{ method_field('PUT') }}
  @csrf
  <div class="row">
    <div class="col-lg-6">
      <div class="form-group">
        <label for="first_name" class="control-label">Имя</label>
        <input type="text" name="first_name" id="first_name" value="{{ old('first_name') ?? $user->first_name }}" class="form-control" disabled>
      </div>
    </div>
    <div class="col-lg-6">
      <div class="form-group">
        <label for="last_name" class="control-label">Фамилия</label>
        <input type="text" name="last_name" id="last_name" value="{{ old('last_name') ?? $user->last_name }}" class="form-control" disabled>
      </div>
    </div>
    <div class="col-lg-6">
      <div class="form-group">
        <label for="patronymic" class="control-label">Отчество</label>
        <input type="text" name="patronymic" id="patronymic" value="{{ old('patronymic') ?? $user->patronymic }}" class="form-control" disabled>
      </div>
    </div>
    <div class="col-lg-6">
      <div class="form-group">
        <label for="passport_data" class="control-label">Документые данные</label>
        <input type="text" name="passport_data" id="passport_data" value="{{ old('passport_data') ?? $user->passport_data }}" class="form-control" disabled>
      </div>
    </div>

    <div class="col-lg-6">
      <img class="img-fluid" src="{{asset('storage/')}}/{{$user->passport_img}}" alt="">
    </div>

    <div class="col-lg-6">
      <h3>Подтверждение почты: <small> {{$user->email_verified_at == null ? 'Нет' : $user->email_verified_at}} </small> </h3>
      <h3>Дата регистрации: <small> {{ $user->created_at }} </small> </h3>
      <div class="form-group" style="display: flex">
        <label style="font-size: 1.75rem;">Статус документа:</label>
        <div style="margin-left: 0.7em; margin-top: 0.7em;">
          <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" class="custom-control-input" id="accept" name="passport_status" value = "1" {{ $user->passport_status != 2 ? 'checked' : '' }}>
            <label class="custom-control-label" for="accept">Подтверждено</label>
          </div>

          <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" class="custom-control-input" id="notAccept" name="passport_status" value = "0" {{ $user->passport_status == 0 ? 'checked' : '' }} >
            <label class="custom-control-label" for="notAccept">Не подтверждено</label>
          </div>
        </div>
      </div>
      <!-- <label class="switch" for="checkbox">
        <input type="checkbox" name="password_status" value="{{ old('passport_status') ?? $user->passport_status }}" id="checkbox" />
        <div class="slider round"></div>
      </label>            -->

      <div id="radioCheck">
        <div class="custom-control custom-radio custom-control-inline">
          <input type="radio" class="custom-control-input" id="defaultInline1" name="isBan" value = "1" {{ $user->status != 2 ? 'checked' : '' }}>
          <label class="custom-control-label" for="defaultInline1">Разбанить</label>
        </div>

        <div class="custom-control custom-radio custom-control-inline">
          <input type="radio" class="custom-control-input" id="defaultInline2" name="isBan" value = "2" {{ $user->status == 2 ? 'checked' : '' }} >
          <label class="custom-control-label" for="defaultInline2">Забанить</label>
        </div>
      </div>

      <div id="comment_container" style="display: none;" class="form-group">
        <label for="comment" class="control-label">Причина бана</label>
        <textarea name="comment" id="comment" cols="30" value="{{ old('comment') }}" placeholder="Вы забанины администратором за ..." class="form-control" rows="10"></textarea>
      </div>

        <br>

        <div class="col-lg-12">
            <div class="form-group">
                <input type="checkbox" name="is_admin" id="is_admin" {{$isAdmin ? 'checked="checked"' : ''}}>
                <label for="is_admin" class="control-label">Администратор</label>
            </div>
        </div>

      <div class="form-group text-right">
        <input type="submit" name="submit" class="btn btn-success" value="Сохранить">
      </div>
    </div>

    <div class="col-lg-12">

    </div>
  </div>
</form>

<style>
.switch {
  display: inline-block;
  height: 34px;
  position: relative;
  width: 60px;
}

.switch input {
  display:none;
}

.slider {
  background-color: #ccc;
  bottom: 0;
  cursor: pointer;
  left: 0;
  position: absolute;
  right: 0;
  top: 0;
  transition: .4s;
}

.slider:before {
  background-color: #fff;
  bottom: 4px;
  content: "";
  height: 26px;
  left: 4px;
  position: absolute;
  transition: .4s;
  width: 26px;
}

input:checked + .slider {
  background-color: #66bb6a;
}

input:checked + .slider:before {
  transform: translateX(26px);
}

.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}

</style>

@endsection
