@extends('admin/layouts.main')
@section('pageTitle', 'Портфолио')
@section('content')
    @if(Session::has('message'))
        <p class="alert alert-info">{{ Session::get('message') }}</p>
    @endif
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Портфолио</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>                        
                        <th>ФИО услугодателя</th>
                        <th>Резюме</th>
                        <th>Изображения</th>
                        <th>Статус</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>                        
                        <th>ФИО услугодателя</th>
                        <th>Резюме</th>
                        <th>Изображения</th>
                        <th>Статус</th>
                    </tr>
                </tfoot>
                <tbody>
                @foreach ($portfolios as $portfolio)
                    <tr>                        
                        <td style="width: 20%;"> {{ $portfolio->user->first_name .' '. $portfolio->user->last_name  .' '. $portfolio->user->patronymic  }} </td>
                        <td style="width: 10%;" class="justify-content-center">
                            <a target="_blank" href="{{ Route ('about', $portfolio->resume_id) }}"> Резюме </a>
                        </td>
                        <td style="width: 40%;"> <img width="200px" height="200px" class="img-fluid rounded " src="{{ asset('storage/') }}/portfolio/{{$portfolio->user_id}}/{{ $portfolio->img }} "  alt=""></td>
                        <td style="width: 10%;" class="justify-content-center">
                            <a href="javascript:void(0);" class="changeStatus" >
                                <form action="{{ Route ('portfolios.update', ['portfolio' => $portfolio->id]) }}" method="post">
                                    {{ method_field('PUT') }}
                                    {{ csrf_field() }}
                                </form>
                                <span> {{ $portfolio->isActive == 1 ? 'Активный' : 'Не активный' }} </span>
                            </a>
                        </td>
                    </tr>
                @endforeach            
                </tbody>
            </table>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script>
    $('body').on('click', '.changeStatus', function(e) {    
        e.preventDefault() 
        let text = $.trim($(this).find('span').text());
        var formData = $(this).find('form').serialize();        
        $.ajax({
            url: $(this).find('form').attr('action'),
            method: 'PUT',          
            data: formData,
            dataType: 'json',   
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }                        
        })     
        if (text === "Активный") {
            $(this).find('span').text('Не активный');
        }
        else {
            $(this).find('span').text('Активный');
        }        
    })
</script>
@endpush