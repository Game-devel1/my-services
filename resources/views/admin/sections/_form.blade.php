  <div class="form-group row">
    <label for="name" class="col-md-4 col-form-label text-md-left">Название</label>

    <div class="col-md-12">
      <input id="name" type="name" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') ?? $section->name }}" autocomplete="name" autofocus>
      @error('name')
        <span class="invalid-feedback" role="alert">
          <strong>{{ $message }}</strong>
        </span>
      @enderror
    </div>
  </div>

  <div class="form-group row">
    <label for="image" class="col-md-4 col-form-label text-md-left">Картинка</label>

    <div class="col-md-12">
      <input id="image" type="file" class="@error('image') is-invalid @enderror" name="image" value="{{ old('image') }}" autocomplete="image" autofocus>        
      @error('image')
        <span style="display: block;" class="invalid-feedback" role="alert">
          <strong>{{ $message }}</strong>
        </span>
      @enderror
    </div>
  </div>
  <div class="form-group text-right">        
    <a class="btn btn-danger" href="{{ Route('sections.index') }}">Отменить</a>
    <input type="submit" name="submit" class="btn btn-success" value="Сохранить и добавить еще">
    <input type="submit" name="submit" class="btn btn-success" value="Сохранить">    
  </div>
