@extends('admin/layouts.main')
@section('pageTitle', 'Разделы')
@section('content')
@if(Session::has('message'))
<p class="alert alert-info">{{ Session::get('message') }}</p>
@endif
<form enctype="multipart/form-data" action="{{ Route('sections.store') }}" method="post">
  @csrf
  @include('admin/sections._form')
</form>

@endsection