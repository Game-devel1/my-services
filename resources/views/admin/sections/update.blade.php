@extends('admin/layouts.main')
@section('pageTitle', 'Разделы')
@section('content')
@if(Session::has('message'))
<p class="alert alert-info">{{ Session::get('message') }}</p>
@endif
<form enctype="multipart/form-data" action="{{ Route('sections.update', $section) }}" method="post">
  {{ method_field('PUT') }}
  @csrf
  @include('admin/sections._form')
</form>
@endsection