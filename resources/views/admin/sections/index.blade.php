@extends('admin/layouts.main')
@section('pageTitle', 'Разделы')
@section('content')
    <p>
        <a class="btn btn-success" href="{{ Route('sections.create') }}">Создать</a>
    </p>
    @if(Session::has('message'))
        <p class="alert alert-info">{{ Session::get('message') }}</p>
    @endif
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Разделы</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                <tr>
                    <th>Наименование</th>
                    <th>Изображение</th>
                    <th>Редактировать</th>
                    <th>Удалить</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th>Наименование</th>
                    <th>Изображение</th>    
                    <th>Редактировать</th>
                    <th>Удалить</th>
                </tr>
                </tfoot>
                <tbody>
                @foreach ($sections as $section)
                    <tr>
                        <td style="width: 50%;">{{ $section->name }}</td>
                        <td style="width: 40%;"> <img width="200px" height="200px" class="img-fluid rounded " src="{{ asset('storage/') }}/{{ $section->img }} "  alt=""></td>
                        <td style="width: 5%;" class="justify-content-center">
                            <a href="{{ Route ('sections.edit', ['section' => $section->id]) }}"> <span class="glyphicon glyphicon-pencil"></span> Редактировать </a>                            
                        </td>
                        <td style="width: 5%;" class="justify-content-center">
                            <a href="javascript:void(0);" onclick="$(this).find('form').submit();" >
                                <form action="{{ Route ('sections.destroy', ['section' => $section->id]) }}" method="post">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                </form>
                                Удалить
                            </a>
                        </td>
                    </tr>
                @endforeach            
                </tbody>
            </table>
            </div>
        </div>
    </div>
@endsection
