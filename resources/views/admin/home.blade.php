@extends('admin/layouts.main')
@section('pageTitle', 'Административный панель')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div id="mainPage">
            <!-- <img src="/img/aboutCompanyLogo.png" alt=""> -->
            <p>Добро пожаловать в Административную панель «{{config('app.name')}}»</p>
        </div>
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Статистика</div>

                <div class="card-body">                    
                    <p> Пользователей кол-во: <span>{{$usersCount}}</span> </p>
                    <p>Из них подтвердили почту: <span>{{$usersCountWithEmail}}</span> </p>
                    <p>Кол-во созданных Резюме: <span>{{$resumeCount}}</span> </p>
                </div>
                <div class="card-header">По разделам</div>
                <div class="card-body">  
                    @foreach ($sections as $section)
                        <p> {{$section->name}}: <span>{{$section->resumeCount}}</span> </p>
                    @endforeach                                                          
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
