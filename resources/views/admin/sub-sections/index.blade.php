@extends('admin/layouts.main')
@section('pageTitle', 'Подразделы')
@section('content')
    <p>
        <a class="btn btn-success" href="{{ Route('sub-sections.create') }}">Создать</a>
    </p>
    @if(Session::has('message'))
        <p class="alert alert-info">{{ Session::get('message') }}</p>
    @endif
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Подразделы</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                <tr>
                    <th>Раздел</th>
                    <th>Наименование</th>
                    <th>Редактировать</th>
                    <th>Удалить</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th>Раздел</th>
                    <th>Наименование</th>   
                    <th>Редактировать</th>
                    <th>Удалить</th>
                </tr>
                </tfoot>
                <tbody>
                @foreach ($subSections as $section)
                    <tr>
                        <td style="width: 50%;">{{ $section->section->name }}</td>
                        <td style="width: 40%;"> {{ $section->name }} </td>
                        <td style="width: 5%;" class="justify-content-center">
                            <a href="{{ Route ('sub-sections.edit', ['section' => $section->id]) }}"> <span class="glyphicon glyphicon-pencil"></span> Редактировать </a>                            
                        </td>
                        <td style="width: 5%;" class="justify-content-center">
                            <a href="javascript:void(0);" onclick="$(this).find('form').submit();" >
                                <form action="{{ Route ('sub-sections.destroy', ['sub_section' => $section->id]) }}" method="post">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                </form>
                                Удалить
                            </a>
                        </td>
                    </tr>
                @endforeach            
                </tbody>
            </table>
            </div>
        </div>
    </div>
@endsection
