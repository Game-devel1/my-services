  <div class="form-group row">
    <label for="name" class="col-md-4 col-form-label text-md-left">Название</label>
    <div class="col-md-12">
      <div class="form-group">
        <select name="section_id" class="form-control @error('section_id') is-invalid @enderror" require>                    
          @foreach ($sections as $section)
            @if ($subSection->section_id != null && $section->id == $subSection->section_id)
              <option selected value="{{$section->id}}">{{$section->name}}</option>
            @else
              <option value="{{$section->id}}">{{$section->name}}</option>
            @endif
          @endforeach
        </select>
        @error('section_id')
          <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
          </span>
        @enderror
      </div>
    </div>
    <div class="col-md-12">
      <input id="name" type="name" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') ?? $subSection->name }}" autocomplete="name" autofocus>
      @error('name')
        <span class="invalid-feedback" role="alert">
          <strong>{{ $message }}</strong>
        </span>
      @enderror
    </div>
  </div>
  
  <div class="form-group text-right">        
    <a class="btn btn-danger" href="{{ Route('sub-sections.index') }}">Отменить</a>
    <input type="submit" name="submit" class="btn btn-success" value="Сохранить и добавить еще">
    <input type="submit" name="submit" class="btn btn-success" value="Сохранить">    
  </div>
