@extends('admin/layouts.main')
@section('pageTitle', 'Подразделы')
@section('content')
@if(Session::has('message'))
<p class="alert alert-info">{{ Session::get('message') }}</p>
@endif
<form enctype="multipart/form-data" action="{{ Route('sub-sections.update', $subSection) }}" method="post">
  {{ method_field('PUT') }}
  @csrf
  @include('admin/sub-sections._form')
</form>
@endsection