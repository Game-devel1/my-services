
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="utf-8">
    <title>@yield('pageTitle')</title>
    <!-- Custom fonts for this template-->        
    <link rel="stylesheet" href="{{ asset('admin_resource/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin_resource/css/fontsNunito.css') }}">
    <link href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" rel="stylesheet" />
    <!-- Custom styles for this template-->
    <link href="{{ asset('admin_resource/css/sb-admin-2.min.css') }}" rel="stylesheet">
    <!-- MDBootstrap Datatables  -->
    <link href="{{ asset('admin_resource/vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.7.6/css/mdb.min.css" /> -->
</head>
<body id="page-top">
<style>
html {
    font-size: 1em !important;
}
</style>

    <!-- Page Wrapper -->
    <div id="wrapper">
      @if (Auth::check())
        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="/admin">
              <!-- <div class="sidebar-brand-icon rotate-n-15">
                <i class="fas fa-laugh-wink"></i>
              </div> -->
                <div class="sidebar-brand-text mx-3">Админ панель</div>
            </a>
                                   
            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block">
        
            <li class="nav-item {{ Request::is('admin/sections') ? 'active' : '' }}">
                <a class="nav-link" href="{{ Route('sections.index') }}">                
                <span>Разделы</span></a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block">

            <li class="nav-item {{ Request::is('admin/sub-sections') ? 'active' : '' }}">
                <a class="nav-link" href="{{ Route('sub-sections.index') }}">                
                <span>Подразделы</span></a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block">

            <li class="nav-item {{ Request::is('admin/regions') ? 'active' : '' }}">
                <a class="nav-link" href="{{ Route('regions.index') }}">                
                <span>Регионы</span></a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block">

            <li class="nav-item {{ Request::is('admin/packets') ? 'active' : '' }}">
                <a class="nav-link" href="{{ Route('packets.index') }}">                
                <span>Пакеты</span></a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block">

            <li class="nav-item {{ Request::is('admin/users') ? 'active' : '' }}">
                <a class="nav-link" href="{{ Route('users.index') }}">                
                <span>Пользователи</span></a>
            </li>

            <hr class="sidebar-divider d-none d-md-block">

            <li class="nav-item {{ Request::is('admin/licenses') ? 'active' : '' }}">
                <a class="nav-link" href="{{ Route('licenses.index') }}">                
                <span>Лицензии и сертификаты</span></a>
            </li>

            <hr class="sidebar-divider d-none d-md-block">

            <li class="nav-item {{ Request::is('admin/portfolios') ? 'active' : '' }}">
                <a class="nav-link" href="{{ Route('portfolios.index') }}">                
                <span>Портфолио</span></a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block">

            <li class="nav-item {{ Request::is('admin/feedbacks') ? 'active' : '' }}">
                <a class="nav-link" href="{{ Route('feedbacks.index') }}">                
                <span>Отзывы</span></a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block">

            <li class="nav-item {{ Request::is('admin/ads-banner') ? 'active' : '' }}">
                <a class="nav-link" href="{{ Route('ads-banner.index') }}">                
                <span>Рекламные баннеры</span></a>
            </li>

              <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block">

            <!-- Sidebar Toggler (Sidebar) -->
            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>

        </ul>
        <!-- End of Sidebar -->
      @endif

         <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
        @if (Auth::check())
        <!-- Topbar -->        
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>
         
          <!-- Topbar Search -->
          <!-- <form class="d-none d-sm-inline-block mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
            <div class="input-group">
              <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
              <div class="input-group-append">
                <button class="btn btn-primary" type="button">
                  <i class="fas fa-search fa-sm"></i>
                </button>
              </div>
            </div>
          </form> -->

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">                                              
            
            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->           
                <li class="nav-item dropdown no-arrow">
                    <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <span class="mr-2 d-none d-lg-inline text-gray-600 small">{{ Auth::user()->first_name }}</span>
                      <img class="img-profile rounded-circle" src="https://previews.123rf.com/images/salamatik/salamatik1801/salamatik180100019/92979836-profile-anonymous-face-icon-gray-silhouette-person-male-default-avatar-photo-placeholder-isolated-on.jpg">
                    </a>
                    <!-- Dropdown - User Information -->
                    <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                      <!-- <a class="dropdown-item" href="#">
                        <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                        Profile
                      </a>
                      <a class="dropdown-item" href="#">
                        <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                        Settings
                      </a>
                      <a class="dropdown-item" href="#">
                        <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                        Activity Log
                      </a> -->
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="{{ route('logout') }}" data-toggle="modal" data-target="#logoutModal"  onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();" >
                        <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                        {{ __('Выйти') }}
                      </a>
                      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                          @csrf
                      </form>
                    </div>   
                </li>
          </ul>

        </nav>
        <!-- End of Topbar -->
        @endif
      <!-- Main Content -->
      <div id="content">                                        
        @yield('content')            
      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; MAINT <?=date('Y')?></span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Bootstrap core JavaScript-->
  <script src="{{ asset('admin_resource/vendor/jquery/jquery.min.js') }}"></script>
  <script src="{{ asset('admin_resource/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

  <!-- Core plugin JavaScript-->
  <script src="{{ asset('admin_resource/vendor/jquery-easing/jquery.easing.min.js') }}"></script>

  <!-- Custom scripts for all pages-->
  <script src="{{ asset('admin_resource/js/sb-admin-2.min.js') }}"></script>

  <script src="{{ asset('admin_resource/vendor/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('admin_resource/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('admin_resource/js/demo/datatables-demo.js') }}"></script>
  @stack('scripts')
<script>
  $('#radioCheck').change(function() {
    var type = $(this).find("input[type = radio]:checked").val()

    if (type == 2) {
      $('#comment_container').css('display', 'block')
    }
    else {
      $('#comment_container').css('display', 'none')
    }
  })
</script>
</body>
</html>
