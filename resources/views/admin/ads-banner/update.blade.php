@extends('admin/layouts.main')
@section('pageTitle', 'Рекламные баннеры')
@section('content')
@if(Session::has('message'))
<p class="alert alert-info">{{ Session::get('message') }}</p>
@endif
<form enctype="multipart/form-data" action="{{ Route('ads-banner.update', $ads) }}" method="post">
  {{ method_field('PUT') }}
  @csrf
  @include('admin/ads-banner._form')
</form>
@endsection