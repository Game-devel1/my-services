@extends('admin/layouts.main')
@section('pageTitle', 'Рекламные баннеры')
@section('content')
@if(Session::has('message'))
<p class="alert alert-info">{{ Session::get('message') }}</p>
@endif
<form enctype="multipart/form-data" action="{{ Route('regions.store') }}" method="post">
  @csrf
  @include('admin/regions._form')
</form>

@endsection