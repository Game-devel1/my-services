  <div class="form-group row">
    <label class="col-md-4 col-form-label text-md-left">{{ $ads->page_name }}</label>    
  </div>
  <div class="form-group row">
    <label class="col-md-4 col-form-label text-md-left">{{ $ads->name }}</label>    
  </div>
  <div class="form-group row">
    <label for="img" class="col-md-4 col-form-label text-md-left">Картинка</label>

    <div class="col-md-12">
      <input id="img" type="file" class="@error('img') is-invalid @enderror" name="img" value="{{ old('img') }}" autocomplete="img" autofocus>        
      @error('img')
        <span style="display: block;" class="invalid-feedback" role="alert">
          <strong>{{ $message }}</strong>
        </span>
      @enderror
    </div>
  </div>

  <div class="form-group row">
    <label for="url" class="col-md-4 col-form-label text-md-left">Ссылка</label>

    <div class="col-md-12">
      <input id="url" type="url" class="form-control @error('url') is-invalid @enderror" name="url" value="{{ old('url') ?? $ads->url }}" autocomplete="url" autofocus>
      @error('url')
        <span class="invalid-feedback" role="alert">
          <strong>{{ $message }}</strong>
        </span>
      @enderror
    </div>
  </div>
  
  <div class="form-group text-right">        
    <a class="btn btn-danger" href="{{ Route('ads-banner.index') }}">Отменить</a>    
    <input type="submit" name="submit" class="btn btn-success" value="Сохранить">    
  </div>
