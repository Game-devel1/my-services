@extends('admin/layouts.main')
@section('pageTitle', 'Рекламные баннеры')
@section('content')
    @if(Session::has('message'))
        <p class="alert alert-info">{{ Session::get('message') }}</p>
    @endif
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Рекламные баннеры</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                <tr>
                    <th>Страница</th>
                    <th>Блок</th>
                    <th>Файл</th>
                    <th>Ссылка</th>
                    <th>Редактировать</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th>Страница</th>   
                    <th>Блок</th>
                    <th>Файл</th>
                    <th>Ссылка</th>
                    <th>Редактировать</th>
                </tr>
                </tfoot>
                <tbody>
                @foreach ($ads as $ad)
                    <tr>
                        <td style="width: 20%;"> {{ $ad->page_name }} </td>
                        <td style="width: 20%;"> {{ $ad->name }} </td>
                        <td style="width: 40%;"> <img width="200px" height="200px" class="img-fluid rounded " src="{{ asset('storage/') }}/{{ $ad->img }} "></td>
                        <td style="width: 10%;" class="justify-content-center">
                            <a href="{{ $ad->url }}"> Ссылка </a>
                        </td>
                        <td style="width: 10%;" class="justify-content-center">
                            <a href="{{ Route ('ads-banner.edit', ['ads_banner' => $ad->id]) }}"> Редактировать </a>
                        </td>                   
                    </tr>
                @endforeach            
                </tbody>
            </table>
            </div>
        </div>
    </div>
@endsection
