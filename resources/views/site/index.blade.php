@extends('layouts.main')
@section('pageTitle','Главная')
@section('content')
@section('template','index')
@section('namespace','index')
    <div class="fon-wrapper">
        <div class="fon"></div>
    </div>
    <div class="top_specialistsAbsolute flex">
        <div class="top_specialistsAbsolute__star">
            <i class="icon-start_top"></i>
        </div>
        <div class="top_specialistsAbsolute__word">
            Top
        </div>
    </div>

    <div class="newListingsAbsolute">
        <div class="newListingsAbsolute__plus">
            <i class = "icon-newListingsPlus"></i>
        </div>
        <div class="newListingsAbsolute__word">
            <p>new</p>
        </div>
    </div>
    <div class="images_back">
        <div class="stamp">
            <img src="images/samples/stamp.png" />
        </div>
        <div class="leaf3">
            <img src="images/samples/leaf3.png" />
        </div>
        <div class="leaf4">
            <img src="images/samples/leaf4.png" />
        </div>
        <div class="leaf9">
            <img src="images/samples/leaf9.png" />
        </div>
        <div class="cup">
            <img src="images/samples/cup.png" />
        </div>
    </div>
    <div class="container-fluid">
        <!-- <div class="topTitle flex web"> -->
        <!-- <div class="topTitle__flex">
            <div class="blockMain">МОИ УСЛУГИ</div>
            <div class="blockSecond">–  услуги всего Казахстана</div>
        </div> -->
        <!-- </div> -->
        <!-- <div class="topTitle mob">
            <p class="topTitle__p"><span>МОИ УСЛУГИ</span> <br> услуги всего Казахстана</p>
        </div>  -->
        <div class="topTitle webTitle">
            <span>МОИ УСЛУГИ</span>
            <p class="topTitle__p">портал объединяющий заказчиков <br> и исполнителей услуг</p>
        </div>
        <div class="topTitle mob">
            <span>МОИ УСЛУГИ</span>
            <p class="topTitle__p">портал объединяющий заказчиков и исполнителей услуг</p>
        </div>
        @include('site.search')
        <div class="services">
            @if( !empty($sections))
                @foreach ($sections as $section)
                    <div class="services__clicked_block post_section" data-id="{{ $section->id }}">
                        <div class="left" style="background-image: url('storage/{{ $section->img }}')">
                            <div class="icon-block" style = "background-image: url(../images/svg-src/svg_internet.svg);"></div>
                        </div>
                        <div class="right">
                            <div class="sub-services">
                                <div class="title">{{ $section->name }}</div>
                                <div class="flex justify-content-between list_services">
                                    @if( !empty($subsections))
                                        <div class="block">
                                            @foreach ($subsections as $subsection)
                                                @if( $subsection->section_id == $section->id)
                                                    <ul>
                                                        <li>
                                                            <a href="/services/{{$subsection->id}}">{{ $subsection->name }}</a>
                                                        </li>
                                                    </ul>
                                                @endif
                                            @endforeach
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="close flex" id = 'close-servicesBlock'>
                                <i class='icon-arrow_near_button'></i>
                                <span>Закрыть {{ $section->name }}</span>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
            <div class="row flex" id = "services-inactive">
                @if( !empty($sections))
                    @foreach ($sections as $section)
                        <div class="outside-offset">
                            <a href="#" class="services__items show_subsection" data-id="{{ $section->id }}">
                                <img src="storage/{{ $section->img }}">
                                <div class="services_information flex">
                                    <p>{{ $section->name }}</p>
                                    <div class="arrow_and_link flex	">
                                        <i class="icon-arrow_near_button"></i>
                                        <span class="onclick-checked">Подробнее</span>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    @include('site.topUsers')
    </div>
    <div class="container-fluid">
        @if ($ads_bottom != null)
            <div class="horizontal-advertizing" style="background-image: url({{ asset('storage/').'/'.$ads_bottom->img }});"></div>
        @endif
        @if( !empty($plainUsersCards))
            <div class="new_listings">
                <div class="new_listings__title">
                    <span> Новые объявления </span>
                </div>
            </div>
            <div id="post-plain-users">
                @include('site.plainUsers')
            </div>
            <div class="download">
                <a href="#" id="load-more" data-limit="6" data-max="{{ $plainUsersCount }}">Загрузить еще</a>
            </div>
        @endif
    </div>
@endsection
