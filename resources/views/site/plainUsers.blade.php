@foreach ($plainUsersCards as $card)
    <div class="listings_blocks flex">
        <div class="listings_blocks__left">
            <div class="avatar" style="background-image: url({{ $card->Big_img != null ? asset('storage/portfolio/').'/'.$card->user_id.'/'.$card->Big_img : '' }})"></div>
        </div>
        <div class="listings_blocks__rigth">
            <div class="flex justify-content-between">
                <div class="emlpoyee_info">
                    <p class="employee_position">{{$card->section->name}}</p>
                    <p class="employee_name">{{ $card->user->last_name }} {{ $card->user->first_name }} {{ $card->user->patronymic }} <span>{{$card->sub_section->name}}</span></p>
                </div>
                <div class="listings_blocks_buttons">
                    <!-- <button onclick="location.href='https://api.whatsapp.com/send?phone={{ $card->whatsapp }}'" class="btn btn_writeWhatssap">Написать в WhatsApp</button> -->
                    <form action="https://wa.me/{{str_replace([' ', '(', ')', '-'], '', $card->whatsapp)}}" target="_blank" style="margin-bottom:15px;">
                      <button class="btn btn_writeWhatssap">
                                Написать в WhatsApp
                      </button>
                    </form>
                    <button onclick="location.href='/about/{{ $card->id }}'" class="btn btn_details">Подробнее</button>
                </div>
            </div>
            <div class="reviews_right">
                <div class="review_flexed flex ">
                    <i class="icon-review_svg"></i>
                    <p>отзывы: {{ $card->feedbacks_count }}</p>
                </div>
            </div>
        </div>
    </div>
@endforeach
<div class="mob_listings">
    @foreach ($plainUsersCards as $card)
        <div class="listining_block_mob">
            <div class="left flex">
                <div class="avatar-image-mob">
                    <img src="{{$card->Big_img != null ? asset('storage/portfolio/').'/'.$card->user_id.'/'.$card->Big_img : ''}}" />
                </div>
                <div class="left-second">
                    <div class="top-title"> {{$card->section->name}} </div>
                    <div class="name">{{ $card->user->last_name }} {{ $card->user->first_name }} {{ $card->user->patronymic }}</div>
                    <div class="info">{{$card->sub_section->name}}</div>
                </div>
            </div>
            <div class="right flex">
                <div class="reviews flex">
                    <i class="icon-review_svg"></i>
                    <p>отзывы: {{ $card->feedbacks_count }}</p>
                </div>
                <div class="buttons flex">
                    <button onclick="location.href='/about/{{ $card->id }}'" class="btn btn_btnSmallMob flex">
                        <i class="icon-mob_arrow_near_button"></i> Подробнее </button>
                    <button onclick="location.href='https://wa.me/{{str_replace([' ', '(', ')', '-'], '', $card->whatsapp)}}'" class="btn btn_btnSmallMob flex">
                        <i class="icon-mob_arrow_near_button"></i> WhatsApp </button>
                </div>
            </div>
        </div>
    @endforeach
</div>
