@if( !empty($topUserCards))
    <div class="top-title2 flex top-title2__line">
        <span> Tоp специалистов </span>
    </div>
    <div class="flex">
        <div class="top-specialists web">
            @foreach ($topUserCards as $card)
                <div class="top_set">
                    <div class="top-specialists__information {{$card->user->hasRole('vip') ? 'vip_border' : 'profi_border'}}">
                        <div class="first {{$card->user->hasRole('vip') ? 'vip' : 'profi'}}">
                            @if ($card->user->hasRole('vip'))
                                <div class="first__top-title flex">
                                    <p class="upper">Vip</p>
                                    <div class="diamonds"></div>
                                </div>
                            @elseif ($card->user->hasRole('profi'))
                                <div class="first__top-title flex">
                                    <p class="upper">Profi</p>
                                    <div class="brief"></div>
                                </div>
                            @endif
                            <div class="first__avatar"
                                 style="background-image: url({{ $card->Big_img != null ? asset('storage/portfolio/').'/'.$card->user_id.'/'.$card->Big_img : '' }});">
                                <div class="review_col">
                                    <p>отзывы <span>{{ $card->feedbacks_count }}</span></p>
                                </div>
                            </div>
                        </div>
                        <div class="second flex">
                            <div class="dots">
                                <span class="dots__dot"></span>
                                <span class="dots__dot"></span>
                                <span class="dots__dot"></span>
                                <span class="dots__dot"></span>
                                <span class="dots__dot"></span>
                                <span class="dots__dot"></span>
                                <span class="dots__dot"></span>
                                <span class="dots__dot"></span>
                                <span class="dots__dot"></span>
                                <span class="dots__dot"></span>
                            </div>
                            <div class="second__information">
                                <p class="second__position">{{$card->sub_section->name}}</p>
                                <p class="second__name">{{ $card->user->first_name }} {{ $card->user->last_name }} {{ $card->user->patronymic }}</p>
                                <div class="ability">
                                    {{ mb_substr($card->description, 0, 160) }}
                                </div>
                                <div class="buttons flex justify-content-around">
                                    <form action="/about/{{ $card->id }}">
                                        <button class="btn btn_small">Подробнее</button>
                                    </form>
                                    <form action="https://wa.me/{{str_replace([' ', '(', ')', '-'], '', $card->whatsapp)}}" target="_blank">
                                        <button class="btn btn_small">WhatsApp</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        @if ($ads_right != null)
            <div class="advertizing">
                <img src="{{ asset('storage/').'/'.$ads_right->img }}"/>
            </div>
        @endif
    </div>
    @foreach ($topUserCards as $card)
        <div class="top-specialists_mob ">
            <div class="block">
                <div class="left flex">
                    @if ($card->user->hasRole('vip'))
                        <!-- <div class="vip_diamond">
                            <img src="/images/samples/vip.png"/>
                            <p>VIP</p>
                        </div> -->
                        <div class="vip_diamond">
                            <div class="status">
                                <img src="/images/samples/vip.png"/>
                                <p>VIP</p>
                            </div>
                            <div class="reviews_col flex">
                                <span>{{ $card->feedbacks_count }}</span>
                            </div>
                        </div>
                        
                    @elseif ($card->user->hasRole('profi'))
                        <!-- <div class="vip_diamond">
                            <div class="brief"></div>  
                            <p>Profi</p>
                        </div> -->
                        <div class="vip_diamond">
                            <div class="status">
                                <div class="brief"></div>
                                <p>Profi</p>
                            </div>
                            <div class="reviews_col flex">
                                {{ $card->feedbacks_count }}
                            </div>
                        </div>
                    @endif
                    <div class="image_avatar">
                        <img src="{{$card->Big_img != null ? asset('storage/portfolio/').'/'.$card->user_id.'/'.$card->Big_img : ''}}">
                    </div>
                    <!-- <div class="image_avatar">
                        <img src="{{$card->Big_img != null ? asset('storage/portfolio/').'/'.$card->user_id.'/'.$card->Big_img : ''}}"/>
                        <div class="reviews_col flex">
                            отзывы
                            <span>{{ $card->feedbacks_count }}</span>
                        </div>
                    </div> -->
                </div>
                <div class="right">
                    <div class="title"> {{$card->sub_section->name}}</div>
                    <div class="name"> {{ $card->user->first_name }} {{ $card->user->last_name }} {{ $card->user->patronymic }} </div>
                    <div class="details">
                        {{ mb_substr($card->description, 0, 100) }}
                    </div>
                    <div class="buttons flex justify-content-between">
                        <button onclick="location.href='/about/{{ $card->id }}'"
                                class="btn btn_btnMob flex">
                            <i class="icon-mob_arrow_near_button"></i>
                            Подробнее
                        </button>
                        <button onclick="location.href='https://wa.me/{{str_replace([' ', '(', ')', '-'], '', $card->whatsapp)}}'"
                                class="btn btn_btnMob flex">
                            <i class="icon-mob_arrow_near_button"></i>
                            WhatsApp
                        </button>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endif