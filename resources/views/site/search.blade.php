
<form id="searchForm" class="menu" method="get" action="{{route('services')}}" class = "searchPromptParent">
    <div class="menu_block flex">
        <div class="menu_block__custom-select ">
            <select class="menu-items parts" name="section_id">
                <option selected disabled>Разделы</option>
                @if( !empty($sections))
                    @foreach ($sections as $section)
                        <option value="{{ $section->id }}">{{ $section->name }}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <div class="menu_block__custom-select" id="second-select">
            <select class="menu-items city" name="region_id">
                <option selected disabled>Города</option>
                @if( !empty($regions))
                    @foreach ($regions as $region)
                        <option value="{{ $region->id }}">{{ $region->name }}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <div class="menu_block__search-block" id='searched' >
            <input type="text" placeholder="Поиск" name = "search" class = 'searchElem' id="search_name" autocomplete="off">            
            <div class="prompt_words">
                <div class = 'prompt_words__info' id="prompt_words__sections">                    
                    <ul>
                        <li>
                            Дом
                        </li>
                        <li>
                        Интернет услуги
                        </li>
                        <li>
                        Образование
                        </li>
                        <li>
                        Красота и здоровье
                        </li>
                        <li>
                        Менеджмент
                        </li>
                        <li>
                        Автомобиль
                        </li>
                    </ul>
                </div>
            </div>            
        </div>
    </div>
    <button type="submit" class="btn btn_large btn_shadow">Искать</button>
</form>