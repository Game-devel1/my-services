@if(!empty($topUserCards))
    <div class="right_inner flex">
        @foreach($topUserCards as $card)
            <div class="top-specialists__information {{$card->user->hasRole('vip') ? 'vip_border' : 'profi_border'}}">
                <div class="first {{$card->user->hasRole('vip') ? 'vip' : 'profi'}}">
                    @if ($card->user->hasRole('vip'))
                        <div class="first__top-title flex">
                            <p class="upper">Vip</p>
                            <div class="diamonds"></div>
                        </div>
                    @elseif ($card->user->hasRole('profi'))
                        <div class="first__top-title flex">
                            <p class="upper">Profi</p>
                            <div class="brief"></div>
                        </div>
                    @endif
                    <div class="first__avatar" style="background-image: url({{$card->Big_img != null ? asset('storage/portfolio/').'/'.$card->user_id.'/'.$card->Big_img : ''}});">
                        <div class="review_col">
                            <p>отзывы <span>{{ $card->feedbacks_count }}</span></p>
                        </div>
                    </div>
                </div>
                <div class="second flex">
                    <div class="dots">
                        <span class="dots__dot"></span>
                        <span class="dots__dot"></span>
                        <span class="dots__dot"></span>
                        <span class="dots__dot"></span>
                        <span class="dots__dot"></span>
                        <span class="dots__dot"></span>
                        <span class="dots__dot"></span>
                        <span class="dots__dot"></span>
                        <span class="dots__dot"></span>
                        <span class="dots__dot"></span>
                    </div>
                    <div class="second__information">
                        <p class="second__position">{{ $card->sub_section->name }}</p>
                        <p class="second__name">{{ $card->user->first_name }} {{ $card->user->last_name }} {{ $card->user->patronimyc }}</p>
                        <div class="ability">
                            {{ mb_substr($card->description, 0, 100) }}
                        </div>
                        <div class="buttons flex justify-content-around">
                            <form action="/about/{{ $card->id }}" target = "_blank">
                                <button class="btn btn_small">Подробнее</button>
                            </form>
                            <form action="https://wa.me/{{str_replace([' ', '(', ')', '-'], '', $card->whatsapp)}}" target="_blank">
                                <button class="btn btn_small">WhatsApp</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endif

