@if(!empty($topUserCards))
    @foreach ($topUserCards as $card)
        <div class="top-specialists_mob ">
            <div class="block">
                <div class="left flex">
                    @if ($card->user->hasRole('vip'))
                        <div class="vip_diamond">
                            <img src="/images/samples/vip.png" />
                            <p>VIP</p>
                        </div>
                    @elseif ($card->user->hasRole('profi'))
                        <div class="vip_diamond">
                            <div class="brief"></div>
                            <p>Profi</p>
                        </div>
                    @endif
                    <div class="image_avatar">
                        @if ($card->Big_img != null)
                            <img src="{{$card->Big_img != null ? asset('storage/portfolio/').'/'.$card->user_id.'/'.$card->Big_img : ''}}" />
                        @endif
                    </div>
                </div>
                <div class="right">
                    <div class="title"> {{$card->sub_section->name}} </div>
                    <div class="name"> {{ $card->user->first_name }} {{ $card->user->last_name }} {{ $card->user->patronimyc }} </div>
                    <div class="details">
                        {{ mb_substr($card->description, 0, 100) }}
                    </div>
                    <div class="buttons justify-content-around flex">
                        <button onclick="location.href='/about/{{$card->id}}'" class="btn btn_btnMob flex">
                            <i class="icon-mob_arrow_near_button"></i> Подробнее </button>
                        <button onclick="location.href='https://wa.me/{{str_replace([' ', '(', ')', '-'], '', $card->whatsapp)}}'" class="btn btn_btnMob flex">
                            <i class="icon-mob_arrow_near_button"></i> WhatsApp </button>
                    </div>
                </div>
            </div>
        </div>
    @endforeach    
@endif