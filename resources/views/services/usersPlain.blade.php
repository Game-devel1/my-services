@foreach ($plainUserCards as $card)
    <div class="listings_blocks flex" style='margin-top: 40px;'>
        <div class="listings_blocks__left">
            <div class="avatar" style="background-image: url( {{ $card->Big_img != null ? asset('storage/portfolio/').'/'.$card->user_id.'/'.$card->Big_img : '' }})"></div>
        </div>
        <div class="listings_blocks__rigth">
            <div class="flex justify-content-between">
                <div class="emlpoyee_info">
                    <p class="employee_position">{{$card->section->name}}</p>
                    <p class="employee_name">{{ $card->user->first_name }} {{ $card->user->last_name }} {{ $card->user->patronymic }} <span>{{$card->sub_section->name}}</span></p>
                </div>
                <div class="listings_blocks_buttons">
                <form action="https://wa.me/{{str_replace([' ', '(', ')', '-'], '', $card->whatsapp)}}" target="_blank" style = "margin-bottom:15px;"> 
                      <button class="btn btn_writeWhatssap">
                                Написать в WhatsApp
                      </button>
                    </form>
                    <form action="/about/{{ $card->id }}" target="_blank">
                        <button class="btn btn_details">Подробнее</button>
                    </form>
                </div>
            </div>
            <div class="reviews_right">
                <div class="review_flexed flex ">
                    <i class="icon-review_svg"></i>
                    <p>отзывы: {{ $card->feedbacks_count }}</p>
                </div>
            </div>
        </div>   
    </div>
@endforeach                  