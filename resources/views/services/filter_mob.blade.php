<div id = "filter_mob">
    <div class="title-close-mob flex" id = 'close-filter'>
        Закрыть фильтр
    </div>
    <div class="filter-block">
        <div class="filter-block__sub">
            <ul class="default-option_filter_mob flex justify-content-between" id = "section_mob_filter">
                <li class = "title" id ="section_title_mob_filter">Разделы </li>
                        <svg xmlns="http://www.w3.org/2000/svg" width="59" height="59" viewBox="0 0 59 59"><g><g><path id="arrow" fill="#92abb4" d="M16.894 29.5a1.823 1.823 0 0 1 3.113-1.29l7.669 7.67V19.814a1.824 1.824 0 0 1 3.647 0v16.064l7.669-7.668a1.823 1.823 0 1 1 2.578 2.578L29.5 42.861 17.428 30.79a1.817 1.817 0 0 1-.534-1.29zm-16.902 0C-.008 13.229 13.23-.008 29.5-.008S59.008 13.23 59.008 29.5c0 16.27-13.237 29.508-29.508 29.508C13.23 59.008-.008 45.77-.008 29.5zm55.369 0C55.36 15.24 43.76 3.64 29.5 3.64S3.64 15.24 3.64 29.5s11.6 25.86 25.86 25.86 25.86-11.6 25.86-25.86z"/></g></g></svg>
                <!-- <i class = "icon-dropdown-arrow-select-mob"></i> -->
                <!-- <i class ="icon-active_arrow" id = 'section_arrow_ico'></i> -->
            </ul>
            <ul class="select_ul_mob_filter">
                @if(!empty($sections))
                    @foreach($sections as $section)
                        <li data-id = "{{$section->id}}" class = "option section_mob_filter_li section_filter">{{$section->name}}</li>
                    @endforeach
                @endif
            </ul>
        </div>
    </div>
    <div class="filter-block">
        <div class="filter-block__sub">
            <ul class="default-option_filter_mob flex justify-content-between" id = "sub_section_mob_filter">
                <li class = "title" id ="sub_section_title_mob_filter">подраздел</li>
                <svg xmlns="http://www.w3.org/2000/svg" width="59" height="59" viewBox="0 0 59 59"><g><g><path id="arrow" fill="#92abb4" d="M16.894 29.5a1.823 1.823 0 0 1 3.113-1.29l7.669 7.67V19.814a1.824 1.824 0 0 1 3.647 0v16.064l7.669-7.668a1.823 1.823 0 1 1 2.578 2.578L29.5 42.861 17.428 30.79a1.817 1.817 0 0 1-.534-1.29zm-16.902 0C-.008 13.229 13.23-.008 29.5-.008S59.008 13.23 59.008 29.5c0 16.27-13.237 29.508-29.508 29.508C13.23 59.008-.008 45.77-.008 29.5zm55.369 0C55.36 15.24 43.76 3.64 29.5 3.64S3.64 15.24 3.64 29.5s11.6 25.86 25.86 25.86 25.86-11.6 25.86-25.86z"/></g></g></svg>
                <!-- <i class = "icon-dropdown-arrow-select-mob"></i> -->
            </ul>
            <ul class="select_ul_mob_filter">
                <li class = "option sub_section_mob_filter_li sub_section_filter" data-id = "" data-section="">Разработка сайтов и приложении</li>
                <li class = "option sub_section_mob_filter_li">Разработка ПО</li>
                <li class = "option sub_section_mob_filter_li">Внедрение 1C</li>
                <li class = "option sub_section_mob_filter_li">Компьютерная помощь</li>
                <li class = "option sub_section_mob_filter_li">Рисунки и иллюстрации</li>
                <li class = "option sub_section_mob_filter_li">Дизайн полиграфии</li>
                <li class = "option sub_section_mob_filter_li">Печатные услуги</li>
                <li class = "option sub_section_mob_filter_li">Копирайтинг</li>
                <li class = "option sub_section_mob_filter_li">Создание презентации</li>
                <li class = "option sub_section_mob_filter_li">Рисунки и иллюстрации</li>
                <li class = "option sub_section_mob_filter_li">Дизайн полиграфии</li>
                <li class = "option sub_section_mob_filter_li">Печатные услуги</li>
                <li class = "option sub_section_mob_filter_li">Копирайтинг</li>
                <li class = "option sub_section_mob_filter_li">Создание презентации</li>
            </ul>
        </div>
    </div>
    <div class="filter-block">
        <div class="filter-block__sub">
            <ul class="default-option_filter_mob flex justify-content-between" id = "city_section_mob_filter">
                <li class = "title" id ="city_section_title_mob_filter">город</li>
                        <svg xmlns="http://www.w3.org/2000/svg" width="59" height="59" viewBox="0 0 59 59"><g><g><path id="arrow" fill="#92abb4" d="M16.894 29.5a1.823 1.823 0 0 1 3.113-1.29l7.669 7.67V19.814a1.824 1.824 0 0 1 3.647 0v16.064l7.669-7.668a1.823 1.823 0 1 1 2.578 2.578L29.5 42.861 17.428 30.79a1.817 1.817 0 0 1-.534-1.29zm-16.902 0C-.008 13.229 13.23-.008 29.5-.008S59.008 13.23 59.008 29.5c0 16.27-13.237 29.508-29.508 29.508C13.23 59.008-.008 45.77-.008 29.5zm55.369 0C55.36 15.24 43.76 3.64 29.5 3.64S3.64 15.24 3.64 29.5s11.6 25.86 25.86 25.86 25.86-11.6 25.86-25.86z"/></g></g></svg>
                <!-- <i class = "icon-dropdown-arrow-select-mob"></i> -->
            </ul>
            @if(!empty($regions))
                <ul class="select_ul_mob_filter">
                    @foreach($regions as $region)
                        <li class = "option city_section_mob_filter_li region_filter" data-id="{{$region->id}}">{{$region->name}}</li>
                    @endforeach
                </ul>
            @endif
        </div>
    </div>
    <div class="filter-block">
        <div class="filter-block__sub">
            <ul class="default-option_filter_mob flex justify-content-between" id = "district_mob">
                <li class = "title" id ="region_title_mob">район</li><svg xmlns="http://www.w3.org/2000/svg" width="59" height="59" viewBox="0 0 59 59"><g><g><path id="arrow" fill="#92abb4" d="M16.894 29.5a1.823 1.823 0 0 1 3.113-1.29l7.669 7.67V19.814a1.824 1.824 0 0 1 3.647 0v16.064l7.669-7.668a1.823 1.823 0 1 1 2.578 2.578L29.5 42.861 17.428 30.79a1.817 1.817 0 0 1-.534-1.29zm-16.902 0C-.008 13.229 13.23-.008 29.5-.008S59.008 13.23 59.008 29.5c0 16.27-13.237 29.508-29.508 29.508C13.23 59.008-.008 45.77-.008 29.5zm55.369 0C55.36 15.24 43.76 3.64 29.5 3.64S3.64 15.24 3.64 29.5s11.6 25.86 25.86 25.86 25.86-11.6 25.86-25.86z"/></g></g></svg>
                <!-- <i class ="icon-active_arrow" id = 'section_arrow_ico'></i> -->
            </ul>
            <ul id="district-select_mob" class="select_ul_mob_filter">
                <li class = "option district_section_mob_filter_li region_filter">Алатауский</li>
                <li class = "option district_section_mob_filter_li">Алатауский</li>
                <li class = "option district_section_mob_filter_li">Алатауский</li>
                <li class = "option district_section_mob_filter_li">Алатауский</li>
            </ul>
        </div>
    </div>
    <div class="title_sort">СОРТИРОВКА</div>
    <div class="search_filter">
        <div class="search_filter__sub">
            <ul class="default-option_filter_mob flex justify-content-between" id = "data_filter_mob">
                <li class = "title" id ="data_filter_mob_title">Дата</li><svg xmlns="http://www.w3.org/2000/svg" width="59" height="59" viewBox="0 0 59 59"><g><g><path id="arrow" fill="#92abb4" d="M16.894 29.5a1.823 1.823 0 0 1 3.113-1.29l7.669 7.67V19.814a1.824 1.824 0 0 1 3.647 0v16.064l7.669-7.668a1.823 1.823 0 1 1 2.578 2.578L29.5 42.861 17.428 30.79a1.817 1.817 0 0 1-.534-1.29zm-16.902 0C-.008 13.229 13.23-.008 29.5-.008S59.008 13.23 59.008 29.5c0 16.27-13.237 29.508-29.508 29.508C13.23 59.008-.008 45.77-.008 29.5zm55.369 0C55.36 15.24 43.76 3.64 29.5 3.64S3.64 15.24 3.64 29.5s11.6 25.86 25.86 25.86 25.86-11.6 25.86-25.86z"/></g></g></svg>
                <!-- <i class = "icon-dropdown-arrow-select-mob"></i> -->
            </ul>
            <ul class="select_ul_mob_filter">
                <li data-id = "new" class = "option datafilter_mob_li data_filter">Новые</li>
                <li data-id = "old" class = "option datafilter_mob_li data_filter">Давние</li>
            </ul>
        </div>
    </div>
    <div class="search_filter">
        <div class="search_filter__sub">
            <ul class="default-option_filter_mob flex justify-content-between" id = "price_filter_mob">
                <li class = "title" id ="price_filter_mob_title">Стоимость</li><svg xmlns="http://www.w3.org/2000/svg" width="59" height="59" viewBox="0 0 59 59"><g><g><path id="arrow" fill="#92abb4" d="M16.894 29.5a1.823 1.823 0 0 1 3.113-1.29l7.669 7.67V19.814a1.824 1.824 0 0 1 3.647 0v16.064l7.669-7.668a1.823 1.823 0 1 1 2.578 2.578L29.5 42.861 17.428 30.79a1.817 1.817 0 0 1-.534-1.29zm-16.902 0C-.008 13.229 13.23-.008 29.5-.008S59.008 13.23 59.008 29.5c0 16.27-13.237 29.508-29.508 29.508C13.23 59.008-.008 45.77-.008 29.5zm55.369 0C55.36 15.24 43.76 3.64 29.5 3.64S3.64 15.24 3.64 29.5s11.6 25.86 25.86 25.86 25.86-11.6 25.86-25.86z"/></g></g></svg>
                <!-- <i class = "icon-dropdown-arrow-select-mob"></i> -->
                <!-- <i class ="icon-active_arrow" id = 'section_arrow_ico'></i> -->
            </ul>
            <ul class="select_ul_mob_filter">
                <li data-id = "low" class = "option price_filter_mob_li price_filter">Низкая</li>
                <li data-id = "high" class = "option price_filter_mob_li price_filter">Высокая</li>
            </ul>
        </div>
    </div>
    <div class="show_only">показывать только</div>
    <div class="flex checked_title-mob justify-content-between">
        <input type="checkbox" id="account-mob">
        <label for="account-mob">С аккаунтом VIP | PROFI </label>
    </div>
    <div class="flex checked_title-mob justify-content-between">
        <input type="checkbox" id="portfolio-mob">
        <label data-id = "notNull" class = "portfolio_filter" for="portfolio-mob">С портфолио </label>
    </div>
    <div class="button-filter">
        <button class = "btn btn_btnFilter" id="apply-filter">Применить фильтр</button>
    </div>
</div>
