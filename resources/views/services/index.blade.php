@extends('layouts.main')
@section('pageTitle','Раздел услуг')
@section('content')
@section('template','all_services')
@section('namespace','all_services')
  <!-- Страница все услуги. Карточки специалистов  -->
  <div class="fon-wrapper">
    <div class="fon"></div>
  </div>
  <div class="images_back">
    <div class="stamp">
      <img src="{{ asset('images/samples/stamp.png') }}" />
    </div>
    <div class="leaf3">
      <img src="{{ asset('images/samples/Leaf3.png') }}" />
    </div>
    <div class="leaf4">
      <img src="{{ asset('images/samples/Leaf4.png') }}" />
    </div>
    <div class="leaf9">
      <img src="{{ asset('images/samples/Leaf9.png') }}" />
    </div>
    <div class="cup">
      <img src="{{ asset('images/samples/cup.png') }}" />
    </div>
  </div>
  <div class="topTitle mob">
      <span>МОИ УСЛУГИ</span>
      <p class="topTitle__p">портал объединяющий заказчиков и исполнителей услуг</p>
  </div>
  <div class="container-fluid ">
    <div class="web-all_services">
      <!-- <div class="topTitle flex web">
          <div class="topTitle__flex">
              <div class="blockMain">МОИ УСЛУГИ</div>
              <div class="blockSecond">– биржа услуг всего Казахстана</div>
          </div>
      </div> -->
      <div class="topTitle webTitle">
            <span>МОИ УСЛУГИ</span>
            <p class="topTitle__p"> портал объединяющий заказчиков <br> и исполнителей услуг</p>
        </div>

      <form id="searchForm" action="{{route('services')}}" method="get" class = "searchPromptParent">
        <div class="menu_card_services flex">
          <div class="menu_card_services__search-block">
            <input type="text" class="searchElem" placeholder="Поиск" name="search" list = 'search_list' id="search_name" autocomplete="off" value="<?=$searchString?>">
            <div class="prompt_words">
              <div class = 'prompt_words__info' id="prompt_words__sections">
                  <!-- <div class="title">Разделы</div> -->
                  <ul >
                      <li>
                          Дом
                      </li>
                      <li>
                        Интернет услуги
                      </li>
                      <li>
                        Образование
                      </li>
                      <li>
                        Красота и здоровье
                      </li>
                      <li>
                        Менеджмент
                      </li>
                      <li>
                        Автомобиль
                      </li>
                  </ul>
              </div>
            </div>
          </div>
          <button class="btn btn_large btn_shadow">Искать</button>
        </div>
      </form>
      <div class="filter flex">
      <!-- <a href = "/">Главная страница -</a>
      <a href = "/services?search=">Раздел услуг</a> -->
        <div class="filter__left">
          <div class="title flex">
            <a href = "/">Главная страница</a>
            <span>-</span>
            <a href="/services?search=">Раздел услуг</a>
          </div>
          @include('services.filter')
        </div>
        <div class="filter__right">
          @if($topUserCards)
            @include('services.usersTop')
          @endif

          @if ($ads_bottom != null)
            <div class="horizontal_advertizing">
              <img src="{{ asset('storage/').'/'.$ads_bottom->img }}" />
            </div>
          @endif
          @if($plainUserCards)
            <!-- <div class="worker_cards">
              <div class="worker_cards__colomn flex post-load"> -->
              <div class="userPlainContainer">
                @include('services.usersPlain')
              </div>
              <!-- </div> -->
              <a href="#" id="load_service" data-limit="4" data-max="{{ $plainUsersCount }}" data-sort="{{ json_encode($searchParams) }}" class="worker_cards__a load_service">Загрузить еще</a>
            <!-- </div>    -->
          @endif
        </div>
      </div>
    </div>
    <div class="mob_allServices">
      <div class="filter_and_previous flex justify-content-between">
        <div onclick="window.history.back();" class="first">
          <p>назад</p>
        </div>
        <button class="btn btn_filterBtn right flex" id="filter_btn_mob">
          <i class="icon-filter-ico"></i> Фильтр </button>
      </div>
      @include('services.filter_mob')
      <div class="unfiltered_bock">
        @include('services.usersTopMobile')
        <div class="horizontal-advertizing" style="background-image: url({{asset('/images/samples/adver_2.jpg')}});"></div>
        @if($plainUserCards)
        <div class="mob_listings post-load-mobile">
          @include('services.usersPlainMobile')
        </div>
        <div class="download">
          <a href="#" class="load_service mob_load" data-limit="4" data-max="{{ $plainUsersCount }}" data-sort="{{ json_encode($searchParams) }}">Загрузить еще</a>
        </div>
        @endif
      </div>
    </div>
  </div>
    @push('scripts')
      <script src="{{ asset('js/filter.js') }}"></script>
  @endpush
@endsection
