<div class="block">
    <div class="title_filter">ФИЛЬТР</div>
    <div class="filter_menu">
        <div class="items flex">
            <div class="select_wrap">
                <ul class="default-option flex justify-content-between" id = "section">
                    <li class = "title" id ="section_title">Разделы</li>
                </ul>
                @if(!empty($sections))
                    <ul class="select_ul">
                        @foreach($sections as $section)
                            <li class = "option section_li section_filter"  href="javascript:void(0);" data-id="{{$section->id}}">{{$section->name}}</li>
                        @endforeach
                    </ul>
                @endif
            </div>
        </div>
        <div class="items flex">
            <div class="select_wrap" id = 'subsection'>
                <ul class="default-option flex justify-content-between" >
                    <li class = "title classik" id = "subsection_title">подраздел</li>
                </ul>
                <ul class="select_ul" id="filter_sub_section_list">
                    <li class = "option subsection_li sub_section_filter" data-id="" data-section=""></li>
                </ul>
            </div>
        </div>
        <div class="items">
            <div class="select_wrap ">
                <ul class="default-option flex justify-content-between">
                    <li class = "title" id ="region_title">город</li>
                </ul>
                @if(!empty($regions))
                    <ul class="select_ul">
                         @foreach($regions as $region)
                            <li class = "option region_li region_filter" href="#" data-id="{{$region->id}}">{{$region->name}}</li>
                        @endforeach
                    </ul>
                @endif
            </div>
        </div>
        <div class="items flex">
            <div class="select_wrap ">
                <ul class="default-option flex justify-content-between" id = "section">
                    <li class = "title" id ="district_title">Район</li>
                    <!-- <i class ="icon-active_arrow" id = 'section_arrow_ico'></i> -->
                </ul>
                <ul id="district-select" class="select_ul"></ul>
            </div>
        </div>
        <div class="title_filter"> СОРТИРОВКА </div>
        <div class="items flex">
            <div class="select_wrap">
                <ul class="default-option flex justify-content-between" id = "section">
                    <li class = "title" id ="data_title">Дата</li>
                </ul>
                <ul class="select_ul">
                    <li data-id = "new" class = "option data_li data_filter">Новые</li>
                    <li data-id = "old" class = "option data_li data_filter">Давние</li>
                </ul>
            </div>
        </div>
        <div class="items flex">
            <div class="select_wrap">
                <ul class="default-option flex justify-content-between" id = "section">
                    <li class = "title" id ="price_title">Стоимость</li>
                </ul>
                <ul class="select_ul">
                    <li data-id = "low" class = "option price_li price_filter">Низкая</li>
                    <li data-id = "high" class = "option price_li price_filter">Высокая</li>
                </ul>
            </div>
        </div>
        <div class="title_filter">показывать только</div>
        <div class="flex checked_title justify-content-between">
            <input type="checkbox" id='account'>
            <label for="account">С аккаунтом VIP | PROFI </label>
        </div>
        <br>
        <div class="flex checked_title justify-content-between">
            <input type="checkbox" id="portfolio">
            <label data-id = "notNull" class = "portfolio_filter" for="portfolio">С портфолио</label>
        </div>
    </div>
</div>
