<div class="mob_listings">
    @foreach ($plainUserCards as $card)
        <div class="listining_block_mob">
            <div class="left flex">
                <div class="avatar-image-mob">
                    <img src="{{$card->Big_img != null ? asset('storage/portfolio/').'/'.$card->user_id.'/'.$card->Big_img : ''}}" />
                </div>
                <div class="left-second">
                    <div class="top-title"> {{$card->section->name}} </div>
                    <div class="name"> {{ $card->user->first_name }} {{ $card->user->last_name }} {{ $card->user->patronymic }}</div>
                    <div class="info">{{$card->sub_section->name}}</div>
                </div>
            </div>
            <div class="right flex">
                <div class="reviews flex">
                    <i class="icon-review_svg"></i>
                    <p>отзывы: {{ $card->feedbacks_count }}</p>
                </div>
                <div class="buttons flex">  
                    <form action="/about/{{ $card->id }}" target = "_blank">
                        <button class="btn btn_btnSmallMob flex">
                            <i class="icon-mob_arrow_near_button"></i> Подробнее </button>
                    </form>
                    <form action="https://wa.me/{{str_replace([' ', '(', ')', '-'], '', $card->whatsapp)}}" target="_blank">
                        <button  class="btn btn_btnSmallMob flex">
                            <i class="icon-mob_arrow_near_button"></i> WhatsApp </button>
                    </form>
                </div>
            </div>
        </div>
    @endforeach
</div>