<!-- Modal content-->
<div class="modal-content smallModalContent">
  <div class="modal-loader">
    <img src="{{ asset('images/loader.gif') }}">
  </div>
  <div class="modal-header flex">
    <div class="title">Изменить пароль </div>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <i class="icon-close_modal-ico close" aria-hidden="true"></i>
    </button>
  </div>
  <div>
    <div class="modal-body smallModal changePassModalBody">
      <form id="changePassForm" action="{{ route('change.password') }}" method="post">      
        <label for='current_password'>Введите текущий пароль</label>
        <input type="password" id='current_password' name='current_password'>
        <span class = "error_word" data-error="current_password"></span>
        <label for="new_password">Введите новый пароль:</label>
        <input type="password" id='new_password' name='new_password'>
        <span class = "error_word" data-error="new_password"></span>
        <label for="new_confirm_password">Повторите новый пароль </label>
        <input type="password" id='new_confirm_password' name='new_confirm_password'>
        <span class = "error_word" data-error="new_confirm_password"></span>
        <button class='btn btn_btnEnterModal flex'>Изменить пароль </button>
      </form>
    </div>
  </div>
</div>