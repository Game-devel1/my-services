<div class="modal" id="signUpModal">
    <div class="modal-dialog modal-dialog-centered" role = 'document' id="diologSignUp">
        <div class="modal-content" id = 'bigModalContent'>
			<div class="modal-loader">
				<img src="{{ asset('images/loader.gif') }}">
			</div>
            <div class="modal-header flex">
                <div class="title">Регистрация</div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class = "icon-close_modal-ico close" aria-hidden="true"></i>
                </button>
            </div>
            <div>
                <div class="modal-body bigModal">
                    <form id="regForm" method="POST" action="{{ route('register') }}">
                        <p>Пожалуйста, введите свои данные, чтобы зарегистрироваться.</p>
                        <p class="text_left"><span class="required_star">*</span>- указаны обязательные поля для заполнения</p>
                        <div class="flex inputs_signUp justify-content-between">
                            <div class = 'flexed_first'>
                                <label for="name_signUp">Имя <span class="required_star"> *</span>:</label>
                                <input  type="text" id = 'name_signUp' name = 'first_name'>
                                <span class = "error_word" data-error="first_name"></span>
                            </div>
                            <div class = 'flexed_first'>
                                <label for="email_signUp">E-mail <span class="required_star"> *</span>:</label>
                                <input type="text" id = 'email_signUp' name = 'email'>
                                <span class = "error_word" data-error="email"></span>
                            </div>
                        </div>
                        <div class="flex inputs_signUp justify-content-between">
                            <div class = 'flexed_first'>
                                <label for="sname_signUp">Фамилия <span class="required_star"> *</span>:</label>
                                <input type="text" id = 'sname_signUp' name = 'last_name'>
                                <span class = "error_word" data-error="last_name"></span>
                            </div>
                            <div class = 'flexed_first'>
                                <label for="phone_signUp">Телефон:</label>
                                <!-- <input type="text" id = 'phone_signUp' name = 'contact' data-mask="+7(999) 999 99 99" placeholder = "+7(___) ___ __ __"> -->
                                <input type="text" id = 'phone_signUp' name = 'phone_signUp'>
                                <span class = "error_word" data-error="contact"></span>
                            </div>
                        </div>
                        <div class="flex inputs_signUp justify-content-between">
                            <div class = 'flexed_first'>
                                <label for="fname_signUp">Отчество:</label>
                                <input type="text" id = 'fname_signUp' name = 'patronymic'>
                                <span class = "error_word" data-error="patronymic"></span>
                            </div>
                            <div class = 'flexed_first'>
                                <label for="password_signUp">Пароль <span class="required_star"> *</span>:</label>
                                <input type="password" id = 'password_signUp' name = 'password'>
                                <span class = "error_word" data-error="password"></span>
                            </div>
                        </div>
                        <div class="flex inputs_signUp justify-content-between">
                            <div class = 'flexed_first mobi'>
                                <label for="password_signUp">Загрузка документа увеличит доверие к вашей карточке услуги</label>
                                <div class = "download_image_modal">
                                    <input type = "file" name = "pass_img" id = "file_img_modal" data-multiple-caption = '{count} файлов выбрано' class = "inputfile" />
                                    <label  for = "file_img_modal" >
                                        <p>Загрузить документ <span class="required_star"> *</span> </p>
                                        <i class = "icon-download_arrow"></i>
                                    </label>
                                </div>
                                <span class = "error_word" data-error="pass_img"></span>
                                <!-- </form> -->
                            </div>
                        </div>
                        <button class = 'btn btn_btnEnterModal flex registerBtn'>Зарегистрироваться </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="success-reg">
    <div class="modal-dialog modal-dialog-centered" role = 'document' id ="passwordDiolagModal">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header flex">
                <div class="title">&nbsp;</div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class = "icon-close_modal-ico close" aria-hidden="true"></i>
                </button>
            </div>
            <div class="modal-body smallModalPassword">
                <p style="margin: 3em 0">Регистрация прошла успешно! <br> Пожалуйста, подтвердите вашу почту. </p>
            </div>
        </div>
    </div>
</div>
<button id="reg_btn" style="display: none" data-toggle="modal" data-target="#success-reg"></button>
