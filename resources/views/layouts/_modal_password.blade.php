<div class="modal" id="passwordModal">
    <div class="modal-dialog modal-dialog-centered" role = 'document' id ="passwordDiolagModal">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-loader">
                <img src="{{ asset('images/loader.gif') }}">
            </div>
            <div class="modal-header flex">
                <div class="title">Забыли пароль?</div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class = "icon-close_modal-ico close" aria-hidden="true"></i>
                </button>
            </div>
            <div>
                <div class="modal-body smallModalPassword">
                    <p>Пожалуйста, введите email</p>
                    <form method="post" id="password-reset" action="{{ route('password.email') }}">                    
                        <label for="modal-email-password">E-mail:</label>
                        <input class = 'bigInputType' type="email" id = 'modal-email-password' name = 'email' required>
                        <span class = "error_word" data-error="email"></span>
                        <button style="margin-top: 10px" class = 'btn btn_btnEnterModal flex'>Отправить</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="success-password">
    <div class="modal-dialog modal-dialog-centered" role = 'document' id ="passwordDiolagModal">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header flex">
                <div class="title">&nbsp;</div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class = "icon-close_modal-ico close" aria-hidden="true"></i>
                </button>
            </div>
            <div class="modal-body smallModalPassword">
                <p style="margin: 3em 0">Ссылка на сброс пароля была отправлена!</p>
            </div>
        </div>
    </div>
</div>

<button id="password_btn" style="display: none" data-toggle="modal" data-target="#success-password"></button>
