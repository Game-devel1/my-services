<div class="modal" id="login">
    <div class="modal-dialog modal-s modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-loader">
                <img src="{{ asset('images/loader.gif') }}">
            </div>
            <div class="modal-header flex">
                <div class="title">Войти</div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class = "icon-close_modal-ico close" aria-hidden="true"></i>
                </button>
            </div>
            <form id="loginForm" method="POST" action="{{ route('login') }}">                 
                <div class="modal-body smallModal">
                    <p>Пожалуйста, введите свои данные, чтобы войти в систему.</p>
                    <label for="modal-email">E-mail:</label>
                    <input class = 'bigInputType' type="text"  name = 'email' required autocomplete="email" autofocus>
                    <label for="password">Пароль:</label>
                    <input class = 'bigInputType' type="password"  name = 'password' required autocomplete="current-password">
                    <span class = "error_word" data-error="email"></span>
                    <span class = "error_word" data-error="password"></span>
                    <div class="link-on-sign-in-modal flex">
                        <a href="#"  data-dismiss = 'modal' id = "passwordLink" data-toggle="modal" data-target="#passwordModal">Забыли пароль? </a>
                        <a href="#" data-dismiss = 'modal' id = 'linkSignUp' data-toggle="modal" data-target="#signUpModal">Зарегистрироваться</a>
                    </div>
                    <button class = 'btn btn_btnEnterModal flex'>Войти</button>
                </div>
            </form>
        </div>
    </div>
</div>
