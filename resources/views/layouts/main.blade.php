<!doctype html>
<html lang="{{ app()->getLocale() }}">
  <head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="HandheldFriendly" content="true">
    <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width">
    <meta name="theme-color" content="#000000">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="csrf-token" content="{{ csrf_token() }}">
	<meta name="_token" content="{{ csrf_token() }}"/>
    <title>@yield('pageTitle')</title>
    
    <link rel="stylesheet" href="{{ asset('css/app.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/mystyle.css') }}">
    @stack('head')
  </head>
  <div class="loader_bg">
    <div class="loader">
      <div class="cssload-loader">МОИ УСЛУГИ</div>
    </div>
  </div>
  <body class="@yield('bodyClass')" data-namespace="@yield('namespace')" data-template="@yield('template')">
    <div class="wrapper">
      <div class="wrapper-in">
        <!-- .header -->
        <header class="header">
          <div class="white-block-header">
            <div class="container">
              <div class="flex justify-content-between header_information">
                <a href="/" class="logo">МОИ УСЛУГИ</a>
                <div class = 'search_header flex' id = 'header_searcher'>
                  <form id = "searchHeadForm" action="{{route('services')}}" method="get" style = "width:100%" class = "searchPromptParent">
                    <input type="text" placeholder="Поиск" name = "search" class = 'search_input searchElem' id="searchHeader" autocomplete="off">                    
                    <div class="prompt_words">
                      <div class = 'prompt_words__info' id="prompt_words__sections">
                          <!-- <div class="title">Разделы</div> -->
                          <ul>
                              <li>
                                  Дом
                              </li>
                              <li>
                                Интернет услуги
                              </li>
                              <li>
                                Образование
                              </li>
                              <li>
                                Красота и здоровье
                              </li>
                              <li>
                                Менеджмент
                              </li>
                              <li>
                                Автомобиль
                              </li>
                          </ul>
                      </div>
                    </div>
                  </form>
                  <button type="submit" class = "btn btn_large btn_shadow serarch_btn_header">Искать</button>
                </div>
                <div class="header__buttons flex">
                  <div class="header_btn_web flex justify-content-between">
                    <!-- <button class="btn btn_small btn_yellow">Подать объявление </button> -->
                    @if (Auth::check() && !Route::is('my_portfolio'))
                      @if (Auth::user()->hasRole('admin'))
                        <button type="button" onclick="event.preventDefault(); 
                          document.getElementById('logout-form').submit();" class="btn btn_small btn_blue">Выход</button>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                      @else                    
                        <button type="button" onclick="window.location='{{route("my_portfolio")}}'" class="btn btn_small btn_blue">Мой профиль</button>
                      @endif
                    @elseif (Route::is('my_portfolio'))
                      <button type="button" onclick="event.preventDefault(); 
                        document.getElementById('logout-form').submit();" class="btn btn_small btn_blue">Выход</button>
                      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                          @csrf
                      </form>
                    @else
                      <button class="btn btn_small btn_blue" data-toggle="modal" data-target="#login">Вход </button>
                      <button class="btn btn_small btn_blue" data-dismiss="modal" data-toggle="modal" data-target="#signUpModal">Регистрация </button>
                    @endif                      
                  </div>
                  <div style="width: auto;" class="header_btn_mob  justify-content-between">
                  @if (Auth::check() && !Route::is('my_portfolio'))
                    <i class="icon-account"></i>
                    <a href="{{route('my_portfolio')}}">мой профиль</a>
                  @elseif (Route::is('my_portfolio'))
                    <button class="btn btn_mobIconPart flex" data-toggle="modal" data-target="#changePassword" data-backdrop="false">
                       <i class="icon-lock"></i> Изменить пароль </button>
                    <button type="button" onclick="event.preventDefault(); 
                      document.getElementById('logout-form').submit();" class="btn btn_small btn_blue">Выход</button>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                  @else
                    <span class = "circle" id = "signInModalMob"   data-toggle="modal" data-target="#login" data-backdrop="false"></span>
                  @endif
                  </div>
                </div>
              </div>
            </div>
          </div>
        </header>         
        <!-- end .header -->        
          <!-- .content -->
          <main class="content">  
            @if(Session::has('message'))
              <p class="my_alert alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }} <span onclick="$(this).parent().remove()">x</span> </p>
            @endif          
            @yield('content')
          </main><!-- end .content -->
		<!-- .footer -->
		<div class="services-item">
		  	
			@php
                $count = 0;
                $sections_icon = ['/images/samples/home.png', '/images/samples/heart.png', '/images/samples/internet.png', '/images/samples/management.png', '/images/samples/education.png', '/images/samples/car.png'];
            @endphp
          	<div class="services_items-mob">
			  <div class="menu__close">
              <span class="menu__close_btn"><svg xmlns="http://www.w3.org/2000/svg" width="124" height="21" viewBox="0 0 124 21">
                  <g>
                    <g>
                      <path d="M.477 8.796h107.895c-2.143-3.253-3.21-5.374-3.32-8.266 4.875 4.985 11.212 7.581 18.87 10.024-7.658 2.337-13.716 5.191-18.87 10.027.329-3.144 1.218-5.042 3.386-8.5H.477z" />
                    </g>
                  </g>
                </svg>Закрыть услуги</span>
			</div>
		  	@if (!empty($sections))
                @foreach ($sections as $section)
            <div class="low-item">
              <div class="low-item_search">
                <ul class="low-item_search_list">
                  <li class="low-item_search_img"><img src="{{$sections_icon[$count]}}" alt="#"></li>
                  <li class="title">{{$section->name}}</li>
                  <li class="low-item_search_arrow"><svg xmlns="http://www.w3.org/2000/svg" width="59" height="59" viewBox="0 0 59 59">
                      <g>
                        <g>
                          <path id="arrow" fill="#92abb4" d="M16.894 29.5a1.823 1.823 0 0 1 3.113-1.29l7.669 7.67V19.814a1.824 1.824 0 0 1 3.647 0v16.064l7.669-7.668a1.823 1.823 0 1 1 2.578 2.578L29.5 42.861 17.428 30.79a1.817 1.817 0 0 1-.534-1.29zm-16.902 0C-.008 13.229 13.23-.008 29.5-.008S59.008 13.23 59.008 29.5c0 16.27-13.237 29.508-29.508 29.508C13.23 59.008-.008 45.77-.008 29.5zm55.369 0C55.36 15.24 43.76 3.64 29.5 3.64S3.64 15.24 3.64 29.5s11.6 25.86 25.86 25.86 25.86-11.6 25.86-25.86z" />
                        </g>
                      </g>
                    </svg></li>
                </ul>
                <ul class="low-item_search_menu">
				          @foreach ($subsections as $sub_section)
                    @if ($sub_section->section_id == $section->id)
                      <li onclick = "location.href = '{{route('services')}}?search={{$sub_section->id}}'" class = "low-item_search_menu_item">{{$sub_section->name}}</li>
                    @endif
                  @endforeach
                </ul>
              </div>
			</div>
			            @php
                      $count++;
                    @endphp
                  @endforeach
            @endif 
          </div>
        </div>
        <div class="mob-search" id = "search_mob_menu">
            <div class="title-close-mob flex" id = "close_search">
              Закрыть поиск
            </div>
            <form id="searchForm_mob" action="{{ route('services') }}" method="get" class="forms">
              <label for="parts">Разделы</label>
              <div class="select-mob">
                  <select name="section_id" class="menu-items_parts section_mob_parts">
                      <option selected="" disabled=""></option>
                      @foreach ($sections as $section)
                        <option value="{{$section->id}}">{{$section->name}}</option>
                      @endforeach                        
                  </select>
              </div>
              <label for="parts">Города</label>
              <div class="select-mob">
                  <select name="region_id" class="menu-items_parts">
                      <option selected="" disabled=""></option>
                        @foreach ($regions as $region)
                          <option value="{{$region->id}}">{{$region->name}}</option>
                        @endforeach                            
                  </select>
              </div>
              <label for="search">Поиск</label>
              <input type="text" name="search" list = 'none' id="search_name_mob" autocomplete="off">
              @if (!empty($subsections))
                  @foreach ($sections as $section)
                      <datalist id ="{{$section->id}}">
                          @foreach ($subsections as $sub_section)
                              @if ($sub_section->section_id == $section->id)                                
                                      <option data-value="{{ $sub_section->id }}" value="{{ $sub_section->name }}"></option>                                                        
                              @endif    
                          @endforeach                        
                      </datalist>
                  @endforeach                        
              @endif  
              <div class="btn-search">
                  <button type="submit" class = "btn btn_searchBtn">Искать</button>
              </div>
            </form>
          </div>
        <footer class="footer">
          <!-- <div class="blue"></div> -->
          <div class="flex center space_between web-footer">		        
            <div class="container space_between flex" style = "align-items:baseline;">
              <div class="footer-left flex">
                <div class="logo_footer upper">
                  <a class="logo_footer__a" href="/">Мои услуги</a>
                </div>
                <div class="contacts">
                  <p>Служба поддержки</p>
                  <a href="tel:88003334545">8 800 333-45-45</a>
                </div>
                <div class="contacts">
                  <p>E-mail</p>
                  <a href="tel:88003334545">8 800 333-45-45</a>
                </div>
                <div class="schedulle">
                  <p>Пн-Пт: <span>с 06:00 до 22:00</span></p>
                  <p>Сб-Вс: <span>с 06:00 до 22:00</span></p>
                </div>
              </div>
              <div class="logo_maint footer_right">
                <a href="https://maint.kz" target = "_blank"><img class="logo_maint__img" src="/images/samples/logo_maint.png" /></a>
              </div>
            </div>
          </div>
          <div class="footer_mob flex justify-content-between">
            <div class="footer_mob__left flex">
              <i class = "icon-layers_footer"></i>
              <p class = "footer_menu">услуги</p>
            </div>
            <!-- <div class = "circle_search"></div> -->
            <div class = "footer_mob__right flex circle_search">
              <i class = "icon-lense_footer-ico"></i>
              <p class = "footer_menu">поиск</p>
            </div>
          </div>          
        </footer><!-- end .footer -->
        
      </div>
    </div><!-- end .wrapper -->

    @include('layouts._modal_login')
    @include('layouts._modal_registration')
    @include('layouts._modal_password')

    <script src="{{ asset('js/app.min.js') }}"></script>
    <script src="{{ asset('js/jquery.mask.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    @stack('scripts')
    <script src="{{ asset('js/code.js') }}"></script>
    <!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> -->
    <!-- <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script> -->
  </body>
</html>	