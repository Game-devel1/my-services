<style>


.dropdown_spec {
  display: none;
  position: relative;
}

.dropdown_spec ul {
  border: .0625rem solid #a2a2a2;
  position: relative;
  display: inline-block;
  list-style: none;
  vertical-align: middle;
  width: 100%;
}

.dropdown-menu {
  position: absolute;
  top: 100%;
  left: 0;
  z-index: 1000;
  display: none;
  float: left;
  min-width: 160px;
  padding: 5px 0;
  margin: 2px 0 0;
  list-style: none;
  font-size: 14px;
  background-color: #fff;
  border: 1px solid #ccc;
  border: 1px solid rgba(0,0,0,.15);
  border-radius: 4px;
  -webkit-box-shadow: 0 6px 12px rgba(0,0,0,.175);
  box-shadow: 0 6px 12px rgba(0,0,0,.175);
  background-clip: padding-box;
}

.btn .caret {
    margin-left: 0;
}

.caret {
    display: inline-block;
    float: right;
    top: 50%;
    width: 0;
    height: 0;
    margin-left: 2px;
    vertical-align: middle;
    border-top: 4px solid;
    border-right: 4px solid transparent;
    border-left: 4px solid transparent;
    margin-top: 0.8rem;
}
</style>
<div class="change_page">
  <div class="change_page__title">Редактировать резюме </div>
  <form id="createResume" action="{{route('create.resume')}}" method="POST" enctype="multipart/form-data">
    {!! csrf_field() !!}
    <div class="change_page__download_photos">
      Размер фотографии 270х190
      <div class="images flex">
        <div style="width: 21%; margin-top: 28px;" class ="download_image_block">
          <button type="submit" class="removeAvaForm" style="visibility: hidden; color: #c8d400; font-size: 1.5rem; cursor: pointer;">x</button>
          <div style="width: 84%;">             
            <input type="file" name="ava_img[]" id="ava_img" data-multiple-caption='{count} файлов выбрано' class="inputfile" size="3" multiple />
            <label for="ava_img">
              <p>Загрузить фотографию </p>
              <i class="icon-download_arrow"></i>
            </label>
          </div>
        </div>
        <span class = "error_word" data-error="ava_img"></span>
      </div>
    </div>
    <div class="input_area">
      <div class="input_area__title">*- поле для обязательного заполнения</div>
      <div class="input_area__flexed_input flex justify-content-between">
        <div class="first">
          <label for="last_name" class='partfoli_change_label'>Фамилия*</label>
          <input type="text" id="last_name" name="last_name" value = "{{ $user->last_name }}">
          <span class = "error_word" data-error="last_name"></span>
        </div>
        <div class="first">
          <label for="patronymic" class='partfoli_change_label'>Отчество</label>
          <input type="text" id="patronymic" name="patronymic" value = "{{ $user->patronymic }}">
          <span class = "error_word" data-error="patronymic"></span>
        </div>
      </div>
      <div class="input_area__flexed_input flex justify-content-between">
        <div class="first">
          <label for="first_name" class='partfoli_change_label'>Имя*</label>
          <input type="text" id="first_name" name="first_name" value = "{{ $user->first_name }}">
          <span class = "error_word" data-error="first_name"></span>
        </div>
        <div class="first">
          <label for="whatsapp" class='partfoli_change_label'>Контакты (WhatsApp)*</label>
          <input type="text" id='whatsapp' name="whatsapp">
          <span class = "error_word" data-error="whatsapp"></span>
        </div>
      </div>
      <div class="input_area__flexed_input flex justify-content-between">
        <div class="first">
          <label style="display: block;" class='partfoli_change_label'>Часы работы (Будни)*</label>
          <input type="time" id="weekday_start_time" name="weekday_start_time">
          <input type="time" id="weekday_finish_time" name="weekday_finish_time">
          <span class = "error_word" data-error="weekday_start_time"></span>
          <!-- <span class = "error_word" data-error="weekday_finish_time"></span> -->
        </div>
        <div class="first">
          <label style="display: block;" class='partfoli_change_label'>Часы работы (Выходные)*</label>
          <input type="time" id="weekend_start_time" name="weekend_start_time">
          <input type="time" id="weekend_finish_time" name="weekend_finish_time">
          <span class = "error_word" data-error="weekend_start_time"></span>
          <!-- <span class = "error_word" data-error="weekend_finish_time"></span> -->
        </div>
      </div>
      <div class="input_area__flexed_input flex justify-content-between">
        <div class="first list_of_specialists" id = "specializationSelect">
          <input type="hidden" name="sub_section_id" id="sub_section_id">
          <label for="specialization" class = 'partfoli_change_label specialization_title'>Специализация*</label>
            <ul class = "default_select default_section_ul">
                <li>Выбрать</li>
            </ul>
            <span class = "error_word" data-error="sub_section_id" style = "margin-top:13px;margin-left:-10px;position:absolute;"></span>
            <ul class = 'section_change'>
              @foreach ($sections as $section)
                <li class = "section_change__item">{{$section->name}}
                  <ul class = "sub_section">
                    @foreach ($subsections as $sub_section)
                      @if ($sub_section->section_id == $section->id)
                        <li data-id = "{{$sub_section->id}}" class = "sub_section__item">{{$sub_section->name}}</li>
                      @endif
                    @endforeach
                  </ul>
                </li>
              @endforeach
            </ul>
        </div>
        <div class="first">
          <label for="site" class='partfoli_change_label'>Сайт</label>
          <input type="text" id='site' name="site">
          <span class = "error_word" data-error="site"></span>
        </div>
      </div>  
      <div class="input_area__flexed_input flex justify-content-between">
          <div class="half">
            <div class="first list_of_specialists" style = "padding-bottom:21px" id = "citySelect">
              <input type="hidden" name="region_id" id="region_id">
              <label for="specialization" class = 'partfoli_change_label specialization_title'>Город*</label>
                <ul class = "default_select default_select_city">
                    <li id = "default_city">Выбрать</li>
                </ul>

                <ul class="section_change cyties_scroll">
                  @foreach ($regions as $region)
                    <li data-id = "{{$region->id}}" class = "city_change section_change__item">{{$region->name}}</li>
                  @endforeach
                </ul>
            </div>
            <span class = "error_word" data-error="region_id"></span>
            
            <div class="first list_of_specialists" style = "padding-bottom:21px" id = "citySelect">
              <input type="hidden" name="region_id" id="region_id">
              <label for="specialization" class = 'partfoli_change_label specialization_title'>Район*</label>
                <ul class = "default_select default_select_regionPart">
                    <li id = "default_regionPart">Выбрать</li>
                </ul>

                <ul class="section_change cyties_scroll">
                  @foreach ($regions as $region)
                    <li data-id = "{{$region->id}}" class = "regionPart_change section_change__item">{{$region->name}}</li>
                  @endforeach
                </ul>
            </div>
         </div>
        <div class="first">
            <label for="description" class = 'partfoli_change_label'>Описание (макс. 256 символов)</label>
            <textarea type="text" id = 'description' name = "description" rows="4" cols="50" maxlength="256"></textarea>
        </div>
      </div>
      <div class="input_area__flexed_input flex justify-content-between">
          <div class="half">
            <div class="first">
                  <label for="cost" class = 'partfoli_change_label'>Стоимость*</label>
                  <input type="text" id = 'cost' name = "cost" maxlength = "9">
                  <span class = "error_word" data-error="cost"></span>
            </div>
          </div>
      </div>
    </div>
    <div id="portfolio_resume">
      @if (count($resumes) == 0)
        <div class="partgolio_change">
          <div class="partgolio_change__title"> Портфолио: Размер фотографии 270х190 </div>
          <div class="partgolio_change__images_partfolio flex">
            <!-- <img src="/images/samples/avatar.jpg" /> -->
            <!-- <img src="/images/samples/avatar.jpg" /> -->
            <div class="download_image_block">
              <input type="file" name="portfolio[]" id="portfolio" class="inputfile" data-multiple-caption='{count} файлов выбрано' size="10" multiple />
              <label for="portfolio">
                <p>Загрузить фотографию </p>
                <i class="icon-download_arrow"></i>
              </label>
            </div>
            <span class = "error_word" data-error="portfolio"></span>
          </div>
        </div>
        <div class="partgolio_change">
          <div class="partgolio_change__title"> Лицензии и сертификаты : Размер фотографии 270х190 </div>
          <div class="partgolio_change__images_partfolio flex">
            <!-- <img src="/images/samples/avatar.jpg" /> -->
            <!-- <img src="/images/samples/avatar.jpg" /> -->
            <div class="download_image_block">
              <input type="file" name="license[]" id="license" class="inputfile" data-multiple-caption='{count} файлов выбрано' size="10" multiple />
              <label for="license">
                <p>Загрузить фотографию </p>
                <i class="icon-download_arrow"></i>
              </label>
            </div>
            <span class = "error_word" data-error="license"></span>
          </div>
        </div>
        <div class = "flex" style = "align-items:baseline;margin-top:8px;">
          <i class = "icon-Star"></i>
          <p style = "margin-left:8px;">Лицензии и портфолио появляются на сайте после подтверждения модератором</p>
        </div>
      @endif
    </div>
    <div class="btn_saveChange">
      <button type="submit" class='btn btn_saveChanges resume_submit flex'>
        <i class="icon-checked_button"></i> Сохранить редактирование</button>
    </div>
  </form>
</div>
