<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeWorkingTimeToResumeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('resume', function (Blueprint $table) {
            $table->time('weekday_start_time')->nullable()->change();
            $table->time('weekday_finish_time')->nullable()->change();
            $table->time('weekend_start_time')->nullable()->change();
            $table->time('weekend_finish_time')->nullable()->change();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('resume', function (Blueprint $table) {
            //
        });
    }
}
