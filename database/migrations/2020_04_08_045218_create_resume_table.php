<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResumeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resume', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('section_id')->unsigned();
            $table->integer('sub_section_id')->unsigned();
            $table->integer('region_id')->unsigned();
            $table->integer('cost');
            $table->time('weekday_start_time');
            $table->time('weekday_finish_time');
            $table->time('weekend_start_time');
            $table->time('weekend_finish_time');
            $table->string('site')->nullable();
            $table->string('whatsapp')->nullable();
            $table->text('description')->nullable();
            $table->integer('viewed')->nullable();
            $table->text('img1')->nullable();
            $table->text('img2')->nullable();
            $table->text('img3')->nullable();
            $table->text('Big_img')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resume');
    }
}
