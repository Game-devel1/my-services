<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToSomeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('licenses', function (Blueprint $table) {
            $table->integer('resume_id');
        });

        Schema::table('portfolios', function (Blueprint $table) {
            $table->integer('resume_id');
        });

        Schema::table('resume', function (Blueprint $table) {
            $table->integer('isLicense')->default(0);
            $table->integer('isPortfolio')->default(0);
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('licenses', function (Blueprint $table) {

        });       
    }
}
